This project is a C++ implementaion of [`rdchiral`](https://github.com/connorcoley/rdchiral).

Compilation
------

Before compilation, first compile RDKit <https://www.rdkit.org/docs/Install.html>. An example build script is included in `third_party` directory. RDKit 2020.03 have been tested.

The code has been tested with GCC 10. This project uses C++20 features. `cxx17` branch uses only C++17 standard and can be compiled with GCC 7.


CMake flags:

- `-DCMAKE_INSTALL_PREFIX=${PREFIX}`: Installation prefix.
- `-DCMAKE_BUILD_TYPE=Release`: Build type, `Release` or `Debug`. `Debug` mode has sanitizers turned on. For debugging with gdb, `export ASAN_OPTIONS=detect_leaks=0`.
- `-DRDKIT_DIR=$RDBASE`: RDKit C++ headers and libs
- `-DUSE_PYTHON=ON`: Build Python wrapper
- `-DRDCHIRALCPP_BUILD_TESTS=ON`: Build the C++ test (or not)
- `-DBoost_NO_BOOST_CMAKE=ON`: For Debian based distribution
- `PYTHON_INCLUDE_DIR, PYTHON_LIBRARY`: Non-standard Python headers and libs

To compile without python

```
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```

To compile debug version

```
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug
make
```

To compile with python, set `PYTHON_INCLUDE_DIR` and `PYTHON_LIBRARY` if necessary

```
mkdir -p build
cd build
cmake .. \
    -DCMAKE_BUILD_TYPE=Release \
    -DUSE_PYTHON=ON
make
```

Test
------

CMake tests,

```
# in cmake build directory
make test
```

C++ tests.

```
RDCHIRAL_ROOT=/path/to/rdchiral_cpp
cd ${RDCHIRAL_ROOT}
cd bin
./test_run_reaction
```

Use with python.

```
# include RDKit lib dir if you build your own version
export LD_LIBRARY_PATH=${RDKIT_DIR}/lib
# NOTE: change the python version to your own version
export PYTHONPATH=$PYTHONPATH:${RDCHIRAL_ROOT}/lib:${RDKIT_DIR}/lib64/python3.9/site-packages
cd ${RDCHIRAL_ROOT}
cd test
python test_rdchiral.py
```

Docker image
------
Important note: The current docker image is based on Debian testing repository in order to access gcc-10 package. The testing repository may not be suitable for production environment.

```
cd ${RDCHIRAL_ROOT}/docker
docker build . -t rdchiral
docker run -it rdchiral /bin/bash
# docker tty
cd /usr/local/src/rdchiral_cpp/test
python test_rdchiral.py
```

Conda
------
Many thanks to Hadrien Mary (@hadim) for maintaining the conda-forge package.

To install the package from anaconda, run

```
conda create --name rdchiral_cpp
conda activate rdchiral_cpp
conda install -c conda-forge rdchiral_cpp

# or install from my personal channel
conda install -c conda-forge -c ljn917 rdchiral_cpp
```


There are two ways to build with conda. One is simply building a conda package.

```
conda create --name conda_build
conda activate conda_build
conda install conda-build conda-verify -c conda-forge
conda-build -c conda-forge conda

# file name may vary depending on your package version
conda install -c conda-forge --use-local /path/to/rdchiral_cpp-1.0.0-py38_0.tar.bz2

# or upload to anaconda
conda install anaconda-client -c conda-forge
anaconda login
anaconda upload /path/to/rdchiral_cpp-1.0.0-py38_0.tar.bz2
```

The second method is using packages from conda as dependencies, but installing as normal cmake. It is useful for development.

```
./conda/build.sh

# uninstall
cd build_conda
make uninstall
```

Usage
------

Refer to the tests in the `test` directory, or show detailed usage:

```
import rdchiral
help(rdchiral)
```

Known caveats
------
- Due to a [RDKit bug](https://github.com/rdkit/rdkit/issues/4315), `rdchiral.canonicalize_tranform` may produce non-unique templates and drop double bond directions if one of the substituent atoms is aromatic.
