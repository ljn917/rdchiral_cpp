# http://www.dalkescientific.com/writings/diary/archive/2016/08/15/fragment_parity_calculation.html

import itertools

def parity_shell(values):
    # Simple Shell sort; while O(N^2), we only deal with at most 4 values 
    values = list(values)
    N = len(values)
    num_swaps = 0
    for i in range(N-1):
        for j in range(i+1, N):
            if values[i] > values[j]:
                values[i], values[j] = values[j], values[i]
                num_swaps += 1
    return num_swaps % 2

def get_partition_score(swap_pair, permutations):
    i, j = swap_pair
    num_lt = num_gt = 0
    for permutation in permutations:
        if permutation[i] < permutation[j]:
            num_lt += 1
        else:
            num_gt += 1
    return abs(num_lt - num_gt)

def partition_permutations(i, j, permutations):
    lt_permutations = []
    gt_permutations = []
    for permutation in permutations:
        if permutation[i] < permutation[j]:
            lt_permutations.append(permutation)
        else:
            gt_permutations.append(permutation)
    return lt_permutations, gt_permutations

def generate_parity_function(n):
    print("def parity{}(data):".format(n))
    permutations = list(itertools.permutations(range(n)))
    swap_pairs = [(i, j) for i in range(n-1) for j in range(i+1, n)]
    _generate_comparison(permutations, swap_pairs, "  ")

def _generate_comparison(permutations, swap_pairs, indent):
    if len(permutations) == 1:
        parity = parity_shell(permutations[0])
        print(indent + "return {} # {} ".format(parity, permutations[0]))
        return
    
    swap_pair = min(swap_pairs, key=lambda x: get_partition_score(x, permutations))
    # Delete the swap pair because it can't be used again.
    # (Not strictly needed as it will always have the worse score.)
    del swap_pairs[swap_pairs.index(swap_pair)]

    # I could have a case where the lt subset has 0 elements while the
    # gt subset has 1 element. Rather than have the 'if' block do nothing,
    # I'll swap the comparison indices and swap branches.
    i, j = swap_pair
    lt_permutations, gt_permutations = partition_permutations(i, j, permutations)
    if not lt_permutations:
        lt_permutations, gt_permutations = gt_permutations, lt_permutations
        i, j = j, i
    
    print(indent + "if data[{i}] < data[{j}]:".format(i=i, j=j))
    # Need to copy the swap_pairs because the 'else' case may reuse a pair.
    _generate_comparison(lt_permutations, swap_pairs[:], indent+"  ")
    if gt_permutations:
        print(indent + "else:")
        _generate_comparison(gt_permutations, swap_pairs, indent+"  ")
    

if __name__ == "__main__":
    import sys
    n = 4
    if sys.argv[1:]:
        n = int(sys.argv[1])
    generate_parity_function(n)
