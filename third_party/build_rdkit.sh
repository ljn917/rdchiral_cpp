# https://www.rdkit.org/docs/Install.html

conda create --name rdchiral
conda install -y python
conda install -y cmake cairo pillow eigen pkg-config
conda install -y boost-cpp boost py-boost
conda install -y numpy matplotlib

conda install -y gxx_linux-64
# conda install -y gxx_linux-ppc64le


git clone https://github.com/rdkit/rdkit.git rdkit-github
cd rdkit-github
export RDBASE=$(pwd)
export RDVER=Release_2020_03
git checkout $RDVER
mkdir build && cd build
# C++ only
cmake -DRDK_BUILD_PYTHON_WRAPPERS=ON \
  -DRDK_BUILD_CAIRO_SUPPORT=ON \
  -DRDK_BUILD_INCHI_SUPPORT=ON \
  -DRDK_BUILD_AVALON_SUPPORT=ON \
  -DRDK_INSTALL_INTREE=OFF \
  -DCMAKE_INSTALL_PREFIX="$RDBASE/../$RDVER" \
  -DRDK_INSTALL_STATIC_LIBS=ON \
  -DRDK_BUILD_CPP_TESTS=ON \
  -DBOOST_ROOT="/" \
  -DCMAKE_BUILD_TYPE=Release \
  ..

#  -DRDK_BUILD_INCHI_SUPPORT=ON \
#  -DRDK_BUILD_AVALON_SUPPORT=ON \

# for ppc64le
#  -DRDK_OPTIMIZE_NATIVE=OFF \
# replace RDK_OPTIMIZE_NATIVE
#  -DRDK_OPTIMIZE_POPCNT=OFF

# -DBoost_USE_STATIC_LIBS=OFF

make -j16
make install


# env
# need to add these to .bash_profile
# export RDBASE=$(HOME)/rdkit-build/rdkit
# or set CMAKE_INSTALL_PREFIX as RDBASE
export PYTHONPATH=$PYTHONPATH:$RDBASE
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$RDBASE/lib

# run test
ctest

