# Attempts to find RDKit libraries using the current value of $RDBASE
# or, failing that, a version in my home directory
# It returns the static (.a) libraries not the .so ones because that's
# easiest for shipping (on Unix anyway. This may have to change once I start
# dealing with Windows as well.)
#
# It will define
# RDKIT_FOUND as RDKit_FOUND if it finds everything it needs
# RDKIT_INCLUDE_DIR
# RDKIT_LIBRARIES as requested

if(NOT RDKIT_DIR)
  set(RDKIT_DIR $ENV{RDBASE})
endif(NOT RDKIT_DIR)
if(NOT RDKIT_DIR)
  message( FATAL_ERROR "Need to provide RDKit location by -DRDKIT_DIR= or RDBASE env." )
endif(NOT RDKIT_DIR)

find_package( Boost COMPONENTS iostreams filesystem system REQUIRED)
find_package( Cairo )
find_package( Threads REQUIRED)

set(RDKIT_INCLUDE_DIR ${RDKIT_DIR}/include/rdkit ${Boost_INCLUDE_DIRS})

if(Cairo_FOUND)
  set(RDKIT_INCLUDE_DIR ${RDKIT_INCLUDE_DIR} ${CAIRO_INCLUDE_DIRS})
  set(RDKIT_LIBRARIES ${RDKIT_LIBRARIES} ${CAIRO_LIBRARIES})
endif()

set(RDKIT_FOUND "RDKit_FOUND")
# libraries, as specified in the COMPONENTS
foreach(component ${RDKit_FIND_COMPONENTS})
  message( "Looking for RDKit component ${component}" )
  find_file( RDKit_LIBRARY_${component}
    libRDKit${component}${CMAKE_SHARED_LIBRARY_SUFFIX}
    PATH ${RDKIT_DIR}/lib NO_DEFAULT_PATH)
  message("RDKit_LIBRARY_${component} : ${RDKit_LIBRARY_${component}}")
  if(${RDKit_LIBRARY_${component}} MATCHES "-NOTFOUND$")
    message(FATAL_ERROR "Didn't find RDKit ${component} library.")
  endif(${RDKit_LIBRARY_${component}} MATCHES "-NOTFOUND$")
  set(RDKIT_LIBRARIES ${RDKIT_LIBRARIES} ${RDKit_LIBRARY_${component}})
endforeach(component)

set(RDKIT_LIBRARIES ${RDKIT_LIBRARIES} ${Boost_LIBRARIES} Threads::Threads z)

message("RDKIT_INCLUDE_DIR : ${RDKIT_INCLUDE_DIR}")
message("RDKIT_LIBRARIES : ${RDKIT_LIBRARIES}")
message("RDKIT_FOUND : ${RDKIT_FOUND} at ${RDKIT_DIR}")
