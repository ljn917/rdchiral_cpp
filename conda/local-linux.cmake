# set sysroot and prefix
set(CMAKE_FIND_ROOT_PATH $ENV{CONDA_BUILD_SYSROOT} $ENV{CONDA_PREFIX})

set(CMAKE_SYSROOT $ENV{CONDA_BUILD_SYSROOT})

# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
