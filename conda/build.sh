#!/bin/bash

# https://docs.conda.io/projects/conda-build/en/latest/resources/build-scripts.html
# run from top source dir
# run as ./conda/build.sh to build locally with conda environment
# `conda-build -c conda-forge conda` to build conda package

# if conditions
set +o errexit

if [ -z "$CONDA_BUILD" ]; then
    CONDA_BUILD=0
    echo "Not in conda-build mode"
fi

conda info -a
CONDA_ENV_NAME=rdchiral_cpp_build
CONDA_ENV_LIST=$(conda env list | awk '{print $1}')
echo "CONDA_ENV_NAME: $CONDA_ENV_NAME"
echo "CONDA_ENV_LIST: $CONDA_ENV_LIST"
# check if env name already exists
CONDA_ENV_CHECK_TMP=$(echo "$CONDA_ENV_LIST" | grep $CONDA_ENV_NAME)
echo "CONDA_ENV_CHECK_TMP: $CONDA_ENV_CHECK_TMP"
if [ $CONDA_BUILD -ne 1 ] && [ "$CONDA_ENV_NAME" != "$CONDA_ENV_CHECK_TMP" ] && [ "$CONDA_DEFAULT_ENV" != "$CONDA_ENV_NAME" ]; then
    compiler_version=">=9.3" # we need gcc 9.3, at least
    boost_version="" # newest should work, otherwise =1.71
    python_version="=3.8"
    rdkit_version="=2020.03"
    echo "Creating conda env: $CONDA_ENV_NAME"
    conda update -q conda
    conda create -y --name rdchiral_cpp_build \
        gxx_linux-64${compiler_version} cmake \
        boost-cpp${boost_version} boost${boost_version} \
        numpy matplotlib cairo pillow eigen pandas \
        sphinx recommonmark jupyter \
        python${python_version} rdkit${rdkit_version} -c conda-forge
    conda activate rdchiral_cpp_build
else
    echo "Using existing conda env: $CONDA_ENV_NAME"
fi

CC=$(which x86_64-conda-linux-gnu-gcc)
CXX=$(which x86_64-conda-linux-gnu-g++)
echo "CC=$CC"
echo "CXX=$CXX"

# https://docs.conda.io/projects/conda-build/en/latest/user-guide/environment-variables.html
# Note: PREFIX is set as the target CONDA_PREFIX
if [ $CONDA_BUILD -eq 1 ]; then
CMAKE_PLATFORM_FLAGS+=(-DCMAKE_TOOLCHAIN_FILE="${SRC_DIR}/conda/cross-linux.cmake")
else
SRC_DIR="$(pwd)"
CMAKE_PLATFORM_FLAGS+=(-DCMAKE_TOOLCHAIN_FILE="${SRC_DIR}/conda/local-linux.cmake")
PREFIX=${CONDA_PREFIX}
PYTHON=python
CPU_COUNT=8
fi
mkdir -p build_conda
cd build_conda
rm * -rf
cmake   -DCMAKE_BUILD_TYPE=Release \
        -DRDKIT_DIR=${PREFIX} \
        -DUSE_PYTHON=ON \
        -DBoost_NO_BOOST_CMAKE=ON \
        ${CMAKE_PLATFORM_FLAGS[@]} \
        -DCMAKE_INSTALL_PREFIX=${PREFIX} \
        ${SRC_DIR}
make -j $CPU_COUNT
make install

if [ $CONDA_BUILD -ne 1 ]; then
    $PYTHON ${SRC_DIR}/test/test_rdchiral.py
fi
