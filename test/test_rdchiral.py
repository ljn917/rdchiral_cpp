# MIT License
#
# Copyright (c) 2017 Connor Coley
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# This test was copied from
# https://github.com/connorcoley/rdchiral/blob/master/test/test_rdchiral.py

import os, sys, json
sys.path = [os.path.dirname(os.path.dirname((__file__)))] + sys.path 

from rdchiral.main import rdchiralReaction, rdchiralReactants, rdchiralRunText, rdchiralRun

with open(os.path.join(os.path.dirname(__file__), 'test_rdchiral_cases.json'), 'r') as fid:
    test_cases = json.load(fid)

all_passed = True
for i, test_case in enumerate(test_cases):

    print('\n# Test {:2d}/{}'.format(i+1, len(test_cases)))

    # Directly use SMILES/SMARTS
    reaction_smarts = test_case['smarts']
    reactant_smiles = test_case['smiles']
    if rdchiralRunText(reaction_smarts, reactant_smiles) == test_case['expected']:
        print('    from text: passed')
    else:
        print('    from text: failed')
        all_passed = False

    # Pre-initialize & repeat
    rxn = rdchiralReaction(reaction_smarts)
    reactants = rdchiralReactants(reactant_smiles)
    if all(rdchiralRun(rxn, reactants) == test_case['expected'] for j in range(3)):
        print('    from init: passed')
    else:
        print('    from init: failed')
        all_passed = False

all_passed = 'All passed!' if all_passed else 'Failed!'
print('\n# Final result: {}'.format(all_passed))

if not all_passed:
    sys.exit(-1)
