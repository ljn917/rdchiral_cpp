from rdchiral.main import rdchiralReaction, rdchiralReactants, rdchiralRunText, rdchiralRun

# combine_enantiomers_into_racemic() test
res = rdchiralRunText("([C:6]/[C:5]=[CH;D2;+0:4]\\[CH;D2;+0:1]=[C:2]/[C:3])>>Br/[CH;D2;+0:1]=[C:2]\\[C:3].C-C-C-C-[Sn](-C-C-C-C)(-C-C-C-C)/[CH;D2;+0:4]=[C:5]\\[C:6]", "C#CC1=CC=C(C(=C)OC)C1")
assert set(res) == {'C#C/C(=C\\Br)CC(=C[Sn](CCCC)(CCCC)CCCC)C(=C)OC', 'C#CC(=CBr)C/C(=C/[Sn](CCCC)(CCCC)CCCC)C(=C)OC', 'C#C/C(=C\\[Sn](CCCC)(CCCC)CCCC)CC(=CBr)C(=C)OC', 'C#C/C(=C/[Sn](CCCC)(CCCC)CCCC)C/C(=C/Br)C(=C)OC'}

res = rdchiralRunText('[F:1][C@@H:2]([Cl:3])([Br:4])>>[F:1][C@H:2]([Cl:3])([Br:4])', '[F:1][C@@H:2]([Cl:3])([Br:4])')
assert res == ['F[C@H](Cl)Br']
res = rdchiralRunText('[F:1][C@@H:2]([Cl:3])([Br:4])>>[F:1][C@H:2]([Cl:3])([Br:4])', '[F:1][C@H:2]([Cl:3])([Br:4])')
assert res == ['F[C@@H](Cl)Br']

print('pass')
