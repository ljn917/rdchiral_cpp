#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <chrono>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/string.hpp>

#include <GraphMol/SmilesParse/SmilesWrite.h>
#include <GraphMol/SmilesParse/SmilesParse.h>
#include <GraphMol/SmilesParse/SmartsWrite.h>

#include "rdchiral/rdchiral.hpp"
#include "rdchiral/smarts_util.hpp"

/*
std::string canonicalize_smarts_old(const std::string& smarts) {
    auto s = rdchiral::canonicalize_smarts_atom(smarts, false);
    return rdchiral::canonicalize_transform_old(s);
}

bool smarts_compare(const std::string& s1, const std::string& s2) {
    if(canonicalize_smarts_old(s1) == canonicalize_smarts_old(s2)) return true;
    // cannot directly convert SMARTS query to SMILES, github:rdkit #3193
    std::vector<std::string> s1_split, s2_split;
    auto s1_smiles = rdchiral::canonicalize_smarts_atom(s1);
    auto s2_smiles = rdchiral::canonicalize_smarts_atom(s2);
    boost::algorithm::split(s1_split, s1_smiles, boost::is_any_of(">"));
    boost::algorithm::split(s2_split, s2_smiles, boost::is_any_of(">"));
    auto mol1_r = RDKit::RWMOL_SPTR(RDKit::SmartsToMol(s1_split[0]));
    rdchiral::clear_mapnum(mol1_r);
    std::string smiles1_r = RDKit::MolToSmiles(*mol1_r);
    std::string smiles1_p;
    if(s1_split.size() == 3) {
        auto mol1_p = RDKit::RWMOL_SPTR(RDKit::SmartsToMol(s1_split[2]));
        rdchiral::clear_mapnum(mol1_p);
        smiles1_p = RDKit::MolToSmiles(*mol1_p);
    }
    auto mol2_r = RDKit::RWMOL_SPTR(RDKit::SmartsToMol(s2_split[0]));
    rdchiral::clear_mapnum(mol2_r);
    std::string smiles2_r = RDKit::MolToSmiles(*mol2_r);
    std::string smiles2_p;
    if(s1_split.size() == 3) {
        auto mol2_p = RDKit::RWMOL_SPTR(RDKit::SmartsToMol(s2_split[2]));
        rdchiral::clear_mapnum(mol2_p);
        smiles2_p = RDKit::MolToSmiles(*mol2_p);
    }
    std::cout << "smiles1_r: " << smiles1_r << std::endl;
    std::cout << "smiles1_p: " << smiles1_p << std::endl;
    std::cout << "smiles2_r: " << smiles2_r << std::endl;
    std::cout << "smiles2_p: " << smiles2_p << std::endl;
    bool res = smiles1_r == smiles2_r && smiles1_p == smiles2_p;
    if(res) std::cout << "warning: smarts_compare using weak condition" << std::endl;
    return res;
}
*/

bool transform_compare(const std::string& s1, const std::string& s2) {
    return rdchiral::canonicalize_transform(s1) == rdchiral::canonicalize_transform(s2);
}

bool smarts_compare(const std::string& s1, const std::string& s2) {
    return rdchiral::canonicalize_smarts(s1) == rdchiral::canonicalize_smarts(s2);
}

std::unordered_set<std::string> bad_reactions{
/*
    "CCCCC(F)(F)C(O)CC[C@@H]1[C@@H](CCC[CH2:18][CH2:19][CH2:20][C:21]([OH:23])=[O:22])C(=O)C[C@H]1O.C1CCC(N[CH:35]2[CH2:40][CH2:39][CH2:38][CH2:37][CH2:36]2)CC1.C(O)(=O)[CH2:42][C:43]([CH2:48]C(O)=O)([C:45](O)=O)[OH:44].[Cl-].[Na+].Cl.C1([N:63]=C=NC2CCCCC2)CCCCC1.[C:72]([O:75][CH2:76]C)(=[O:74])C>C(Cl)Cl.C(N(CC)CC)C>[NH:63]([C:72]([O:75][CH2:76][C:35]1[CH:36]=[CH:37][CH:38]=[CH:39][CH:40]=1)=[O:74])[C@H:20]([C:21]([OH:23])=[O:22])[C@@H:19]([CH3:18])[O:44][C:43]([CH3:42])([CH3:45])[CH3:48] |f:0.1,3.4|",
    "[NH:1]([C:10]([O:12][CH2:13][C:14]1[CH:19]=[CH:18][CH:17]=[CH:16][CH:15]=1)=[O:11])[C@H:2]([C:7]([OH:9])=O)[C@H:3]([CH2:5][CH3:6])[CH3:4].CCCCC(F)(F)C(O)CC[C@@H]1[C@@H](CCCCCCC(O)=O)[C:31](=[O:32])C[C@H]1O.C1CC[CH:50]([NH:53]C2CCCCC2)[CH2:49]C1.C(O)(=O)CC(CC(O)=O)(C(O)=O)[OH:63]>C(OCC)(=O)C>[NH:1]([C:10]([O:12][CH2:13][C:14]1[CH:19]=[CH:18][CH:17]=[CH:16][CH:15]=1)=[O:11])[C@H:2]([C:7]([NH:53][CH2:50][C:49]([O:32][CH3:31])=[O:63])=[O:9])[C@H:3]([CH2:5][CH3:6])[CH3:4] |f:0.1.2|",
    "CC(C[C@H](NC([C@H](NC([C@@H](NC([C@@H](NC([C@@H](NC([C@@H](N[C:84]([C@@H:86]([NH2:92])[CH2:87][CH2:88][C:89]([OH:91])=[O:90])=[O:85])CC1NC=NC=1)=O)CC1C2C(=CC=CC=2)NC=1)=O)CO)=O)CC1NC=NC=1)=O)CC1C=CC(O)=CC=1)=O)C(N[C@H](C(N1[C@H](C(NCC(O)=O)=O)CCC1)=O)CCCN=C(N)N)=O)C.C[OH:94].ClCl.CC1C=C(C2C=CC(N)=C(C)C=2)C=CC=1N.II>CC(O)=O>[NH2:92][C@H:86]([C:84]([OH:85])=[O:94])[CH2:87][CH2:88][C:89](=[O:90])[OH:91] |f:2.3|",
    "[C@@H]1(N2C3N=CN=C(N)C=3N=C2)O[C@H](CO)[C@@H](O)[C@H]1[OH:3].[NH2:20][C:21]1[N:29]=[C:28]2[C:24]([N:25]=[CH:26][N:27]2[C@@H:30]2[O:36][C@H:35]([CH2:37][OH:38])[C@@H:33]([OH:34])[C@@H:31]2[OH:32])=[C:23](N)[N:22]=1.P([O-])([O-])([O-])=O>>[NH2:20][C:21]1[N:29]=[C:28]2[C:24]([N:25]=[CH:26][N:27]2[C@@H:30]2[O:36][C@H:35]([CH2:37][OH:38])[C@@H:33]([OH:34])[C@@H:31]2[OH:32])=[C:23]([OH:3])[N:22]=1",
    "C([O:4][C@@H:5]1[C@H:9]([O:10]C(=O)C)[C@@H:8]([CH2:14][O:15]C(=O)C)[O:7][C@H:6]1C1N=NNC=1)(=O)C.N.OO.[C@@H]1([N:36]2[N:40]=[C:39]([C:41]([NH2:43])=[O:42])[CH:38]=[N:37]2)O[C@H](CO)[C@@H](O)[C@H]1O>CC(O)C>[C@@H:6]1([N:37]2[CH:38]=[C:39]([C:41]([NH2:43])=[O:42])[N:40]=[N:36]2)[O:7][C@H:8]([CH2:14][OH:15])[C@@H:9]([OH:10])[C@H:5]1[OH:4]",
    "[CH:1]([N:3]1[CH2:7][CH2:6][NH:5][C:4]1=[O:8])=[O:2].C(N(CC)CC)C.C[Si](C)(C)[Cl:18].CC1(C)S[C@@H]2[C@H](NC([C@H](N)C3C=CC=CC=3)=O)C(=O)N2[C@H]1[C:41]([OH:43])=O>C1C=CC=CC=1>[CH:1]([N:3]1[CH2:7][CH2:6][N:5]([C:41]([Cl:18])=[O:43])[C:4]1=[O:8])=[O:2]",
    "[Cl:1][C:2]([N:4]1[CH2:8][S:7](=[O:10])(=[O:9])[NH:6][C:5]1=[O:11])=[O:3].[CH3:12]C1(C)S[C@@H]2[C@H](NC([C@H](N)C3C=CC=CC=3)=O)C(=O)N2[C@H]1C(O)=O>>[Cl:1][C:2]([N:4]1[CH2:8][CH2:12][S:7](=[O:10])(=[O:9])[NH:6][C:5]1=[O:11])=[O:3]",
    "[CH2:1]([Mg]Br)[CH3:2].[Mg].C(Br)C.[CH3:9][CH:10]([OH:13])[C:11]#[CH:12].[C:14]([O:28][C@@H]1C[C@@H](C)C(=O)C(C)(C)C1)([O:17][C@@H:18]1[CH2:23][C@@H:22]([CH3:24])[C:21](=[O:25])[C:20]([CH3:27])([CH3:26])[CH2:19]1)(C)[CH3:15].S(=O)(=O)(O)[OH:40]>O1CCCC1>[C:1]([O:13][CH:10]([C:11]#[C:12][C:21]1([OH:25])[C@H:22]([CH3:24])[CH2:23][C@@H:18]([O:17][C:14](=[O:28])[CH3:15])[CH2:19][C:20]1([CH3:26])[CH3:27])[CH3:9])(=[O:40])[CH3:2]",
    "C[C:2]1([CH3:14])S[C@@H]2[C@H](N)C(=O)N2[C@H:3]1[C:11]([OH:13])=O.CC(O[CH2:19][C:20]1CS[C@@H]2[C@H](N)C(=O)N2[C:21]=1C(O)=O)=O.[CH3:33]C1CS[C@@H]2[C@H](N)C(=O)N2C=1C(O)=O>>[C:11]([C:3]1[CH:2]=[CH:14][CH:21]=[CH:20][CH:19]=1)(=[O:13])[CH3:33]",
    "N[C@@H]1C(=O)N2C(C(OC(C3C=CC=CC=3)C3C=CC=CC=3)=[O:12])=C(C=C)CS[C@H]12.[CH:29]1([N:35]=[C:36]=[N:37][CH:38]2[CH2:43][CH2:42][CH2:41][CH2:40][CH2:39]2)[CH2:34][CH2:33][CH2:32][CH2:31][CH2:30]1>C(Cl)Cl.CN(C)C=O>[C:36]([NH:35][CH:29]1[CH2:30][CH2:31][CH2:32][CH2:33][CH2:34]1)([NH:37][CH:38]1[CH2:43][CH2:42][CH2:41][CH2:40][CH2:39]1)=[O:12]",
    "[CH3:1][C:2](=[CH:4][C:5](=[O:10])[C:6]([CH3:9])(O)C)[CH3:3].[CH:11](C=C1CC[C@H]2[C@H]3[C@H](CC[C@]12C)[C@]1(C)C(C[C@@H](O)CC1)=CC3)=O.C(C=C1CC[C@H]2[C@H]3[C@H](CC[C@]12C)C1CCC(=O)CC=1CC3)=O.CCC1C[C@@H]2[C@H](CC[C@@H]3[C@@H]4C(=CC(=O)CC4)CC[C@H]32)C1=CC=O>>[CH3:3][C:2](=[CH:4][C:5](=[O:10])[CH2:6][CH2:9][CH3:11])[CH3:1]",
*/
};

std::unordered_set<std::string> except_reactions{
/*
    "[CH2:1]1[C@H:6]([NH2:7])[C@@H:5]([O:8][C@H:9]2[O:14][C@H:13]([CH2:15][NH2:16])[C@@H:12]([OH:17])[C@H:11]([OH:18])[C@H:10]2[NH2:19])[C@H:4]([O:20][C@@H]2O[C@H](CO)[C@H](O)[C@H]2O)[C@@H:3]([OH:30])[C@@H:2]1[NH2:31].Cl>CO>[CH2:1]1[C@H:6]([NH2:7])[C@@H:5]([O:8][C@H:9]2[O:14][C@H:13]([CH2:15][NH2:16])[C@@H:12]([OH:17])[C@H:11]([OH:18])[C@H:10]2[NH2:19])[C@H:4]([OH:20])[C@@H:3]([OH:30])[C@@H:2]1[NH2:31]",
*/
};

int main(int argc, char *argv[]) {
    std::string fn{"../test/data/uspto.templates.txt"};
    std::ifstream input_file{fn};
    if(!input_file.is_open()) {
        std::cout << "Cannot open data file: " << fn << std::endl;
        std::cout << "Please generate data with clean_and_extract_uspto.py first." << std::endl;
        return -1;
    }
    
    std::ofstream output_file{"failed_extraction.txt", std::ofstream::trunc};
    if(!output_file.is_open()) {
        std::cout << "Cannot open output file" << std::endl;
        return -1;
    }
    
    bool all_correct{true};
    int correct_cnt{0}, record_cnt{0};
    
    std::string line;
    std::stringstream ss;
    
    auto time_start = std::chrono::high_resolution_clock::now();
    
    while(std::getline(input_file, line)) {
        ss.str(line);
        boost::property_tree::ptree root;
        boost::property_tree::read_json(ss, root);
        
        std::cout << "Test case: " << record_cnt << std::endl;
        
        // correct results
        auto reaction_smarts  = root.get<std::string>("reaction_smarts");
        auto reactants_smarts = root.get<std::string>("reactants");
        auto products_smarts  = root.get<std::string>("products");
        auto intra_only = root.get<bool>("intra_only");
        auto dimer_only = root.get<bool>("dimer_only");
        
        // reaction inputs
        auto reaction_node = root.get_child("reaction");
        std::string patent_number  = reaction_node.get<std::string>("PatentNumber");
        std::string reaction_smiles  = reaction_node.get<std::string>("ReactionSmiles");
        std::string reactants_smiles = reaction_node.get<std::string>("reactants");
        std::string products_smiles  = reaction_node.get<std::string>("products");
        
        std::cout << "Inputs: " << std::endl;
        std::cout << "patent_number:    " << patent_number  << std::endl;
        std::cout << "reaction_smiles:  " << reaction_smiles  << std::endl;
        std::cout << "reactants_smiles: " << reactants_smiles << std::endl;
        std::cout << "products_smiles:  " << products_smiles  << std::endl;
        
        std::cout << "Expected: " << std::endl;
        std::cout << "reaction_smarts:  " << reaction_smarts << std::endl; // retro
        std::cout << "reactants_smarts: " << reactants_smarts << std::endl;
        std::cout << "products_smarts:  " << products_smarts << std::endl;
        std::cout << "intra_only: " << intra_only << std::endl;
        std::cout << "dimer_only: " << dimer_only << std::endl;
        
        if(bad_reactions.contains(reaction_smiles)) continue;
        
        struct rdchiral::template_result results;
        try {
            results = \
                rdchiral::extract_from_reaction(
                    reactants_smiles,
                    products_smiles,
                    true,
                    true,
                    5,
                    true
                );
        } catch(std::exception& e) {
            std::string error1{"get_changed_atoms(): Reactants have non-unique atom mappings."};
            std::string error2{"Could not find consistent tetrahedral mapping"};
            if(e.what() == error1) {
                std::cout << e.what() << std::endl << std::endl;
                continue;
            } else if(std::string{e.what()}.find(error2) != std::string::npos) {
                std::cout << e.what() << std::endl << std::endl;
                continue;
            } else {
                throw;
            }
        }
        
        std::cout << "Results: " << std::endl;
        std::cout << "reaction_smarts_forward:  " << results.reaction_smarts_forward << std::endl;
        std::cout << "reaction_smarts_retro:  " << results.reaction_smarts_retro << std::endl;
        std::cout << "reactants_smarts: " << results.reactants_smarts << std::endl;
        std::cout << "products_smarts:  " << results.products_smarts << std::endl;
        std::cout << "intra_only: " << results.intra_only << std::endl;
        std::cout << "dimer_only: " << results.dimer_only << std::endl;
        
        // need to canonicalize smart atom
        bool is_correct =   transform_compare(results.reaction_smarts_retro, reaction_smarts) && \
                            results.intra_only == intra_only && \
                            results.dimer_only == dimer_only;
        
        if(except_reactions.contains(reaction_smiles)) is_correct = !is_correct;
        
        if(!is_correct) {
            std::cout << "reactants smarts: " << (smarts_compare(results.reactants_smarts, reactants_smarts) ? "correct" : "wrong") << std::endl;
            std::cout << "products smarts: " << (smarts_compare(results.products_smarts, products_smarts) ? "correct" : "wrong") << std::endl;
            std::cout << "reactants smarts (expected): " << rdchiral::canonicalize_smarts(reactants_smarts) << std::endl;
            std::cout << "reactants smarts (results):  " << rdchiral::canonicalize_smarts(results.reactants_smarts) << std::endl;
            std::cout << "products smarts (expected):  " << rdchiral::canonicalize_smarts(products_smarts) << std::endl;
            std::cout << "products smarts (results):   " << rdchiral::canonicalize_smarts(results.products_smarts) << std::endl;
            std::cout << "reaction_smiles:  " << reaction_smiles  << std::endl;
        }
        
        all_correct = all_correct && is_correct;
        if(is_correct) correct_cnt++;
        record_cnt++;
        
        std::cout << "correct: " << (is_correct?"true":"false") << std::endl;
        std::cout << std::endl;
        
        if(!is_correct) {
            boost::property_tree::ptree results_node;
            results_node.put("reaction_smarts_retro", results.reaction_smarts_retro);
            results_node.put("intra_only", results.intra_only);
            results_node.put("dimer_only", results.dimer_only);
            root.add_child("results", results_node);
            std::ostringstream buf;
            boost::property_tree::write_json(buf, root, false);
            output_file << buf.str();
        }
        
        // if(!is_correct) break;
    }
    
    auto time_end = std::chrono::high_resolution_clock::now();
    
    std::cout << "Test passes: " << correct_cnt << "/" << record_cnt << std::endl;
    std::cout << "Final results: " << (all_correct?"All passed":"Failed") << std::endl;
    
    auto execution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_end - time_start);
    std::cout << "Time used: " << execution_duration.count() << " milliseconds" << std::endl;
    
    input_file.close();
    output_file.close();
    
    return 0;
}
