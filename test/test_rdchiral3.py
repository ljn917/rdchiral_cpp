import rdchiral

res = rdchiral.extract_from_reaction(reactants='[CH3:1][CH3:2]', products='[CH4:1]')
print(res)

rxn = {
    '_id': '0',
    'reactants': '[Br:1][CH2:2][CH2:3][OH:4].[CH2:5]([S:7](Cl)(=[O:9])=[O:8])[CH3:6].CCOCC',
    'products':  '[CH2:5]([S:7]([O:4][CH2:3][CH2:2][Br:1])(=[O:9])=[O:8])[CH3:6]',
}
res = rdchiral.extract_from_reaction(rxn)
print(res)

r = '[Na+:1].[CH3:2][CH2:3][CH2:4][CH2:5][CH:6]([Br:7])[CH2:8]Br.[O-:9][S:10]([O-:11])=[O:12]'
p = '[Na+:1].[CH3:2][CH2:3][CH2:4][CH2:5][CH:6]([Br:7])[CH2:8][S:10]([O-:12])(=[O:9])=[O:11]'
template = rdchiral.extract_from_reaction(reactants=r, products=p)
print(template)
res = rdchiral.rdchiralRunText(reaction_smarts=template['reaction_smarts_retro'], reactant_smiles=p)
print(res)
assert len(res) > 0
template_mols = template['reaction_smarts_forward'].split('>')
template1 = "(" + template_mols[0] + ")>>" + template_mols[2]
res = rdchiral.rdchiralRunText(reaction_smarts=template1, reactant_smiles=r)
print(res)
assert len(res) > 0

print('pass')
