/* test_run_reaction2.cpp
 * test_run_reaction2 test_data.json verbose
 * Arguments:
 *      test_data.json: list of dict with keys: "reaction_smarts" (retro template) and "reaction_smiles"
 *      verbose: bool, verbose mode
 */

#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <chrono>
#include <mutex>
#include <thread>
#include <atomic>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/asio.hpp>

#include "rdchiral/rdchiral.hpp"
#include <GraphMol/SmilesParse/SmilesWrite.h>
#include <GraphMol/SmilesParse/SmilesParse.h>

std::mutex cout_mutex, cerr_mutex;

void clear_mapnum(RDKit::RWMOL_SPTR mol) {
    for(auto atom: mol->atoms()) {
        atom->setAtomMapNum(0);
    }
}

std::string canonicalize_smiles(const std::string& smiles) {
    auto mol = RDKit::RWMOL_SPTR(RDKit::SmilesToMol(smiles));
    clear_mapnum(mol);
    return RDKit::MolToSmiles(*mol);
}

void log_reaction_json(const std::string& reaction_smiles, const std::string& reaction_smarts) {
    std::lock_guard _lock{cerr_mutex};
    std::cerr << "{\"reaction_smiles\": \"" << reaction_smiles << "\", \"reaction_smarts\": \"" << reaction_smarts << "\"}" << std::endl;
}

int main(int argc, char *argv[]) {
    std::string fn{"../test/data/templates.1.3m.json"};
    if(argc > 1) {
        fn = argv[1];
    }
    
    std::cout << "Loading data: " << fn << std::endl;
    boost::property_tree::ptree root;
    boost::property_tree::read_json(fn, root);
    
    int num_thread = std::thread::hardware_concurrency();
    if(num_thread < 4) num_thread = 4;
    std::cout << "Using " << num_thread << " threads" << std::endl;
    
    std::atomic<int> correct_cnt{0}, record_cnt{0}, no_outcome_cnt{0}, mismatch_cnt{0};
    
    auto time_start = std::chrono::high_resolution_clock::now();
    
    boost::asio::thread_pool pool(num_thread);
    for(auto& record: root) {
        auto f = \
        [&correct_cnt, &record_cnt, &no_outcome_cnt, &mismatch_cnt, &record]() {
            auto reaction_smarts = record.second.get<std::string>("reaction_smarts");
            auto reaction_smiles = record.second.get<std::string>("reaction_smiles");
            
            std::vector<std::string> reaction_split;
            reaction_split.reserve(3);
            boost::algorithm::split(reaction_split, reaction_smiles, boost::is_any_of(">"));
            std::string smiles = reaction_split[2];
            std::string expected = canonicalize_smiles(reaction_split[0]);
            
            std::vector<std::string> smarts_split;
            smarts_split.reserve(3);
            boost::algorithm::split(smarts_split, reaction_smarts, boost::is_any_of(">"));
            //auto smarts = std::string{"("} + smarts_split[0] + ")>>(" + smarts_split[2] + ")";
            auto smarts = std::string{"("} + smarts_split[0] + ")>>" + smarts_split[2];
            
            // run with text wrapper
            bool is_correct{false};
            try {
                auto [outcomes, mapped_outcomes] = rdchiral::Reaction::run(smarts, smiles);
                
                for(auto i: outcomes) {
                    if(expected == canonicalize_smiles(i)) {
                        is_correct = true;
                        break;
                    }
                }
                
                if(!is_correct) {
                    log_reaction_json(reaction_smiles, reaction_smarts);
                    {
                        std::lock_guard _lock{cout_mutex};
                        std::cout << "reaction_smiles: " << reaction_smiles << std::endl;
                        std::cout << "smarts:   " << smarts << std::endl;
                        std::cout << "smiles:   " << smiles << std::endl;
                        std::cout << "expected: " << expected << std::endl;
                        std::cout << "outcomes: " << outcomes.size() << std::endl;
                        for(auto i: outcomes) {
                            std::cout << i << std::endl;
                        }
                    }
                    if(outcomes.empty()) no_outcome_cnt++;
                    else mismatch_cnt++;
                }
            } catch(std::exception& e) {
                log_reaction_json(reaction_smiles, reaction_smarts);
                {
                    std::lock_guard _lock{cout_mutex};
                    std::cout << "reaction_smiles: " << reaction_smiles << std::endl;
                    std::cout << "smarts:   " << smarts << std::endl;
                    std::cout << "smiles:   " << smiles << std::endl;
                    std::cout << "expected: " << expected << std::endl;
                    std::cout << "exception: " << e.what() << std::endl;
                }
            }
            
            if(is_correct) correct_cnt++;
            record_cnt++;
        };
#ifdef NDEBUG
        boost::asio::post(pool, f);
#else
        f();
#endif // NDEBUG
    }
    
    pool.join();
    
    auto time_end = std::chrono::high_resolution_clock::now();
    
    std::cout << "Test passes: " << correct_cnt << "/" << record_cnt << std::endl;
    std::cout << "No outcome: " << no_outcome_cnt << std::endl;
    std::cout << "Have outcomes but mismatch: " << mismatch_cnt << std::endl;
    std::cout << "Final results: " << (correct_cnt==record_cnt?"All passed":"Failed") << std::endl;
    
    auto execution_duration = std::chrono::duration_cast<std::chrono::seconds>(time_end - time_start);
    std::cout << "Time used: " << execution_duration.count() << " seconds" << std::endl;
    
    return 0;
}
