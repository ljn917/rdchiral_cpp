import rdchiral

def compare_templates(s1, s2, expected:bool=True):
    c1 = rdchiral.canonicalize_transform(s1)
    c2 = rdchiral.canonicalize_transform(s2)
    print(c1)
    print(c2)
    assert (c1 == c2) == expected, f"Error: {s1}, {s2} are not the same after canonicalize_transform(): {c1}, {c2}, "

compare_templates(
'[C:8]-[C&H0&+0&D3:7](=[O&H0&D1:6])-N-C-C-C-C-[C@&H1](-N)-C(-O)=O.[N&H2&+0&D1:5]-[C:4]-[C:3](=[O&H0&D1:2])-[O&H1&D1:1]>>[C:8]-[C&H0&+0&D3:7](-[N&H1&+0&D2:5]-[C:4]-[C:3](=[O&H0&D1:2])-[O&H1&D1:1])=[O&H0&D1:6]',
'N-[C@@H](-C-C-C-C-N-[C;H0;D3;+0:1](-[C:2])=[O;D1;H0:3])-C(-O)=O.[NH2;D1;+0:4]-[C:5]-[C:6](=[O;D1;H0:7])-[O;D1;H1:8]>>[C:2]-[C;H0;D3;+0:1](=[O;D1;H0:3])-[NH;D2;+0:4]-[C:5]-[C:6](=[O;D1;H0:7])-[O;D1;H1:8]'
)

compare_templates(
'[C:8]-[C&H0&+0&D3:7](=[O&H0&D1:6])-N-C-C-C-C-[C@@&H1](-N)-C(-O)=O.[O&H0&D1:2]=[C:3](-[O&H1&D1:1])-[C:4]-[N&H2&+0&D1:5]>>[C:8]-[C&H0&+0&D3:7](=[O&H0&D1:6])-[N&H1&+0&D2:5]-[C:4]-[C:3](=[O&H0&D1:2])-[O&H1&D1:1]',
'N-[C@@H](-C-C-C-C-N-[C;H0;D3;+0:1](-[C:2])=[O;D1;H0:3])-C(-O)=O.[NH2;D1;+0:4]-[C:5]-[C:6](=[O;D1;H0:7])-[O;D1;H1:8]>>[C:2]-[C;H0;D3;+0:1](=[O;D1;H0:3])-[NH;D2;+0:4]-[C:5]-[C:6](=[O;D1;H0:7])-[O;D1;H1:8]'
)

print('pass')
