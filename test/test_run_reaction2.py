import json
import sys
from rdkit import Chem
from rdchiral.main import rdchiralRunText

def clear_mapno(mol):
    for atom in mol.GetAtoms():
        atom.SetAtomMapNum(0)

def canonicalize_smiles(smiles):
    try:
        mol = Chem.MolFromSmiles(smiles)
        clear_mapno(mol)
        res = Chem.MolToSmiles(mol)
    except:
        return ''
    return res

def apply_template(reaction_smarts, reaction_smiles, **kwarg):
    left, _, right = reaction_smarts.split('>')
    smarts = '({})>>{}'.format(left, right)
    reactants, _, products = reaction_smiles.split(' ')[0].split('>')
    return rdchiralRunText(smarts, products)

with open('./data/templates.1.3m.json', 'r') as f:
    reactions = json.load(f)

error_cnt = 0
no_outcome_cnt = 0
mismatch_cnt = 0
for reaction in reactions:
    results = apply_template(**reaction)
    reactants, _, products = reaction['reaction_smiles'].split(' ')[0].split('>')
    if canonicalize_smiles(reactants) not in [canonicalize_smiles(s) for s in results]:
        error_cnt += 1
        if len(results) == 0:
            no_outcome_cnt += 1
        else:
            mismatch_cnt += 1
        sys.stderr.write(str(reaction)+'\n')
        print("reaction: ", reaction)
        print("results: ", results)
        print()

print('Total records: ', len(reactions))
print('error_cnt: ', error_cnt)
print('no_outcome_cnt: ', no_outcome_cnt)
print('mismatch_cnt: ', mismatch_cnt)
