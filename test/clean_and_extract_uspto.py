# MIT License
#
# Copyright (c) 2017 Connor Coley
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# This test was originally obtained from
# https://github.com/connorcoley/rdchiral/blob/master/templates/clean_and_extract_uspto.py
# with modifications

import os
import json
import urllib.request
from time import time
from rdkit import Chem

from rdchiral import template_extractor
    
t0 = time()

path = 'data'
os.makedirs(path, exist_ok=True)

uspto_url = 'https://ndownloader.figshare.com/files/8664379'
uspto_fn = '1976_Sep2016_USPTOgrants_smiles'
uspto_fn_7z = os.path.join(path, uspto_fn+'.7z')
uspto_fn_rsmi = os.path.join(path, uspto_fn+'.rsmi')

# download uspto
if not os.path.exists(uspto_fn_7z):
    print("Downloading USPTO dataset")
    urllib.request.urlretrieve(uspto_url, uspto_fn_7z)

# unzip 7z
if not os.path.exists(uspto_fn_rsmi):
    print("Extracting USPTO dataset")
    try:
        import libarchive
    except:
        print("Error: Cannot import libarchive")
        print("Note: you can manually unpack the 7z file ({}) in ./data directory as {}, and then re-run this script.".format(uspto_fn_7z, uspto_fn_rsmi))
    with libarchive.file_reader(uspto_fn_7z) as entries:
        for entry in entries:
            with open(os.path.join(path, str(entry)), 'wb') as f:
                for block in entry.get_blocks():
                    f.write(block)

uspto = open(uspto_fn_rsmi, 'r')
uspto.readline() # skip csv header

output_fn = os.path.join(path, 'uspto.templates.txt')
print('output file: ', output_fn)
templates_results = open(output_fn, 'w')

reaction_seen = set()

for line in uspto:
    ReactionSmiles,PatentNumber,ParagraphNum,Year,TextMinedYield,CalculatedYield = line.split('\t')
    
    # remove duplicate
    if ReactionSmiles in reaction_seen:
        continue
    reaction_seen.add(ReactionSmiles)
    
    split_smiles = ReactionSmiles.split(' ')[0].split('>')
    reaction = {}
    reaction['_id'] = str(PatentNumber) + '_' + str(ParagraphNum)
    reaction['ReactionSmiles'] = str(ReactionSmiles)
    reaction['PatentNumber'] = PatentNumber
    reaction['ParagraphNum'] = ParagraphNum
    reaction['reactants'] = split_smiles[0]
    reaction['spectators'] = split_smiles[1]
    reaction['products'] = split_smiles[2]
    
    # substructure search for large molecules is very slow
    if len(reaction['ReactionSmiles']) > 500: continue
    
    try:
        r = Chem.MolFromSmiles(reaction['reactants'])
        p = Chem.MolFromSmiles(reaction['products'])
        if r is None or p is None:
            continue
    except:
        continue
    
    try:
        templates = template_extractor.extract_from_reaction(reaction)
    except KeyboardInterrupt:
        print('Interrupted')
        raise KeyboardInterrupt
    except Exception as e:
        print('reaction: ', reaction)
        print(e)
        continue
    
    if templates.get('reaction_smarts', None) is None:
        continue
    
    templates['reaction'] = reaction
    templates['reaction_smiles'] = reaction['ReactionSmiles']
    templates_results.write(json.dumps(templates)+'\n')

print('elapsed seconds: {}'.format(int(time()-t0)))

uspto.close()
templates_results.close()
