Template application testcases 1
------

The template application testcases were obtained from the [original python version of rdchiral](https://github.com/connorcoley/rdchiral/blob/master/test/test_rdchiral.py).

```
Testcases:       test_rdchiral_cases.json
C++ version:     test_run_reaction.cpp
Python version:  test_rdchiral.py
```

Template application testcases 2
------

Consistency testing for template application. Template extraction testcases generated below by `clean_and_extract_uspto.py` can be used.

```
C++ version:     test_run_reaction2.cpp
Python version:  test_run_reaction2.py
```

Template extraction
------

The template extraction testcases can be generated using `clean_and_extract_uspto.py`. The code was adapted from the [original version of rdchiral](https://github.com/connorcoley/rdchiral/blob/master/templates/clean_and_extract_uspto.py). The test can be run using the following programs.

```
C++ version:     test_extract_reaction.cpp
Python version:  test_extract_reaction.py
```
