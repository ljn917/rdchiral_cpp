#include <iostream>
#include <array>

#include "rdchiral/dfs.hpp"

void print(const auto& all_connected_nodes) {
    for(const auto& connected_nodes: all_connected_nodes) {
        for(const auto node_id: connected_nodes) {
            std::cout << node_id << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

template<typename Container>
void test_true(
    std::vector<Container> adj_mat,
    std::vector<std::vector<typename Container::value_type>> expected
) {
    const auto all_connected_nodes = rdchiral::dfs(adj_mat);
    print(all_connected_nodes);
    if(expected != all_connected_nodes) {
        std::cout << "failed" << std::endl;
        throw std::runtime_error{"failed test"};
    }
}

int main() {
    {
        // 0-1-2-3-8
        // 4-5-6-7
        // 9-10
        std::vector<std::vector<int>> expected = {
            {0, 1, 2, 3, 8},
            {4, 5, 6, 7},
            {9, 10},
        };
        std::vector<std::vector<int>> adj_mat = {
            {1},    // 0
            {0, 2}, // 1
            {1, 3}, // 2
            {2, 8}, // 3
            {5},    // 4
            {4, 6}, // 5
            {5, 7}, // 6
            {6},    // 7
            {3},    // 8
            {10},   // 9
            {9},    // 10
        };
        test_true(adj_mat, expected);
    }
    
    {
        // 0
        // 1-2-3-8
        // 4-5-6-7
        std::vector<std::vector<int>> expected = {
            {0},
            {1, 2, 3, 8},
            {4, 5, 6, 7},
        };
        std::vector<std::vector<int>> adj_mat = {
            {},     // 0
            {2},    // 1
            {1, 3}, // 2
            {2, 8}, // 3
            {5},    // 4
            {4, 6}, // 5
            {5, 7}, // 6
            {6},    // 7
            {3},    // 8
        };
        
        test_true(adj_mat, expected);
    }
    
    {
        // 0-1-2-3-8
        // 4-5-6-7
        std::vector<std::vector<int>> expected = {
            {0, 1, 2, 3, 8},
            {4, 5, 6, 7},
        };
        std::vector<std::array<int, 2>> adj_mat = {
            {1, -1}, // 0
            {0,  2}, // 1
            {1,  3}, // 2
            {2,  8}, // 3
            {5, -1}, // 4
            {4,  6}, // 5
            {5,  7}, // 6
            {6, -1}, // 7
            {3, -1}, // 8
        };
        
        test_true(adj_mat, expected);
    }
    
    return 0;
}
