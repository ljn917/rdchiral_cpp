#include <iostream>
#include <string>
#include <vector>
#include <tuple>

#include <GraphMol/SmilesParse/SmilesParse.h>

#include <rdchiral/chiral.hpp>

int main() {
    // smiles1, smiles2, mapno, expected res
    std::vector<std::tuple<std::string, std::string, int, int>> chiral_list = {
        {"[F:2][C@H:1]([Cl:3])[Br:4]", "[F:2][C@@H:1]([Br:4])[Cl:3]", 1, 1}
    };
    
    bool correct{true};
    for(const auto& [s1, s2, mapno, expected]: chiral_list) {
        bool this_correct{true};
        
        auto mol1 = RDKit::RWMOL_SPTR(RDKit::SmilesToMol(s1));
        auto mol2 = RDKit::RWMOL_SPTR(RDKit::SmilesToMol(s2));
        
        for(const auto atom1: mol1->atoms()) {
            auto mapno1 = atom1->getAtomMapNum();
            if(mapno1 != mapno) continue;
            
            for(const auto atom2: mol2->atoms()) {
                auto mapno2 = atom2->getAtomMapNum();
                if(mapno2 != mapno) continue;
                
                if(mapno1 == mapno2) {
                    auto res = rdchiral::atom_chirality_matches(atom1, atom2);
                    if(res != expected) {
                        std::cout << s1 << ", " << s2 << ": "
                        << "res=" << res << ", expected=" << expected << std::endl;
                        this_correct = false;
                    }
                }
            }
        }
        
        correct = correct && this_correct;
    }
    
    std::cout << (correct?"Pass":"Failed") << std::endl;
    
    return correct ? 0 : 1;
}
