from time import time
import json
from rdkit import Chem

from rdchiral import template_extractor

t0 = time()

input_file = open('data/uspto.templates.txt')

for line in input_file:
    line = line.strip()
    data = json.loads(line)
    reaction = data['reaction']
    
    try:
        templates = template_extractor.extract_from_reaction(reaction)
    except KeyboardInterrupt:
        print('Interrupted')
        raise KeyboardInterrupt
    except Exception as e:
        # should not happen
        print('reaction: ', reaction)
        print(e)
        continue
    
    print("data: ", data)
    print("templates: ", templates)

input_file.close()

print('elapsed seconds: {}'.format(int(time()-t0)))
