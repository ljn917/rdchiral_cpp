#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <chrono>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "rdchiral/rdchiral.hpp"

int main(int argc, char* argv[]) {
    std::string fn{"../test/test_rdchiral_cases.json"};
    bool relax_chiral_match{false};
    bool verbose{false};
    
    for(int i = 1; i < argc; i++) {
        const auto cmd_flag = std::string{argv[i]};
        if(cmd_flag == "-v") {
            verbose = true;
        } else if(cmd_flag == "-r") {
            relax_chiral_match = true;
        } else if(cmd_flag == "-f") {
            fn = argv[++i];
        } else {
            std::cout << "Unrecognized command line argument: " << cmd_flag << std::endl;
            std::cout << "Usage: test_run_reaction [-v] [-f] test_case_filename" << std::endl;
            return -1;
        }
    }
    
    std::cout << "test case file: " << fn << std::endl;
    std::cout << "relax_chiral_match: " << relax_chiral_match << std::endl;
    std::cout << "verbose: " << verbose << std::endl;
    std::cout << std::endl;
    
    boost::property_tree::ptree root;
    boost::property_tree::read_json(fn, root);
    
    bool all_correct{true};
    int correct_cnt{0}, record_cnt{0};
    
    auto time_start = std::chrono::high_resolution_clock::now();
    
    for(auto record: root) {
        std::cout << "Test case: " << record_cnt << std::endl;
        
        auto smarts = record.second.get<std::string>("smarts");
        auto smiles = record.second.get<std::string>("smiles");
        std::vector<std::string> expected;
        for(auto s: record.second.get_child("expected")) {
            expected.push_back(s.second.get_value<std::string>());
        }
        std::sort(expected.begin(), expected.end());
        auto description = record.second.get<std::string>("description");
        std::cout << "smarts: " << smarts << std::endl;
        std::cout << "smiles: " << smiles << std::endl;
        std::cout << "expected: " << std::endl;
        for(auto s: expected) {
            std::cout << s << std::endl;
        }
        std::cout << "description: " << description << std::endl;
        
        bool is_correct{true};
        
        // repeated run
        {
            auto reaction = rdchiral::Reaction(smarts);
            auto reactants = rdchiral::Reactants(smiles);
            std::cout << "reactants.smiles(): " << reactants.smiles() << std::endl;
            for(int n = 0; n < 3; n++) {
                auto [outcomes, mapped_outcomes] = reaction.run(
                    reactants,
                    false, // keep_mapnums
                    true, // combine_enantiomers
                    relax_chiral_match, // relax_chiral_match
                    verbose // verbose
                );
                std::sort(outcomes.begin(), outcomes.end());
                is_correct = is_correct && outcomes == expected;
            }
        }
        
        // run with text wrapper
        {
            auto [outcomes, mapped_outcomes] = rdchiral::Reaction::run(
                smarts,
                smiles,
                false, // keep_mapnums
                true, // combine_enantiomers
                relax_chiral_match, // relax_chiral_match
                verbose // verbose
            );
            std::sort(outcomes.begin(), outcomes.end());
                
            std::cout << "outcomes: " << std::endl;
            for(auto i: outcomes) {
                std::cout << i << std::endl;
            }
            
            std::cout << "outcomes mapped: " << std::endl;
            for(auto [k, v]: mapped_outcomes) {
                std::cout << k << ": " << v.first << ", changed atoms: ";
                for(auto i: v.second) {
                    std::cout << i << " ";
                }
                std::cout << std::endl;
            }
            
            is_correct = is_correct && outcomes == expected;
        }
        
        all_correct = all_correct && is_correct;
        if(is_correct) correct_cnt++;
        record_cnt++;
        
        std::cout << "correct: " << (is_correct?"true":"false") << std::endl;
        std::cout << std::endl;
    }
    
    auto time_end = std::chrono::high_resolution_clock::now();
    
    std::cout << "Test passes: " << correct_cnt << "/" << record_cnt << std::endl;
    std::cout << "Final results: " << (all_correct?"All passed":"Failed") << std::endl;
    
    auto execution_duration = std::chrono::duration_cast<std::chrono::milliseconds>(time_end - time_start);
    std::cout << "Time used: " << execution_duration.count() << " milliseconds" << std::endl;
    
    return all_correct ? 0 : 1;
}
