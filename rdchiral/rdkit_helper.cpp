/*
    BSD 3-Clause License

    Copyright (c) 2006-2020, Rational Discovery LLC, Greg Landrum, and Julie Penzotti and others
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "rdchiral/rdkit_helper.hpp"

#include <GraphMol/MolOps.h>
#include <GraphMol/QueryAtom.h>
#include <GraphMol/QueryBond.h>
#include <GraphMol/SmilesParse/SmartsWrite.h>
#include <GraphMol/SmilesParse/SmilesWrite.h>

namespace rdchiral {

std::string AtomGetSmarts(const RDKit::Atom *atom, bool doKekule, bool allHsExplicit,
                          bool isomericSmiles) {
    std::string res;
    if(atom->hasQuery()) {
        res = RDKit::SmartsWrite::GetAtomSmarts(static_cast<const RDKit::QueryAtom *>(atom));
    } else {
        // FIX: this should not be necessary
        res = RDKit::SmilesWrite::GetAtomSmiles(atom, doKekule, nullptr, allHsExplicit,
                                        isomericSmiles);
    }
    return res;
}

std::vector<RDKit::Bond*> AtomGetBonds(const RDKit::Atom *atom) {
    std::vector<RDKit::Bond*> res;
    res.reserve(8);
    RDKit::ROMol *parent = &atom->getOwningMol();
    auto [begin, end] = parent->getAtomBonds(atom);
    while (begin != end) {
        RDKit::Bond *tmpB = (*parent)[*begin];
        res.push_back(tmpB);
        begin++;
    }
    return res;
}

std::vector<RDKit::Atom*> AtomGetNeighbors(const RDKit::Atom *atom) {
  std::vector<RDKit::Atom*> res;
  res.reserve(8);
  RDKit::ROMol *parent = &atom->getOwningMol();
  //ROMol::ADJ_ITER begin, end;
  //boost::tie(begin, end) = parent->getAtomNeighbors(atom);
  auto [begin, end] = parent->getAtomNeighbors(atom);
  while (begin != end) {
    res.push_back(parent->getAtomWithIdx(*begin));
    begin++;
  }
  return res;
}

bool BondIsInRing(const RDKit::Bond *bond) {
    if (!bond->getOwningMol().getRingInfo()->isInitialized()) {
        RDKit::MolOps::findSSSR(bond->getOwningMol());
    }
    return bond->getOwningMol().getRingInfo()->numBondRings(bond->getIdx()) != 0;
}

std::string BondGetSmarts(const RDKit::Bond *bond, bool allBondsExplicit) {
  std::string res;
  if (bond->hasQuery()) {
    res = RDKit::SmartsWrite::GetBondSmarts(static_cast<const RDKit::QueryBond *>(bond));
  } else {
    res = RDKit::SmilesWrite::GetBondSmiles(bond, -1, false, allBondsExplicit);
  }
  return res;
}

};
