#include <regex>
#include <vector>
#include <tuple>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>
#include <exception>
#include <iostream>
#include <cmath>
#include <mutex>

#include <boost/algorithm/string.hpp>

#include <GraphMol/GraphMol.h>
#include <GraphMol/SmilesParse/SmilesWrite.h>
#include <GraphMol/SmilesParse/SmilesParse.h>
#include <GraphMol/SmilesParse/SmartsWrite.h>
#include <GraphMol/MolOps.h>
#include <GraphMol/Substruct/SubstructMatch.h>
#include <GraphMol/ChemReactions/ReactionParser.h>
#include <GraphMol/PeriodicTable.h>

#include "rdchiral/rdkit_helper.hpp"
#include "rdchiral/template_extractor.hpp"
#include "rdchiral/bonds.hpp"
#include "rdchiral/smarts_util.hpp"

namespace rdchiral {

std::vector<RDKit::RWMOL_SPTR> mols_from_smiles_list(const std::vector<std::string>& all_smiles) {
    // Given a list of smiles strings, this function creates rdkit molecules
    std::vector<RDKit::RWMOL_SPTR> mols;
    mols.reserve(all_smiles.size());
    
    for(auto smiles: all_smiles) {
        if(smiles.empty()) continue;
        auto mol = RDKit::SmilesToMol(smiles);
        if(mol == nullptr) throw std::runtime_error{"Cannot parse SMILES. RDKit::SmilesToMol returned null."};
        mols.emplace_back(mol);
    }
    return mols;
}

std::string replace_deuterated(const std::string& s) {
    static std::regex reg_expr(R"(\[2H\])");
    return std::regex_replace(s, reg_expr, R"([H])", std::regex_constants::match_any);
}

// Takes an RDKit molecule and returns list of tagged atoms and their corresponding numbers
std::tuple<std::vector<RDKit::Atom*>, std::vector<int>>
get_tagged_atoms_from_mol(const RDKit::RWMOL_SPTR& mol) {
    std::vector<RDKit::Atom*> atoms;
    std::vector<int> atom_tags;
    atoms.reserve(mol->getNumAtoms());
    atom_tags.reserve(mol->getNumAtoms());
    
    for(auto atom: mol->atoms()) {
        int mapno = atom->getAtomMapNum();
        if(mapno != 0) {
            atoms.push_back(atom);
            atom_tags.push_back(mapno);
        }
    }
    return std::tuple(atoms, atom_tags);
}

std::tuple<std::vector<RDKit::Atom*>, std::vector<int>>
get_tagged_atoms_from_mols(const std::vector<RDKit::RWMOL_SPTR>& mols) {
    // Takes a list of RDKit molecules and returns total list of atoms and their tags
    std::vector<RDKit::Atom*> atoms;
    std::vector<int> atom_tags;
    
    for(auto mol: mols) {
        auto [new_atoms, new_atom_tags] = get_tagged_atoms_from_mol(mol);
        atoms.reserve(atoms.size()+new_atoms.size());
        atom_tags.reserve(atom_tags.size()+new_atom_tags.size());
        atoms.insert(atoms.end(), new_atoms.begin(), new_atoms.end());
        atom_tags.insert(atom_tags.end(), new_atom_tags.begin(), new_atom_tags.end());
    }
    return std::tuple(atoms, atom_tags);
}

// atom_mapno -> (reactant_atom, product_atom)
// missing atom is nullptr
std::unordered_map<int, std::pair<RDKit::Atom*, RDKit::Atom*>>
build_atom_tags_map(
    const std::vector<RDKit::Atom*>&    reactants_atoms,
    const std::vector<int>&             reactants_atom_tags,
    const std::vector<RDKit::Atom*>&    products_atoms,
    const std::vector<int>&             products_atom_tags,
    const bool verbose
) {
    std::unordered_map<int, std::pair<RDKit::Atom*, RDKit::Atom*>> res;
    res.reserve(reactants_atoms.size());
    
    if( reactants_atoms.size() != reactants_atom_tags.size() || 
        products_atoms.size() != products_atom_tags.size()
    ) {
        // throw std::runtime_error{"build_atom_tags_map(): size mismatches"};
        if(verbose) {
            std::cout << "build_atom_tags_map(): size mismatches, reactants_atoms.size()="
            << reactants_atoms.size() << ", products_atoms.size(): " << products_atoms.size() << std::endl;
        }
    }
    
    for(size_t i = 0; i < reactants_atoms.size(); i++) {
        if(res.contains(reactants_atom_tags[i])) {
            //throw std::runtime_error{"build_atom_tags_map(): duplicated atom mappings in reactants"};
            if(verbose) std::cout << "warning: build_atom_tags_map(): duplicated atom mappings in reactants" << std::endl;
        } else {
            res[reactants_atom_tags[i]] = std::pair<RDKit::Atom*, RDKit::Atom*>(reactants_atoms[i], nullptr);
        }
    }
    
    for(size_t i = 0; i < products_atoms.size(); i++) {
        if(res.contains(products_atom_tags[i])) {
            auto a = res[products_atom_tags[i]];
            if(a.second != nullptr) {
                // throw std::runtime_error{"build_atom_tags_map(): duplicated atom mappings in products"};
                if(verbose) std::cout << "warning: build_atom_tags_map(): duplicated atom mappings in products" << std::endl;
            } else {
                a.second = products_atoms[i];
                res[products_atom_tags[i]] = a;
            }
        } else {
            res[products_atom_tags[i]] = std::pair<RDKit::Atom*, RDKit::Atom*>(nullptr, products_atoms[i]);
        }
    }
    
    return res;
}

std::vector<std::tuple<int, RDKit::Atom*, RDKit::Atom*>>
get_tetrahedral_atoms(
    const std::unordered_map<int, std::pair<RDKit::Atom*, RDKit::Atom*>>& atom_tags_map
) {
    std::vector<std::tuple<int, RDKit::Atom*, RDKit::Atom*>> tetrahedral_atoms;
    
    for(auto [mapno, atoms]: atom_tags_map) {
        if(atoms.first == nullptr || atoms.second == nullptr) continue;
        
        if( atoms.first->getChiralTag()  != RDKit::Atom::CHI_UNSPECIFIED || \
            atoms.second->getChiralTag() != RDKit::Atom::CHI_UNSPECIFIED
        ) {
            tetrahedral_atoms.emplace_back(mapno, atoms.first, atoms.second);
        }
    }
    
    return tetrahedral_atoms;
}

// Builds a MolFragment using neighbors of a tetrahedral atom,
// where the molecule has already been updated to include isotopes
std::string get_frag_around_tetrahedral_center(const RDKit::RWMol& mol, const int idx) {
    std::vector<int> ids_to_include;
    ids_to_include.reserve(8);
    ids_to_include.push_back(idx);
    for(auto neighbor: AtomGetNeighbors(mol.getAtomWithIdx(idx))) {
        ids_to_include.push_back(neighbor->getIdx());
    }
    
    std::vector<std::string> symbols;
    symbols.reserve(mol.getNumAtoms());
    for(auto a: mol.atoms()) {
        std::string tmp;
        if(a->getIsotope() != 0) {
            tmp = std::string{"["} + std::to_string(a->getIsotope()) + a->getSymbol() + "]";
        } else {
            tmp = std::string{"[#"} + std::to_string(a->getAtomicNum()) + "]";
        }
        symbols.push_back(tmp);
    }
    
    // std::string MolFragmentToSmiles(
    // const ROMol &mol, const std::vector<int> &atomsToUse,
    // const std::vector<int> *bondsToUse = 0,
    // const std::vector<std::string> *atomSymbols = 0,
    // const std::vector<std::string> *bondSymbols = 0,
    // bool doIsomericSmiles = true, bool doKekule = false, int rootedAtAtom = -1,
    // bool canonical = true, bool allBondsExplicit = false,
    // bool allHsExplicit = false);
    return RDKit::MolFragmentToSmiles(
        mol, ids_to_include, // mol, atomsToUse
        0, // bondsToUse
        &symbols, // atomSymbols
        0, // bondSymbols
        true, // doIsomericSmiles
        false, // doKekule
        -1, // rootedAtAtom
        true, // canonical
        true, // allBondsExplicit
        true // allHsExplicit
    );
}

// Checks to see if tetrahedral centers are equivalent in
// chirality, ignoring the ChiralTag. Owning molecules of the
// input atoms must have been Isotope-mapped
bool check_tetrahedral_centers_equivalent(const RDKit::Atom *atom1, const RDKit::Atom *atom2) {
    auto atom1_frag = get_frag_around_tetrahedral_center(dynamic_cast<RDKit::RWMol&>(atom1->getOwningMol()), atom1->getIdx());
    RDKit::RWMOL_SPTR atom1_neighborhood = RDKit::RWMOL_SPTR(RDKit::SmilesToMol(atom1_frag, 0, false)); // sanitize=False
    
    auto params = RDKit::SubstructMatchParameters();
    params.uniquify = true;
    params.recursionPossible = true;
    params.useChirality = true;
    params.useQueryQueryMatches = false;
    params.maxMatches = 1000;
    params.numThreads = 1;
    // std::vector<RDKit::MatchVectType>, MatchVectType = std::vector<std::pair<int, int>>
    auto matchVect = RDKit::SubstructMatch(atom2->getOwningMol(), *atom1_neighborhood, params);
    for(auto matched_ids: matchVect) {
        for(auto idx: matched_ids) {
            if(static_cast<int>(atom2->getIdx()) == idx.second) {
                return true;
            }
        }
    }
    return false;
}

std::unordered_map<int, RDKit::Atom*>
get_changed_atoms(
    const std::vector<RDKit::RWMOL_SPTR>& reactants,
    const std::vector<RDKit::RWMOL_SPTR>& products,
    const bool verbose
) {
    std::unordered_map<int, RDKit::Atom*> changed_atom_tags; // changed reactant atoms
    changed_atom_tags.reserve(64);
    
    auto [reac_atoms, reac_atom_tags] = get_tagged_atoms_from_mols(reactants);
    auto [prod_atoms, prod_atom_tags] = get_tagged_atoms_from_mols(products);
    
    std::unordered_set<int> reac_atom_tags_dup, prod_atom_tags_dup;
    reac_atom_tags_dup.reserve(64);
    prod_atom_tags_dup.reserve(64);
    {
        std::unordered_set<int> reac_atom_tags_flags;
        reac_atom_tags_flags.reserve(reac_atom_tags.size());
        for(auto i: reac_atom_tags) {
            if(reac_atom_tags_flags.contains(i)) {
                reac_atom_tags_dup.insert(i);
            } else {
                reac_atom_tags_flags.insert(i);
            }
        }
        
        std::unordered_set<int> prod_atom_tags_flags;
        prod_atom_tags_flags.reserve(prod_atom_tags.size());
        for(auto i: prod_atom_tags) {
            if(prod_atom_tags_flags.contains(i)) {
                prod_atom_tags_dup.insert(i);
            } else {
                prod_atom_tags_flags.insert(i);
            }
        }
    }
    
    if(verbose) {
        std::cout << "Products contain " << prod_atoms.size() << " tagged atoms." << std::endl;
        std::cout << "Products contain " << prod_atom_tags.size() - prod_atom_tags_dup.size()
                  << " unique atom numbers." << std::endl;
        if(prod_atom_tags.size() - prod_atom_tags_dup.size() != reac_atom_tags.size() - reac_atom_tags_dup.size())
            std::cout << "warning: different atom tags appear in reactants and products" << std::endl;
        if(prod_atoms.size() != reac_atoms.size())
            std::cout << "warning: total number of tagged atoms differ, stoichometry != 1?" << std::endl;
    }
    
    if(reac_atom_tags_dup.size() > 0) {
        throw std::runtime_error{"get_changed_atoms(): Reactants have non-unique atom mappings."};
    }
    
     // map of int mapno -> (Atom*, Atom*)
    auto atom_tags_map = build_atom_tags_map(reac_atoms, reac_atom_tags, prod_atoms, prod_atom_tags, verbose);
    
    // *NOTE*: we assume that atoms in reactants all have different mappings
    // Product atoms that are different from reactant atom equivalent
    for(auto [reac_tag, reac_prod_pair]: atom_tags_map) {
        if(!reac_prod_pair.first || !reac_prod_pair.second) continue;
        
        if(!changed_atom_tags.contains(reac_tag)) { // don't bother comparing if we know this atom changes
            // If atom changed, add
            if(atoms_are_different(reac_prod_pair.second, reac_prod_pair.first, false)) {
                if(verbose) {
                    std::cout << "atoms_are_different: " << AtomGetSmarts(reac_prod_pair.first) << "|";
                    for(auto bond: AtomGetBonds(reac_prod_pair.first))
                        std::cout << bond_to_label(bond) << "|";
                    std::cout <<  std::endl;
                    std::cout << "                     " << AtomGetSmarts(reac_prod_pair.second) << "|";
                    for(auto bond: AtomGetBonds(reac_prod_pair.second))
                        std::cout << bond_to_label(bond) << "|";
                    std::cout <<  std::endl;
                }
                changed_atom_tags.emplace(reac_tag, reac_prod_pair.first);
            }
            // If reac_tag appears multiple times, add (need for stoichometry > 1)
            if(prod_atom_tags_dup.contains(reac_tag)) {
                if(verbose) std::cout << "prod_atom_tags_dup: " << AtomGetSmarts(reac_prod_pair.first) << std::endl;
                changed_atom_tags.emplace(reac_tag, reac_prod_pair.first);
            }
        }
    }
    if(verbose) {
        std::cout << "get_changed_atoms(): stage1, tagged atoms: " << changed_atom_tags.size() << std::endl;
        for(auto [atom_tag, atom]: changed_atom_tags) {
            auto smarts = AtomGetSmarts(atom);
            std::cout << smarts << "  ";
        }
        std::cout << std::endl;
    }
    
    // Reactant atoms that do not appear in product (tagged leaving groups)
    for(auto [reac_tag, reac_prod_pair]: atom_tags_map) {
        if(reac_prod_pair.second == nullptr) {
            changed_atom_tags.emplace(reac_tag, reac_prod_pair.first);
        }
    }
    if(verbose) {
        std::cout << "get_changed_atoms(): stage2, tagged atoms: " << changed_atom_tags.size() << std::endl;
        for(auto [atom_tag, atom]: changed_atom_tags) {
            auto smarts = AtomGetSmarts(atom);
            std::cout << smarts << "  ";
        }
        std::cout << std::endl;
    }
    
    // Atoms that change CHIRALITY (just tetrahedral for now...)
    auto tetra_atoms = get_tetrahedral_atoms(atom_tags_map); // tuple of int mapno, Atom*, Atom*)
    if(verbose)
        std::cout << "Found " << tetra_atoms.size()
                  << " atom-mapped tetrahedral atoms that have chirality specified at least partially" << std::endl;
    for(auto mol: reactants) {
        set_isotope_to_equal_mapnum(mol);
    }
    for(auto mol: products) {
        set_isotope_to_equal_mapnum(mol);
    }
    
    // dirty hack for BFS
    int _cnt_changed_atom = -1;
    for(size_t i = 0; i < tetra_atoms.size() && _cnt_changed_atom != static_cast<int>(changed_atom_tags.size()); i++) {
        _cnt_changed_atom = changed_atom_tags.size();
        if(verbose) std::cout << "Round " << i << std::endl;
        for(auto [atom_tag, ar, ap]: tetra_atoms) {
            if(verbose) {
                std::cout << "For atom tag " << atom_tag << std::endl;
                std::cout << "    reactant: " << ar->getChiralTag() << ", " << AtomGetSmarts(ar) << std::endl;
                std::cout << "    product:  " << ap->getChiralTag() << ", " << AtomGetSmarts(ap) << std::endl;
            }
            if(changed_atom_tags.contains(atom_tag)) {
                if(verbose) std::cout << "-> atoms have changed (by more than just chirality!)" << std::endl;
            } else {
                bool unchanged = check_tetrahedral_centers_equivalent(ar, ap) && \
                    RDKit::Atom::CHI_UNSPECIFIED != ar->getChiralTag() && \
                    RDKit::Atom::CHI_UNSPECIFIED != ap->getChiralTag();
                if(unchanged) {
                    if(verbose) std::cout << "-> atoms confirmed to have same chirality, no change" << std::endl;
                } else {
                    if(verbose) std::cout << "-> atom changed chirality!!" << std::endl;
                    // Make sure chiral change is next to the reaction center and not
                    // a random specification (must be CONNECTED to a changed atom)
                    bool tetra_adj_to_rxn{false};
                    for(auto neighbor: AtomGetNeighbors(ap)) {
                        int mapno = neighbor->getAtomMapNum();
                        if(mapno != 0) {
                            if(changed_atom_tags.contains(mapno)) {
                                tetra_adj_to_rxn = true;
                                break;
                            }
                        }
                    }
                    if(tetra_adj_to_rxn) {
                        changed_atom_tags.emplace(atom_tag, ar);
                        if(verbose) std::cout << "-> atom adj to reaction center, now included" << std::endl;
                    } else {
                        if(verbose) std::cout << "-> adj far from reaction center, not including" << std::endl;
                    }
                }
            }
        }
    }
    for(auto mol: reactants) {
        clear_isotope(mol);
    }
    for(auto mol: products) {
        clear_isotope(mol);
    }

    if(verbose) {
        std::cout << changed_atom_tags.size() << " tagged atoms in reactants change 1-atom properties" << std::endl;
        for(auto [atom_tag, atom]: changed_atom_tags) {
            auto smarts = AtomGetSmarts(atom);
            std::cout << smarts << "  ";
        }
        std::cout << std::endl;
    }
    return changed_atom_tags;
}

const std::vector<std::pair<std::vector<int>,RDKit::RWMOL_SPTR>>& get_default_special_groups() {
    // Define templates
    const static std::vector<std::pair<std::vector<int>,std::string>> group_templates_smarts = {
        {{0,1,2}, R"([OH0,SH0]=C[O,Cl,I,Br,F])"}, // carboxylic acid / halogen
        {{0,1,2}, R"([OH0,SH0]=CN)"}, // amide/sulfamide
        {{0,1,2,3}, R"(S(O)(O)[Cl])"}, // sulfonyl chloride
        {{0,1,2}, R"(B(O)O)"}, // boronic acid/ester
        {{0}, R"([Si](C)(C)C)"}, // trialkyl silane
        {{0}, R"([Si](OC)(OC)(OC))"}, // trialkoxy silane, default to methyl
        {{0,1,2}, R"([N;H0;$(N-[#6]);D2]-,=[N;D2]-,=[N;D1])"}, // azide
        {{0,1,2,3,4,5,6,7}, R"(O=C1N([Br,I,F,Cl])C(=O)CC1)"}, // NBS brominating agent
        {{0,1,2,3,4,5,6,7,8,9,10}, R"(Cc1ccc(S(=O)(=O)O)cc1)"}, // Tosyl
        {{7}, R"(CC(C)(C)OC(=O)[N])"}, // N(boc)
        {{4}, R"([CH3][CH0]([CH3])([CH3])O)"}, // 
        {{0,1}, R"([C,N]=[C,N])"}, // alkene/imine
        {{0,1}, R"([C,N]#[C,N])"}, // alkyne/nitrile
        {{2}, R"(C=C-[*])"}, // adj to alkene
        {{2}, R"(C#C-[*])"}, // adj to alkyne
        {{2}, R"(O=C-[*])"}, // adj to carbonyl
        {{3}, R"(O=C([CH3])-[*])"}, // adj to methyl ketone
        {{3}, R"(O=C([O,N])-[*])"}, // adj to carboxylic acid/amide/ester
        {{0,1,2,3}, R"(ClS(Cl)=O)"}, // thionyl chloride
        {{0,1}, R"([Mg,Li,Zn,Sn][Br,Cl,I,F])"}, // grinard/metal (non-disassociated)
        {{0,1,2}, R"(S(O)(O))"}, // SO2 group
        {{0,1}, R"(N~N)"}, // diazo
        {{1}, R"([!#6;R]@[#6;R])"}, // adjacency to heteroatom in ring
        {{2}, R"([a!c]:a:a)"}, // two-steps away from heteroatom in aromatic ring
        //{{1}, R"(c(-,=[*]):c([Cl,I,Br,F]))"}, // ortho to halogen on ring - too specific?
        //{{1}, R"(c(-,=[*]):c:c([Cl,I,Br,F]))"}, // meta to halogen on ring - too specific?
        {{0}, R"([B,C](F)(F)F)"}, // CF3, BF3 should have the F3 included
        // Stereo-specific ones (where we will need to include neighbors)
        // Tetrahedral centers should already be okay...
        {{1,2}, R"([*]/[CH]=[CH]/[*])"}, // trans with two hydrogens
        {{1,2}, R"([*]/[CH]=[CH]\[*])"}, // cis with two hydrogens
        {{1,2}, R"([*]/[CH]=[CH0]([*])\[*])"}, // trans with one hydrogens
        {{1,2}, R"([*]/[D3;H1]=[!D1])"} // specified on one end, can be N or C
    };

    static std::once_flag is_init;
    static std::vector<std::pair<std::vector<int>,RDKit::RWMOL_SPTR>> group_templates_mol;
    // init table
    std::call_once(
        is_init,
        [&]() {
            group_templates_mol.reserve(group_templates_smarts.size());
            for(auto [n, template_smarts]: group_templates_smarts) {
                group_templates_mol.push_back(std::pair(n, RDKit::RWMOL_SPTR(RDKit::SmartsToMol(template_smarts))));
            }
        }
    );
    
    return group_templates_mol;
}

/*
Given an RDKit molecule, this function returns a list of tuples, where
each tuple contains the AtomIdx's for a special group of atoms which should 
be included in a fragment all together. This should only be done for the 
reactants, otherwise the products might end up with mapping mismatches

We draw a distinction between atoms in groups that trigger that whole
group to be included, and "unimportant" atoms in the groups that will not
be included if another atom matches.
*/
std::vector<
    std::pair<
        std::vector<int>,
        std::vector<int>
    >
>
get_special_groups(
    const RDKit::RWMOL_SPTR& mol,
    const bool use_custom_special_groups,
    const std::vector<std::pair<std::vector<int>,RDKit::RWMOL_SPTR>>& special_groups,
    const bool verbose
) {
    const auto* group_templates_mol = &special_groups;
    if(!use_custom_special_groups) {
        group_templates_mol = &get_default_special_groups();
    }
    
    // Build list
    std::vector<std::pair<std::vector<int>,std::vector<int>>> groups;
    groups.reserve(16);
    auto params = RDKit::SubstructMatchParameters();
    params.uniquify = true;
    params.recursionPossible = true;
    params.useChirality = true;
    params.useQueryQueryMatches = false;
    params.maxMatches = 1000;
    params.numThreads = 1;
    
    for(auto [add_if_match, template_mol]: *group_templates_mol) {
        // std::vector<RDKit::MatchVectType>, MatchVectType = std::vector<std::pair<int, int>>
        auto matches = RDKit::SubstructMatch(*mol, *template_mol, params);
        // if(verbose) std::cout << "matching template: " << RDKit::MolToSmarts(*template_mol) << std::endl;
        for(auto match: matches) {
            if(verbose) std::cout << "matched template: " << RDKit::MolToSmarts(*template_mol) << std::endl;
            std::vector<int> add_if, match_ids;
            add_if.reserve(16);
            match_ids.reserve(16);
            for(auto [pattern_idx, atom_idx]: match) {
                match_ids.push_back(atom_idx);
                if(std::find(add_if_match.begin(), add_if_match.end(), pattern_idx) != add_if_match.end()) {
                    add_if.push_back(atom_idx);
                }
            }
            groups.push_back(std::pair(add_if, match_ids));
        }
    }
    if(verbose && groups.size() > 0) {
        std::cout << "mol: " << RDKit::MolToSmiles(*mol) << std::endl;
        std::cout << "get_special_groups(): size=" << groups.size() << std::endl;
        for(auto group: groups) {
            std::cout << "add_if,    size=" << group.first.size() << ": ";
            for(auto i: group.first) std::cout << i << ", ";
            std::cout << std::endl;
            std::cout << "match_ids, size=" << group.second.size() << ": ";
            for(auto i: group.second) std::cout << i << ", ";
            std::cout << std::endl;
        }
    }
    return groups;
}

// Given an RDKit molecule and a list of AtomIdx which should be included
// in the reaction, this function extends the list of atoms_to_use by considering 
// a candidate atom extension, atom_idx
std::pair<
    std::vector<int>,
    std::vector<std::pair<int, std::string>>
>
expand_atoms_to_use_atom(
    const RDKit::RWMOL_SPTR& mol,
    std::vector<int> atoms_to_use, // must by value
    const int atom_idx,
    const std::vector<
        std::pair<
            std::vector<int>,
            std::vector<int>
        >
    >& groups,
    std::vector<std::pair<int, std::string>> symbol_replacements, // must be by value
    const bool verbose
) {
    // See if this atom belongs to any special groups (highest priority)
    bool found_in_group = false;
    for(auto group: groups) { // first index is atom IDs for match, second is what to include
        if(group.first.end() != std::find(group.first.begin(), group.first.end(), atom_idx)) {
            if(verbose) {
                std::cout << "expand_atoms_to_use_atom: adding group due to match" << std::endl;
                std::cout << "Match from molAtomMapNum "
                    << mol->getAtomWithIdx(atom_idx)->getAtomMapNum() << std::endl;
            }
            // Add the whole list, redundancies don't matter 
            // *but* still call convert_atom_to_wildcard!
            for(auto idx: group.second) {
                if(std::find(atoms_to_use.begin(), atoms_to_use.end(), idx) == atoms_to_use.end()) {
                    atoms_to_use.push_back(idx);
                    symbol_replacements.emplace_back(idx, convert_atom_to_wildcard(mol->getAtomWithIdx(idx), verbose));
                }
            }
            found_in_group = true;
        }
    }
    if(found_in_group)
        return std::pair(atoms_to_use, symbol_replacements);
        
    // How do we add an atom that wasn't in an identified important functional group?
    // Develop generalized SMARTS symbol

    // Skip current candidate atom if it is already included
    if(std::find(atoms_to_use.begin(), atoms_to_use.end(), atom_idx) != atoms_to_use.end())
        return std::pair(atoms_to_use, symbol_replacements);

    // Include this atom
    atoms_to_use.push_back(atom_idx);

    // Look for suitable SMARTS replacement
    symbol_replacements.emplace_back(atom_idx, convert_atom_to_wildcard(mol->getAtomWithIdx(atom_idx), verbose));

    return std::pair(atoms_to_use, symbol_replacements);
}

// Given an RDKit molecule and a list of AtomIdX which should be included
// in the reaction, this function expands the list of AtomIdXs to include one
// nearest neighbor with special consideration of (a) unimportant neighbors and
// (b) important functional groupings'''
std::pair<
    std::vector<int>,
    std::vector<std::pair<int, std::string>>
>
expand_atoms_to_use(
    const RDKit::RWMOL_SPTR& mol,
    const std::vector<int>& atoms_to_use,
    const std::vector<
        std::pair<
            std::vector<int>,
            std::vector<int>
        >
    >& groups,
    std::vector<std::pair<int, std::string>> symbol_replacements, // must be by value
    const bool verbose
) {
    // Copy
    std::vector<int> new_atoms_to_use(atoms_to_use);
    
    // Look for all atoms in the current list of atoms to use
    for(auto atom: mol->atoms()) {
        if(atoms_to_use.end() == std::find(atoms_to_use.begin(), atoms_to_use.end(), atom->getIdx())) {
            continue;
        }
        // Ensure membership of changed atom is checked against group
        for(auto group: groups) {
            if(group.first.end() != std::find(group.first.begin(), group.first.end(), atom->getIdx())) {
                if(verbose) {
                    std::cout << "expand_atoms_to_use: adding group due to match" << std::endl;
                    std::cout << "Match from molAtomMapNum "
                        << atom->getAtomMapNum() << std::endl;
                }
                for(auto idx: group.second) {
                    if(std::find(atoms_to_use.begin(), atoms_to_use.end(), idx) == atoms_to_use.end()) {
                        new_atoms_to_use.push_back(idx);
                        symbol_replacements.emplace_back(idx, convert_atom_to_wildcard(mol->getAtomWithIdx(idx), verbose));
                    }
                }
            }
        }
        // Look for all nearest neighbors of the currently-included atoms
        for(auto neighbor: AtomGetNeighbors(atom)) {
            // Evaluate nearest neighbor atom to determine what should be included
            std::tie(new_atoms_to_use, symbol_replacements) = \
                expand_atoms_to_use_atom(mol, new_atoms_to_use, neighbor->getIdx(), 
                    groups, symbol_replacements, verbose);
        }
    }
    return std::pair(new_atoms_to_use, symbol_replacements);
}

std::vector<int> expand_changed_atom_tags(
    const std::unordered_map<int, RDKit::Atom*>& changed_atom_tags,
    const std::string& reactant_fragments,
    const bool verbose
) {
    std::vector<int> expansion;
    expansion.reserve(32);
    
    static std::regex reg_expr_label(R"(\:([0-9]+)\])");
    
    std::sregex_iterator rit(reactant_fragments.begin(), reactant_fragments.end(), reg_expr_label);
    std::sregex_iterator rend;
    
    while(rit != rend) {
        std::smatch pieces_match = *rit;
        int label = std::stoi(std::string{pieces_match[1].str()});
        if(!changed_atom_tags.contains(label)) {
            expansion.push_back(label);
        }
        ++rit;
    }
    if(verbose) {
        std::cout << "after building reactant fragments, additional labels included: ";
        for(auto i: expansion) std::cout << i << " ";
        std::cout << std::endl;
    }
    return expansion;
}


/*
 * Given a list of RDKit mols and a list of changed atom tags, this function
 * computes the SMILES string of molecular fragments using MolFragmentToSmiles 
 * for all changed fragments.
 * 
 * expansion: atoms added during reactant expansion that should be included and
 *            generalized in product fragment
 */
std::tuple<std::string, bool, bool>
get_fragments_for_changed_atoms(
    const std::vector<RDKit::RWMOL_SPTR>& mols,
    const std::unordered_map<int, RDKit::Atom*>& changed_atom_tags,
    const int radius,
    const std::string& category,
    const std::vector<int>& expansion,
    const bool include_all_unmapped_reactant_atoms,
    const bool use_stereochemistry,
    const bool use_custom_special_groups,
    const std::vector<std::pair<std::vector<int>,RDKit::RWMOL_SPTR>>& special_groups,
    const bool verbose
) {
    std::string fragments;
    fragments.reserve(64);
    std::vector<std::string> mols_changed;
    mols_changed.reserve(mols.size());
    
    std::vector<std::pair<int, std::string>> symbol_replacements;
    for(auto mol: mols) {
        // Initialize list of replacement symbols (updated during expansion)
        symbol_replacements.clear();

        // Are we looking for special reactive groups? (reactants only)
        std::vector<
            std::pair<
                std::vector<int>,
                std::vector<int>
            >
        > groups;
        if(category == "reactants") {
            groups = get_special_groups(mol, use_custom_special_groups, special_groups, verbose);
        }

        // Build list of atoms to use
        std::vector<int> atoms_to_use;
        atoms_to_use.reserve(mol->getNumAtoms());
        for(auto atom: mol->atoms()) {
            // Check self (only tagged atoms)
            int mapno = atom->getAtomMapNum();
            if(mapno != 0 && changed_atom_tags.contains(mapno)) {
                atoms_to_use.push_back(atom->getIdx());
                auto symbol = get_strict_smarts_for_atom(atom, use_stereochemistry);
                if(symbol != AtomGetSmarts(atom)) {
                    symbol_replacements.emplace_back(atom->getIdx(), symbol);
                }
                continue;
            }
        }
        if(verbose) {
            std::cout << "after tagged atom, atoms_to_use.size(): " << atoms_to_use.size() << std::endl;
            for(auto s: atoms_to_use) std::cout << s << " ";
            std::cout << std::endl;
        }
        // Fully define leaving groups and this molecule participates?
        if(include_all_unmapped_reactant_atoms && atoms_to_use.size() > 0) {
            if(category == "reactants") {
                for(auto atom: mol->atoms()) {
                    if(atom->getAtomMapNum() == 0) {
                        atoms_to_use.push_back(atom->getIdx());
                    }
                }
            }
        }
        if(verbose) {
            std::cout << "after reactants atom, atoms_to_use.size(): " << atoms_to_use.size() << std::endl;
            for(auto s: atoms_to_use) std::cout << s << " ";
            std::cout << std::endl;
        }
        // Check neighbors (any atom)
        for(int k = 0; k < radius; k++) {
            std::tie(atoms_to_use, symbol_replacements) = expand_atoms_to_use(
                mol, atoms_to_use, groups, symbol_replacements, verbose);
        }
        if(verbose) {
            std::cout << "after neighbors atom, atoms_to_use.size(): " << atoms_to_use.size() << std::endl;
            for(auto s: atoms_to_use) std::cout << s << " ";
            std::cout << std::endl;
        }
        
        if(category == "products") {
            // Add extra labels to include (for products only)
            if(expansion.size() > 0) {
                for(auto atom: mol->atoms()) {
                    // if ":" not in atom.GetSmarts(): continue;
                    if(atom->getAtomMapNum() == 0) continue;
                    // label = atom.GetSmarts().split(':')[1][:-1]
                    int label = atom->getAtomMapNum();
                    //if label in expansion and label not in changed_atom_tags:
                    if(std::find(expansion.begin(), expansion.end(), label) != expansion.end() &&
                        !changed_atom_tags.contains(label)
                    ) {
                        atoms_to_use.push_back(atom->getIdx());
                        // Make the expansion a wildcard
                        symbol_replacements.emplace_back(atom->getIdx(), convert_atom_to_wildcard(atom, verbose));
                        if(verbose) std::cout << "expanded label " << label << " to wildcard in products" << std::endl;
                    }
                }
            }
            // Make sure unmapped atoms are included (from products)
            for(auto atom: mol->atoms()) {
                if(atom->getAtomMapNum() == 0) {
                    atoms_to_use.push_back(atom->getIdx());
                    auto symbol = get_strict_smarts_for_atom(atom, use_stereochemistry);
                    symbol_replacements.emplace_back(atom->getIdx(), symbol);
                }
            }
        }
        
        // Define new symbols based on symbol_replacements
        std::vector<std::string> symbols;
        symbols.reserve(mol->getNumAtoms());
        for(auto atom: mol->atoms()) {
            symbols.push_back(
                RDKit::SmilesWrite::GetAtomSmiles(atom, false, nullptr, false, true) // cannot use AtomGetSmarts
            );
        }
        for(auto id_sym: symbol_replacements) {
            symbols[id_sym.first] = id_sym.second;
        }

        if(atoms_to_use.size() == 0) continue;
        
        // build chiral atom idx for non-mapped atoms
        std::vector<int> chiral_atom_idx;
        for(auto idx: atoms_to_use) {
            auto atom = mol->getAtomWithIdx(idx);
            if(symbols[idx].find("@") != std::string::npos) {
                if(verbose) std::cout << "chiral_atom_idx: idx=" << idx << ", symbols=" << symbols[idx] << std::endl;
                if(atom->getAtomMapNum() == 0) {
                    chiral_atom_idx.push_back(idx);
                } else {
                    chiral_atom_idx.insert(chiral_atom_idx.begin(), idx);
                }
            }
        }
        
        auto params = RDKit::SubstructMatchParameters();
        params.uniquify = true;
        params.recursionPossible = true;
        params.useChirality = true;
        params.useQueryQueryMatches = false;
        params.maxMatches = 1000;
        params.numThreads = 1;
        // Keep flipping stereocenters until we are happy...
        // this is a sloppy fix during extraction to achieve consistency
        bool tetra_consistent = false;
        int num_tetra_flips = 0;
        int max_tetra_flips = 1024;
        std::string this_fragment;
        while(!tetra_consistent && num_tetra_flips < max_tetra_flips) {
            if(verbose) std::cout << "mol: " << RDKit::MolToSmiles(*mol, true) << std::endl;
            RDKit::RWMOL_SPTR mol_copy(new RDKit::RWMol(*mol));
            clear_mapnum(mol_copy);
            if(verbose) {
                std::cout << "mol_copy: " << RDKit::MolToSmiles(*mol_copy, true) << std::endl;
                for(auto s: symbols) std::cout << s << " ";
                std::cout << std::endl << "symbols.size(): " << symbols.size() << std::endl;
                for(auto s: atoms_to_use) std::cout << s << " ";
                std::cout << std::endl << "atoms_to_use.size(): " << atoms_to_use.size() << std::endl;
            }
            // std::string MolFragmentToSmiles(
            // const ROMol &mol, const std::vector<int> &atomsToUse,
            // const std::vector<int> *bondsToUse = 0,
            // const std::vector<std::string> *atomSymbols = 0,
            // const std::vector<std::string> *bondSymbols = 0,
            // bool doIsomericSmiles = true, bool doKekule = false, int rootedAtAtom = -1,
            // bool canonical = true, bool allBondsExplicit = false,
            // bool allHsExplicit = false);
            this_fragment = RDKit::MolFragmentToSmiles(
                *mol_copy, atoms_to_use,
                0, // bondsToUse
                &symbols, // atomSymbols
                0, // bondSymbols
                use_stereochemistry, // doIsomericSmiles
                false, // doKekule
                -1, // rootedAtAtom
                true, // canonical
                true, // allBondsExplicit
                true // allHsExplicit
            );
            if(verbose)std::cout << "this_fragment: " << this_fragment << std::endl;

            // Figure out what atom maps are tetrahedral centers
            // Set isotopes to make sure we're getting the *exact* match we want
            auto this_fragment_mol = RDKit::RWMOL_SPTR(RDKit::SmartsToMol(this_fragment));
            for(auto atom: this_fragment_mol->atoms()) {
                int mapno = atom->getAtomMapNum();
                if(mapno != 0) {
                    atom->setIsotope(mapno);
                }
            }
            if(num_tetra_flips == 0) {
                max_tetra_flips = std::min(max_tetra_flips, static_cast<int>(std::pow(2., chiral_atom_idx.size())));
            }
            for(auto atom: mol->atoms()) {
                int mapno = atom->getAtomMapNum();
                if(mapno != 0) {
                    atom->setIsotope(mapno);
                }
            }
            // Look for matches
            tetra_consistent = true;
            std::unordered_set<int> all_matched_ids;
            all_matched_ids.reserve(mol->getNumAtoms());
            
            // skip substructure matching if there are a lot of fragments
            // this can help prevent GetSubstructMatches from hanging 
            std::string frag_smi = RDKit::MolToSmiles(*this_fragment_mol);
            if(std::count(frag_smi.begin(), frag_smi.end(), '.') > 5) break;
            
            //for matched_ids in mol.GetSubstructMatches(this_fragment_mol, useChirality=True):
            //    all_matched_ids.extend(matched_ids)
            // std::vector<RDKit::MatchVectType>, MatchVectType = std::vector<std::pair<int, int>>
            auto matchVect = RDKit::SubstructMatch(*mol, *this_fragment_mol, params);
            for(auto matched_ids: matchVect) {
                for(auto idx: matched_ids) {
                    all_matched_ids.insert(idx.second);
                }
            }
            if(verbose) {
                std::cout << "all_matched_ids, size=" << all_matched_ids.size() << ": ";
                for(auto i: all_matched_ids) std::cout << i << " ";
                std::cout << std::endl;
            }
            
            for(auto idx: chiral_atom_idx) {
                if(verbose) std::cout << "Checking consistency of tetrahedral " << idx << std::endl;
                if(!all_matched_ids.contains(idx)) {
                    tetra_consistent = false;
                    if(verbose) std::cout << "@@@@@@@@@@@ FRAGMENT DOES NOT MATCH PARENT MOL @@@@@@@@@@@@@@" << std::endl;
                    break;
                }
            }
            num_tetra_flips += 1;
            
            if(!tetra_consistent) {
                if(verbose) {
                    std::cout << "@@@@@@@@@@@ FLIPPING CHIRALITY SYMBOL NOW      @@@@@@@@@@@@@@" << std::endl;
                    std::cout << "num_tetra_flips=" << num_tetra_flips << std::endl;
                }
                int bits = num_tetra_flips ^ (num_tetra_flips-1);
                for(unsigned i = 0; i < chiral_atom_idx.size(); i++) {
                    int bit = bits % 2;
                    bits >>= 1;
                    int idx = chiral_atom_idx[i];
                    std::string& prevsymbol = symbols[idx];
                    if(bit) {
                        if(verbose) std::cout << "prevsymbol: " << prevsymbol;
                        auto pos = prevsymbol.find("@@");
                        if(pos != std::string::npos) {
                            //symbol = prevsymbol.replace("@@", "@");
                            prevsymbol.erase(pos, 1);
                        } else {
                            pos = prevsymbol.find("@");
                            if(pos != std::string::npos)
                                //symbol = prevsymbol.replace("@", "@@");
                                prevsymbol.insert(pos, "@");
                            else {
                                throw std::runtime_error{"Need to modify symbol of tetra atom without @ or @@??"};
                            }
                        }
                        if(verbose) std::cout << ", flipped symbol: " << prevsymbol << std::endl;
                    } else {
                        if(verbose) std::cout << "do not flip symbol, prevsymbol: " << prevsymbol << std::endl;
                    }
                }
            }
            // Clear isotopes
            for(auto atom: mol->atoms()) {
                atom->setIsotope(0);
            }
        }
        if(!tetra_consistent) {
            throw std::runtime_error{
                std::string{"Could not find consistent tetrahedral mapping, "}
                + std::to_string(chiral_atom_idx.size()) + " centers"
            };
        }
        
        fragments += std::string{"("} + this_fragment + ").";
        
        auto _new_mol = RDKit::RWMOL_SPTR(RDKit::SmilesToMol(RDKit::MolToSmiles(*mol, true)));
        clear_mapnum(_new_mol);
        mols_changed.push_back(RDKit::MolToSmiles(*_new_mol, true));
    }
    
    // auxiliary template information: is this an intramolecular reaction or dimerization?
    bool intra_only = 1 == mols_changed.size();
    bool dimer_only = mols_changed.size() == 2 && mols_changed[0] == mols_changed[1];
    
    fragments.pop_back();
    if(verbose) std::cout << "fragments: " << fragments << std::endl;
    return std::tuple(fragments, intra_only, dimer_only);
}

// NOTE: may throw
struct template_result
extract_from_reaction(
    const std::string& reactants_smiles,
    const std::string& products_smiles,
    const bool verbose,
    const bool use_stereochemistry,
    const unsigned maximum_number_unmapped_product_atoms,
    const bool include_all_unmapped_reactant_atoms,
    const int radius,
    const bool use_custom_special_groups,
    const std::vector<std::pair<std::vector<int>,RDKit::RWMOL_SPTR>> special_groups,
    const bool do_canonicalize
) {
    struct template_result result;
    
    std::string reactants_smiles_nonD = replace_deuterated(reactants_smiles);
    std::string products_smiles_nonD = replace_deuterated(products_smiles);
    
    std::vector<std::string> reactants_smiles_split, products_smiles_split;
    reactants_smiles_split.reserve(16);
    products_smiles_split.reserve(16);
    boost::algorithm::split(reactants_smiles_split, reactants_smiles_nonD, boost::is_any_of("."));
    boost::algorithm::split(products_smiles_split,  products_smiles_nonD,  boost::is_any_of("."));
    
    std::vector<RDKit::RWMOL_SPTR> reactants, products;
    try {
        reactants = mols_from_smiles_list(reactants_smiles_split);
        products = mols_from_smiles_list(products_smiles_split);
    } catch(std::exception& e) {
        throw std::runtime_error{
            std::string{"extract_from_reaction(): RDKit::SmilesToMol() in mols_from_smiles_list() failed. msg: "}
            + e.what()
        };
    }
    
    if(reactants.empty() || products.empty()) {
        throw std::runtime_error{"No reactants or products parsed."};
    }
    
    // try to sanitize molecules
    try {
        for(auto i: reactants) {
            RDKit::MolOps::removeHs(*i); // *might* not be safe
            i->updatePropertyCache();
        }
        for(auto i: products) {
            RDKit::MolOps::removeHs(*i); // *might* not be safe
            i->updatePropertyCache();
        }
        // [Chem.SanitizeMol(mol) for mol in reactants + products] // redundant w/ RemoveHs
    } catch(std::exception& e) {
        throw std::runtime_error{
            std::string{"extract_from_reaction(): Could not load SMILES or sanitize. msg: "}
            + e.what()
        };
    }
    
    bool are_unmapped_product_atoms = false;
    std::string extra_reactant_fragment;
    for(auto product: products) {
        for(auto atom: product->atoms()) {
            if(atom->getAtomMapNum() == 0) {
                are_unmapped_product_atoms = true;
                break;
            }
        }
        if(are_unmapped_product_atoms) break;
    }
    if(are_unmapped_product_atoms && verbose) {
        std::cout << "Not all product atoms have atom mapping" << std::endl;
    }
    
    if(are_unmapped_product_atoms) { // add fragment to template
        for(auto product: products) {
            std::vector<int> unmapped_ids;
            unmapped_ids.reserve(32);
            // Get unmapped atoms
            for(auto atom: product->atoms()) {
                if(atom->getAtomMapNum() == 0) {
                    unmapped_ids.push_back(atom->getIdx());
                }
            }
            if(unmapped_ids.size() > maximum_number_unmapped_product_atoms) {
                // Skip this example - too many unmapped product atoms!
                throw std::runtime_error{"Too many unmapped product atoms!"};
            }
            // Define new atom symbols for fragment with atom maps, generalizing fully
            std::vector<std::string> atom_symbols;
            atom_symbols.reserve(product->getNumAtoms());
            for(auto atom: product->atoms()) {
                atom_symbols.push_back(std::string{"["} + atom->getSymbol() + "]");
            }
            // And bond symbols...
            std::vector<std::string> bond_symbols(product->getNumBonds(), std::string{"~"});
            if(unmapped_ids.size() > 0) {
                // std::string MolFragmentToSmiles(
                // const ROMol &mol, const std::vector<int> &atomsToUse,
                // const std::vector<int> *bondsToUse = 0,
                // const std::vector<std::string> *atomSymbols = 0,
                // const std::vector<std::string> *bondSymbols = 0,
                // bool doIsomericSmiles = true, bool doKekule = false, int rootedAtAtom = -1,
                // bool canonical = true, bool allBondsExplicit = false,
                // bool allHsExplicit = false);
                extra_reactant_fragment +=
                    RDKit::MolFragmentToSmiles(
                        *product, unmapped_ids,
                        0, // bondsToUse
                        &atom_symbols, // atomSymbols
                        &bond_symbols, // bondSymbols
                        use_stereochemistry, // doIsomericSmiles
                        false, // doKekule
                        -1, // rootedAtAtom
                        true, // canonical
                        false, // allBondsExplicit
                        false // allHsExplicit
                    ) + '.';
            }
        }
        if(extra_reactant_fragment.size() > 0) {
            extra_reactant_fragment.pop_back();
            if(verbose) std::cout << "    extra reactant fragment: " << extra_reactant_fragment << std::endl;
        }
        // Consolidate repeated fragments (stoichometry)
        std::vector<std::string> smiles_split;
        smiles_split.reserve(16);
        boost::algorithm::split(smiles_split, extra_reactant_fragment, boost::is_any_of("."));
        std::sort(smiles_split.begin(), smiles_split.end());
        auto it = std::unique(smiles_split.begin(), smiles_split.end());
        smiles_split.erase(it, smiles_split.end());
        extra_reactant_fragment = boost::algorithm::join(smiles_split, ".");
    }
    
    // Calculate changed atoms
    // int -> Atom* (reactants)
    auto changed_atom_tags = get_changed_atoms(reactants, products, verbose);
    if(changed_atom_tags.empty()) {
        if(verbose) {
            std::cout << "No atoms changed?" << std::endl;
            std::cout << "\treactants_smiles: " << reactants_smiles << std::endl;
            std::cout << "\tproducts_smiles: " << products_smiles << std::endl;
        }
        throw std::runtime_error{"No changed atoms"};
    }
    
    // Get fragments for reactants
    //   std::tuple<std::string, bool, bool>
    //   get_fragments_for_changed_atoms(
    //       std::vector<RDKit::RWMOL_SPTR> mols,
    //       std::unordered_map<int, RDKit::Atom*> changed_atom_tags,
    //       int radius,
    //       std::string category,
    //       std::vector<int> expansion,
    //       bool include_all_unmapped_reactant_atoms,
    //       bool use_stereochemistry,
    //       bool use_custom_special_groups,
    //       std::vector<std::pair<std::vector<int>,RDKit::RWMOL_SPTR>> special_groups
    //       bool verbose
    //   )
    auto [reactant_fragments, intra_only, dimer_only] = 
    get_fragments_for_changed_atoms(
        reactants,
        changed_atom_tags, 
        radius, // radius
        "reactants", // category
        {}, // expansion
        include_all_unmapped_reactant_atoms,
        use_stereochemistry,
        use_custom_special_groups,
        special_groups,
        verbose
    );
    // Get fragments for products 
    // (WITHOUT matching groups but WITH the addition of reactant fragments)
    auto [product_fragments, _intra_only, _dimer_only] =
    get_fragments_for_changed_atoms(
        products,
        changed_atom_tags, 
        0, // radius
        "products", // category
        expand_changed_atom_tags(changed_atom_tags, reactant_fragments, verbose), // expansion
        include_all_unmapped_reactant_atoms,
        use_stereochemistry,
        use_custom_special_groups,
        special_groups,
        verbose
    );
    
    // Put together and canonicalize (as best as possible)
    std::string rxn_string = reactant_fragments + ">>" + product_fragments;
    if(verbose) std::cout << "before canonicalization: " << rxn_string << std::endl;
    // Change from inter-molecular to intra-molecular
    std::string rxn_canonical = canonicalize_transform_remove_brackets(rxn_string);
    //rxn_canonical = canonicalize_transform_old(rxn_canonical);
    if(do_canonicalize) rxn_canonical = canonicalize_transform(rxn_canonical, true);
    std::vector<std::string> rxn_canonical_split;
    rxn_canonical_split.reserve(4);
    boost::algorithm::split(rxn_canonical_split, rxn_canonical, boost::is_any_of(">"));

    std::string reactants_string = rxn_canonical_split[0];
    std::string products_string  = rxn_canonical_split[2];

    std::string retro_canonical = products_string + ">>" + reactants_string;

    // Load into RDKit
    {
        std::shared_ptr<RDKit::ChemicalReaction> rxn{RDKit::RxnSmartsToChemicalReaction(retro_canonical)};
        if (!rxn->isInitialized()) {
            rxn->initReactantMatchers();
        }
        
        unsigned int numWarnings, numErrors;
        rxn->validate(numWarnings, numErrors);
        if(numErrors != 0) {
            throw std::runtime_error{
                std::string{"reaction validation failed, numWarnings="}
                + std::to_string(numWarnings) + ", numErrors=" + std::to_string(numErrors)
                + ", retro_canonical=" + retro_canonical
            };
        }
    }

    // set up return value
    result.products_smarts = products_string;
    result.reactants_smarts = reactants_string,
    result.reaction_smarts_forward = rxn_canonical;
    result.reaction_smarts_retro = retro_canonical;
    result.intra_only = intra_only;
    result.dimer_only = dimer_only;
    result.necessary_reagent = extra_reactant_fragment;
    
    return result;
}

}
