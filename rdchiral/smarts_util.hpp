#ifndef RDCHIRAL_SMARTS_UTIL_HEADER
#define RDCHIRAL_SMARTS_UTIL_HEADER

#include <string>

#include <GraphMol/GraphMol.h>
#include <GraphMol/SmilesParse/SmilesWrite.h>
#include <GraphMol/SmilesParse/SmilesParse.h>
#include <GraphMol/SmilesParse/SmartsWrite.h>
#include <GraphMol/MolOps.h>

namespace rdchiral {

void clear_mapnum(const RDKit::RWMOL_SPTR& mol);

void set_isotope_to_equal_mapnum(const RDKit::RWMOL_SPTR& mol);

void clear_isotope(const RDKit::RWMOL_SPTR& mol);

std::string convert_atom_to_wildcard(const RDKit::Atom * const atom, const bool verbose = false);

std::string get_strict_smarts_for_atom(const RDKit::Atom * const atom, const bool use_stereochemistry, const bool use_mapno = true);


// old SMARTS canonicalization

std::string reassign_atom_mapping(const std::string& transform);

[[deprecated]]
std::string canonicalize_smarts_old(const std::string& smarts);

[[deprecated]]
std::string canonicalize_transform_old(const std::string& transform);

std::string canonicalize_transform_remove_brackets(const std::string& transform);

std::string canonicalize_smarts_atom(
    const std::string& transform,
    const bool output_degree = true,
    const bool invert_all_chiral_centers = false,
    const bool output_hydrogen = true
);


// new SMARTS canonicalization

void reassign_atom_mapping_mol(
    const RDKit::RWMOL_SPTR& reactants_pattern,
    const RDKit::RWMOL_SPTR& products_pattern
);

void reassign_atom_mapping_mol(
    const RDKit::ROMOL_SPTR& reactants_pattern,
    const RDKit::ROMOL_SPTR& products_pattern
);

// canonicalize a single smarts
std::string canonicalize_smarts(const std::string& smarts);

// canonicalize a pair of smarts (aka template)
std::string canonicalize_transform(const std::string& transform, const bool chirality_inversion_equivalent = true);
std::string canonicalize_transform_consistency_check(const std::string& transform, const bool chirality_inversion_equivalent = true);

}

#endif // RDCHIRAL_SMARTS_UTIL_HEADER
