#ifndef RDCHIRAL_BONDS_HEADER
#define RDCHIRAL_BONDS_HEADER

#include <exception>
#include <string>
#include <vector>
#include <tuple>
#include <utility>
#include <unordered_set>
#include <unordered_map>

#include <GraphMol/GraphMol.h>

#include "rdchiral/util.hpp"

namespace rdchiral {

RDKit::Bond::BondDir BondDirOpposite(const RDKit::Bond::BondDir dir);

std::string BondDirLabel(const RDKit::Bond::BondDir dir);

struct DoubleBond {
    int mapnums[4]={0}; // front_mapnums, back_mapnums
    std::pair<RDKit::Bond::BondDir, RDKit::Bond::BondDir> dirs{RDKit::Bond::NONE, RDKit::Bond::NONE}; // front_dir, back_dir
    bool is_implicit{false};
};

std::vector<DoubleBond> get_atoms_across_double_bonds(const RDKit::RWMol& mol);

std::unordered_map<std::pair<int, int>, RDKit::Bond::BondDir, _hash_pair>
bond_dirs_by_mapnum(const RDKit::ROMol& mol);

std::pair<
    std::unordered_map<
        std::tuple<int, int, int, int>,
        std::pair<RDKit::Bond::BondDir, RDKit::Bond::BondDir>,
        _hash_tuple<int, int, int, int>
    >,
    std::unordered_set<std::pair<int, int>, _hash_pair>
>
enumerate_possible_cistrans_defs(const RDKit::ROMol& mol);

bool
restore_bond_stereo_to_sp2_atom(
    const RDKit::Atom* const atom,
    const std::unordered_map<std::pair<int, int>, RDKit::Bond::BondDir, _hash_pair>& bond_dirs_by_mapnum,
    const bool verbose = false
);

std::string bond_to_label(const RDKit::Bond *bond);

bool atoms_are_different(const RDKit::Atom * const atom1, const RDKit::Atom * const atom2, const bool use_smarts=true);

}

#endif // RDCHIRAL_BONDS_HEADER
