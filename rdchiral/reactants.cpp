#include <GraphMol/SmilesParse/SmilesWrite.h>
#include <GraphMol/SmilesParse/SmilesParse.h>
#include <GraphMol/MolOps.h>

#include "rdchiral/rdchiral.hpp"
#include "rdchiral/rdkit_helper.hpp"


namespace rdchiral {

RDKit::RWMOL_SPTR initialize_reactants_from_smiles(const std::string& reactant_smiles) {
    auto reactants = RDKit::RWMOL_SPTR{RDKit::SmilesToMol(reactant_smiles)};
    RDKit::MolOps::assignStereochemistry(*reactants, false, true, true);
    reactants->updatePropertyCache(false);
    
    for(int i{0}; auto atom: reactants->atoms()) {
        i++;
        atom->setAtomMapNum(i);
    }
    return reactants;
}

Reactants::Reactants(const std::string& reactant_smiles) :
    m_reactant_smiles{reactant_smiles},
    m_reactants{initialize_reactants_from_smiles(reactant_smiles)},
    m_reactants_achiral{RDKit::RWMOL_SPTR{new RDKit::RWMol(*m_reactants, false, -1)}}
{
    m_atoms_r.reserve(m_reactants->getNumAtoms());
    for(const auto atom: m_reactants->atoms()) {
        m_atoms_r.emplace(atom->getAtomMapNum(), atom);
    }
    
    m_bonds_by_mapnum.reserve(m_reactants->getNumBonds());
    for(const auto bond: m_reactants->bonds()) {
        m_bonds_by_mapnum.emplace_back(bond->getBeginAtom()->getAtomMapNum(), bond->getEndAtom()->getAtomMapNum(), bond);
    }
    
    m_bond_dirs_by_mapnum.reserve(m_reactants->getNumBonds()*10);
    for(const auto [i, j, bond]: m_bonds_by_mapnum) {
        if(bond->getBondDir() != RDKit::Bond::NONE) {
            m_bond_dirs_by_mapnum.emplace(std::pair{i, j}, bond->getBondDir());
            m_bond_dirs_by_mapnum.emplace(std::pair{j, i}, BondDirOpposite(bond->getBondDir()));
        }
    }
    
    m_atoms_across_double_bonds = get_atoms_across_double_bonds(*m_reactants);
    
    for(auto atom: m_reactants_achiral->atoms()) {
        atom->setChiralTag(RDKit::Atom::CHI_UNSPECIFIED);
    }
    for(auto bond: m_reactants_achiral->bonds()) {
        bond->setStereo(RDKit::Bond::STEREONONE);
        bond->setBondDir(RDKit::Bond::NONE);
    }
}

std::string Reactants::smiles(void) const {
    return RDKit::MolToSmiles(*m_reactants, true);
}

}
