#ifndef RDCHIRAL_UTIL_HEADER
#define RDCHIRAL_UTIL_HEADER

#include <utility>
#include <tuple>
#include <boost/container_hash/hash.hpp>

#include "parity.hpp"

namespace rdchiral {

struct _hash_pair {
    template <class T1, class T2>
    size_t operator()(const std::pair<T1, T2>& p) const
    {
        auto hash1 = std::hash<T1>{}(p.first);
        auto hash2 = std::hash<T2>{}(p.second);
        return hash1 ^ hash2;
    }
};

template<typename... TT>
struct _hash_tuple
{
    size_t operator()(std::tuple<TT...> const& arg) const noexcept
    {
        return boost::hash_value(arg);
    }
};

}

#endif // RDCHIRAL_UTIL_HEADER
