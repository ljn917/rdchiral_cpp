#ifndef RDCHIRAL_CHIRAL_HEADER
#define RDCHIRAL_CHIRAL_HEADER

#include <GraphMol/GraphMol.h>

namespace rdchiral {

// NOTE: input must have full atom mapping

bool template_atom_could_have_been_tetra(RDKit::Atom *atom, const bool strip_if_spec=false, const bool cache=true);

void copy_chirality(RDKit::Atom *a_src, RDKit::Atom *a_new, const bool verbose=false);

int atom_chirality_matches(RDKit::Atom *a_tmp, RDKit::Atom* a_mol, const bool verbose=false);

}

#endif // RDCHIRAL_CHIRAL_HEADER
