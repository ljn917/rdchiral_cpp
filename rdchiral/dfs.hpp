#ifndef RDCHIRAL_DFS_HEADER
#define RDCHIRAL_DFS_HEADER

#include <stack>
#include <vector>
#include <algorithm>
#include <cassert>

namespace rdchiral {

int find_one_undiscovered_node(const std::vector<bool>& discovered) {
    const auto iter = std::find(discovered.begin(), discovered.end(), false);
    if(iter == discovered.end()) {
        return -1;
    } else {
        return std::distance(discovered.begin(), iter);
    }
}

// Inputs:
// adjacency_matrix is of size N, where N is the number of nodes
// each adjacency_matrix[i] stores the indices of adjacent nodes
// node indices must be 0 - (N-1); any i < 0 is empty node index
// Return: connected node indices
//
// from https://en.wikipedia.org/wiki/Depth-first_search
// procedure DFS_iterative(G, v) is
//     let S be a stack
//     S.push(v)
//     while S is not empty do
//         v = S.pop()
//         if v is not labeled as discovered then
//             label v as discovered
//             for all edges from v to w in G.adjacentEdges(v) do 
//                 S.push(w)
template <typename Container>
std::vector<std::vector<typename Container::value_type>>
dfs(const std::vector<Container>& adjacency_matrix) {
    using NodeType = typename Container::value_type;
    
    const auto num_nodes = adjacency_matrix.size();
    
    std::stack<NodeType, std::vector<NodeType>> s;
    std::vector<std::vector<NodeType>> res;
    std::vector<bool> discovered(num_nodes, false);
    
    while(true) {
        if(const auto node_id = find_one_undiscovered_node(discovered); node_id >= 0) {
            s.push(node_id);
        } else {
            break;
        }
        
        std::vector<NodeType> connected_nodes;
        
        while(!s.empty()) {
            const auto node_id = s.top();
            s.pop();
            assert(node_id >= 0);
            assert(node_id < num_nodes);
            
            if(!discovered[node_id]) {
                discovered[node_id] = true;
                
                connected_nodes.push_back(node_id);
                
                for(const auto adj_node_id: adjacency_matrix[node_id]) {
                    if(adj_node_id >= 0) {
                        s.push(adj_node_id);
                    }
                }
            }
        }
        
        res.emplace_back(std::move(connected_nodes));
    }
    
    return res;
}

}

#endif // RDCHIRAL_DFS_HEADER
