#include <set>
#include <unordered_map>
#include <regex>
#include <vector>
#include <tuple>
#include <string>
#include <algorithm>
#include <functional>
#include <cassert>

#include <boost/range.hpp>
#include <boost/functional/hash.hpp>
#include <boost/algorithm/string.hpp>

#include <GraphMol/QueryAtom.h>

#include "rdchiral/smarts_util.hpp"
#include "rdchiral/rdkit_helper.hpp"
#include "rdchiral/parity.hpp"
#include "rdchiral/dfs.hpp"

namespace rdchiral {

void clear_mapnum(const RDKit::RWMOL_SPTR& mol) {
    for(auto atom: mol->atoms()) {
        atom->setAtomMapNum(0);
    }
}

void set_isotope_to_equal_mapnum(const RDKit::RWMOL_SPTR& mol) {
    for(auto a: mol->atoms()) {
        int mapno = a->getAtomMapNum();
        if(mapno != 0) {
            a->setIsotope(mapno);
        }
    }
}

void clear_isotope(const RDKit::RWMOL_SPTR& mol) {
    for(auto a: mol->atoms()) {
        a->setIsotope(0);
    }
}

int max_mapno(const RDKit::RWMOL_SPTR& mol) {
    int res{0};
    
    for(const auto atom: mol->atoms()) {
        res = std::max(res, atom->getAtomMapNum());
    }
    
    return res;
}

int assign_mapno(RDKit::RWMOL_SPTR mol, int start_mapno) {
    for(const auto atom: mol->atoms()) {
        if(atom->getAtomMapNum() == 0) {
            atom->setAtomMapNum(start_mapno++);
        }
    }
    
    return start_mapno;
}


// This function takes an RDKit atom and turns it into a wildcard 
// using heuristic generalization rules. This function should be used
// when candidate atoms are used to extend the reaction core for higher
// generalizability
std::string convert_atom_to_wildcard(const RDKit::Atom * const atom, const bool verbose) {
    //static std::regex reg_expr_charges(R"(([-+]+[1-9]?))");
    //static std::regex reg_expr_label(R"(\:[0-9]+\])");
    
    //auto atom_smarts = AtomGetSmarts(atom);
    std::string charge_symbol;
    int charges = atom->getFormalCharge();
    if(charges > 0){
        charge_symbol = "+";
    } else {
        charge_symbol = "-";
    }
    charge_symbol += std::to_string(std::abs(charges));
    
    std::string symbol;
    
    // Is this a terminal atom? We can tell if the degree is one
    if(atom->getDegree() == 1) {
        symbol = "[" + atom->getSymbol();
        if(charges != 0) {
            symbol += std::string{";"} + charge_symbol;
        }
        symbol += std::string{";D1;H"} + std::to_string(atom->getTotalNumHs());
    } else {
        // Initialize
        symbol = "[";

        // Add atom primitive - atomic num and aromaticity (don't use COMPLETE wildcards)
        if(atom->getAtomicNum() != 6) {
            symbol += std::string{"#"}+std::to_string(atom->getAtomicNum())+";";
            if(atom->getIsAromatic()) symbol += "a;";
        } else if(atom->getIsAromatic()) {
            symbol += "c;";
        } else {
            symbol += "C;";
        }
        
        // Charge is important
        if(charges != 0) symbol += charge_symbol + ";";
        // Strip extra semicolon
        if(symbol.back() == ';') symbol.pop_back();
    }
    
    // Close with label or with bracket
    int label = atom->getAtomMapNum();
    if(label != 0) symbol += std::string{":"} + std::to_string(label);
    symbol += ']';

    if(verbose) {
        auto atom_smarts = AtomGetSmarts(atom);
        if(symbol != atom_smarts)
            std::cout << "Improved generality of atom SMARTS " << atom_smarts << " -> " << symbol << std::endl;
    }
    
    return symbol;
}

// For an RDkit atom object, generate a SMARTS pattern that
// matches the atom as strictly as possible
std::string get_strict_smarts_for_atom(
    const RDKit::Atom * const atom,
    const bool use_stereochemistry,
    const bool use_mapno
) {
    std::string smarts;
    smarts.reserve(64);
    
    smarts = "[";
    
    std::string atom_symbol = atom->getSymbol();
    if(atom_symbol == "H") atom_symbol = "#1";
    if(atom->getIsAromatic() && atom_symbol[0] >= 'A' && atom_symbol[0] <= 'Z') {
        atom_symbol[0] -= ('A' - 'a');
    }
    smarts += atom_symbol;

    // Explicit stereochemistry - *before* H
    std::string atsign;
    if(use_stereochemistry) {
        if(atom->getChiralTag() != RDKit::Atom::CHI_UNSPECIFIED) {
            // Be explicit when there is a tetrahedral chiral tag
            if(atom->getChiralTag() == RDKit::Atom::CHI_TETRAHEDRAL_CCW)
                atsign = "@";
            else if(atom->getChiralTag() == RDKit::Atom::CHI_TETRAHEDRAL_CW)
                atsign = "@@";
        }
    }
    smarts += atsign;
    
    // Explicit number of hydrogens: include "H0" when no hydrogens present
    smarts += std::string{";H"} + std::to_string(atom->getTotalNumHs());
      
    // Explicit degree
    smarts += std::string{";D"} + std::to_string(atom->getDegree());

    // Explicit formal charge
    int charges = atom->getFormalCharge();
    std::string charges_symbol;
    if(charges >= 0) {
        charges_symbol = ";+";
    } else {
        charges_symbol = ";-";
    }
    charges_symbol += std::to_string(std::abs(charges));
    smarts += charges_symbol;
    
    int mapno = atom->getAtomMapNum();
    if(mapno != 0 && use_mapno) {
        smarts += std::string{":"} + std::to_string(mapno);
    }
    
    smarts += "]";
    
    return smarts;
}

// This function takes an atom-mapped reaction SMILES and reassigns 
// the atom-mapping labels (numbers) from left to right, once 
// that transform has been canonicalized.
std::string reassign_atom_mapping(const std::string& transform) {
    static std::regex reg_expr_label(R"(\:([0-9]+)\])");
    
    std::sregex_iterator rit(transform.begin(), transform.end(), reg_expr_label);
    std::sregex_iterator rend;

    // Define list of replacements which matches all_labels *IN ORDER*
    std::unordered_map<std::string, std::string> replacement_dict; // use ":\d+]" as key/value
    replacement_dict.reserve(32);
    int counter = 1;
    std::string suffix, transform_newmaps;
    std::string new_label, label;
    while(rit != rend) {
        label = rit->str();
        if(replacement_dict.contains(label)) {
            new_label = replacement_dict[label];
        } else {
            new_label = std::string{":"} + std::to_string(counter) + "]";
            replacement_dict[label] = new_label;
            counter++;
        }
        
        transform_newmaps += std::string{rit->prefix()} + new_label;
        
        suffix = rit->suffix();
        rit++;
    }
    
    return transform_newmaps + suffix;
}

// This function takes one-half of a template SMARTS string 
// (i.e., reactants or products) and re-orders them based on
// an equivalent string without atom mapping.
std::string canonicalize_smarts_old(const std::string& smarts_template) {
    static std::regex reg_expr_label(R"(\:[0-9]+\])");

    // remove wrapper parentheses
    //smarts_template.pop_back();
    //smarts_template.erase(smarts_template.begin());
    // Split into separate molecules *WITHOUT wrapper parentheses*
    std::vector<std::string> template_mols;
    template_mols.reserve(8);
    //boost::algorithm::iter_split(template_mols, smarts_template, boost::first_finder(").("));
    boost::algorithm::split(template_mols, smarts_template, boost::is_any_of("."));
    
    // pair of nolabel, labeled
    std::vector<std::pair<std::string, std::string>> template_nolabels_mols(template_mols.size());

    // Split into fragments within those molecules
    for(size_t i = 0; i < template_mols.size(); i++) {
        std::vector<std::string> mol_frags;
        mol_frags.reserve(8);
        boost::algorithm::split(mol_frags, template_mols[i], boost::is_any_of("."));
        
        std::vector<std::pair<std::string, std::string>> nolabel_mol_frags; // pair of nolabel, labeled
        nolabel_mol_frags.reserve(mol_frags.size());
        for(auto const& s: mol_frags) {
            nolabel_mol_frags.emplace_back(std::regex_replace(s, reg_expr_label, "]"), s);
        }

        // Get sort order within molecule, defined WITHOUT labels
        //sortorder = [j[0] for j in sorted(enumerate(nolabel_mol_frags), key = lambda x:x[1])]
        std::sort(
            nolabel_mol_frags.begin(),
            nolabel_mol_frags.end(),
            [](
                const std::pair<std::string, std::string>& x,
                const std::pair<std::string, std::string>& y
            ) {
                return x.first < y.first;
            }
        );

        // Apply sorting and merge list back into overall mol fragment
        std::vector<std::string> _tmp1, _tmp2;
        _tmp1.reserve(nolabel_mol_frags.size());
        _tmp2.reserve(nolabel_mol_frags.size());
        for(auto const& p: nolabel_mol_frags) {
            _tmp1.push_back(p.first);
            _tmp2.push_back(p.second);
        }
        template_nolabels_mols[i].first  = boost::algorithm::join(_tmp1, ".");
        template_nolabels_mols[i].second = boost::algorithm::join(_tmp2, ".");
    }
    
    // Get sort order between molecules, defined WITHOUT labels
    //sortorder = [j[0] for j in sorted(enumerate(template_nolabels_mols), key = lambda x:x[1])]
    std::sort(
        template_nolabels_mols.begin(),
        template_nolabels_mols.end(),
        [](
            const std::pair<std::string, std::string>& x,
            const std::pair<std::string, std::string>& y
        ) {
            return x.first < y.first;
        }
    );

    // change order
    template_mols.clear();
    for(auto const& p: template_nolabels_mols) {
        template_mols.push_back(p.second);
    }
    
    // Apply sorting and merge list back into overall transform
    //smarts_template = std::string{"("} + boost::algorithm::join(template_mols, ").(") + ")";
    return boost::algorithm::join(template_mols, ".");
}

// This function takes an atom-mapped SMARTS transform and
// converts it to a canonical form by, if nececssary, rearranging
// the order of reactant and product templates and reassigning
// atom maps.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
std::string canonicalize_transform_old(const std::string& transform) {
    std::vector<std::string> transform_split;
    transform_split.reserve(4);
    boost::algorithm::split(transform_split, transform, boost::is_any_of(">"));
    
    for(auto& x: transform_split) {
        if(!x.empty()) {
            x = canonicalize_smarts_old(x);
        }
    }
    
    std::string transform_reordered = boost::algorithm::join(transform_split, ">");
    return reassign_atom_mapping(transform_reordered);
}
#pragma GCC diagnostic pop

// assume there are (*.*).(*) wrapper
std::string canonicalize_transform_remove_brackets(const std::string& transform) {
    std::vector<std::string> transform_split;
    transform_split.reserve(4);
    boost::algorithm::split(transform_split, transform, boost::is_any_of(">"));
    
    for(auto& x: transform_split) {
        if(!x.empty()) {
            std::vector<std::string> mol_frags;
            mol_frags.reserve(4);
            boost::algorithm::iter_split(mol_frags, x, boost::first_finder(").("));
            mol_frags.front().erase(mol_frags.front().begin());
            mol_frags.back().pop_back();
            x = boost::algorithm::join(mol_frags, ".");
        }
    }
    
    return boost::algorithm::join(transform_split, ">");
}

// This is not the right way to do this. Do not want to export this function
// Only a convient function, should not use it in real applications.
// Only support ";&"
// H must be implicit or #1
std::string canonicalize_smarts_atom(
    const std::string& transform,
    const bool output_degree,
    const bool invert_all_chiral_centers,
    const bool output_hydrogen
) {
    static std::regex reg_expr_atom_label(R"(\[([^\]]+)\])");
    static std::regex reg_expr_mapno_label(R"(^([^\:]+)(\:[0-9]+)$)");
    static std::regex reg_expr_atom_type_label(R"(([0-9]*)([A-Za-z]+|\#[0-9]+)(@*)(H[0-9]*)?([\+\-][0-9]*|\++|\-+)?)"); // isotope, symbol, at, H, charge
    
    std::sregex_iterator rit(transform.begin(), transform.end(), reg_expr_atom_label);
    std::sregex_iterator rend;
    
    std::string res, suffix;
    while (rit != rend) {
        std::smatch pieces_match = *rit;
        std::string atom_label = pieces_match[1].str();
        std::string mapno;
        
        // try to get mapno
        std::smatch mapno_match;
        bool is_match = std::regex_match(atom_label, mapno_match, reg_expr_mapno_label);
        if(is_match) {
            mapno = mapno_match[2];
            atom_label = mapno_match[1]; // must be after mapno
        }
        
        std::vector<std::string> atom_label_split;
        boost::algorithm::split(atom_label_split, atom_label, boost::is_any_of(";&"));
        std::string isotope, symbol, charges, degree, hydrogens, atsign;
        bool is_aromatic{false};
        std::smatch atom_type_match;
        is_match = std::regex_match(atom_label_split[0], atom_type_match, reg_expr_atom_type_label);
        if(is_match) {
            isotope   = atom_type_match[1];
            symbol    = atom_type_match[2];
            atsign    = atom_type_match[3];
            hydrogens = atom_type_match[4];
            charges   = atom_type_match[5];
        } else {
            throw std::logic_error{
                    std::string{"canonicalize_smarts_atom(): unrecognized atom type group, transform="}
                    + transform + ", atom_label_split[0]=" + atom_label_split[0]
                };
        }
        
        for(size_t i = 1; i < atom_label_split.size(); i++) {
            std::string& condition = atom_label_split[i];
            if(condition.front() == 'D') {
                if(!degree.empty()) throw std::runtime_error{std::string{"canonicalize_smarts_atom(): degree set twice, "} + rit->str()};
                degree = condition;
            } else if(condition.front() == 'H') {
                if(!hydrogens.empty()) throw std::runtime_error{"canonicalize_smarts_atom(): hydrogens set twice"};
                hydrogens = condition;
            } else if(condition.front() == '@') {
                if(!atsign.empty()) throw std::runtime_error{"canonicalize_smarts_atom(): atsign set twice"};
                atsign = condition;
            } else if(condition.front() == '+' || condition.front() == '-') {
                if(!charges.empty()) throw std::runtime_error{"canonicalize_smarts_atom(): charges set twice"};
                charges = condition;
            } else if(condition == "a") {
                is_aromatic = true;
            } else {
                throw std::logic_error{
                    std::string{"canonicalize_smarts_atom(): unimplemented condition, transform="}
                    + transform + ", condition=" + condition
                };
            }
        }
        
        if(is_aromatic && symbol.front() == '#') {
            unsigned atom_num = std::stoi(symbol.substr(1));
            symbol = RDKit::PeriodicTable::getTable()->getElementSymbol(atom_num);
        }
        
        if(is_aromatic && symbol[0] >= 'A' && symbol[0] <= 'Z') {
            symbol[0] -= ('A' - 'a');
        }
        
        if(charges.empty()) {
            //charges = "+0";
        } else {
            if(charges.size() == 1) {
                charges += "1";
            } else {
                if(charges[0] == charges[1]) charges = std::string{charges[0]} + std::to_string(charges.size());
            }
            if(std::stoi(charges) == 0) {
                charges = "+0";
            }
        }
        
        if(hydrogens.empty()) {
            //hydrogens = "H0";
        } else {
            if(hydrogens.size() > 1 && std::stoi(hydrogens.substr(1)) == 0) hydrogens = "H0";
            if(hydrogens.size() == 1) {
                hydrogens += "1";
            }
        }
        
        if(invert_all_chiral_centers) {
            if(atsign == "@") {
                atsign = "@@";
            } else if(atsign == "@@") {
                atsign = "@";
            }
        }
        
        std::string new_label;
        new_label += "[";
        new_label += isotope;
        new_label += symbol;
        new_label += atsign;
        if(output_hydrogen) new_label += hydrogens;
        new_label += charges;
        if(output_degree) {
            if(!degree.empty()) new_label += std::string{";"} + degree;
        }
        new_label += mapno;
        new_label += "]";
        
        res += std::string{rit->prefix()} + new_label;
        
        suffix = rit->suffix();
        rit++;
    }

    return res + suffix;
}

// return chiral_tag, has_same_neighbor
// if(has_same_neighbor) chiral_tag = 0;
std::pair<int, bool> compute_chiral_flag(
    const RDKit::Atom* const atom,
    const std::unordered_map<int, std::size_t>& bond_repr
) {
    const auto chiral_tag = atom->getChiralTag();

#ifndef NDEBUG
    std::cout << "compute_chiral_flag(): chiral_tag=" << chiral_tag << std::endl;
#endif // NDEBUG
    
    if(chiral_tag == RDKit::Atom::CHI_UNSPECIFIED or chiral_tag == RDKit::Atom::CHI_OTHER) {
        return {0, false};
    }
    
    int tag{1};
    if(chiral_tag == RDKit::Atom::CHI_TETRAHEDRAL_CCW) tag *= -1;
    if(chiral_tag == RDKit::Atom::CHI_TETRAHEDRAL_CW)  tag *= 1;
    
    std::vector<std::size_t> repr;
    repr.reserve(4);
    std::set<std::size_t> repr_set;
    
    for(const auto atom_nei: AtomGetNeighbors(atom)) {
        const auto idx_nei = atom_nei->getIdx();
        const auto repr_nei = bond_repr.at(idx_nei);
        
#ifndef NDEBUG
        std::cout << "compute_chiral_flag(): idx_nei=" << idx_nei << ", mapno: " << atom_nei->getAtomMapNum() << ", bond_repr: " << bond_repr.at(idx_nei) << std::endl;
#endif // NDEBUG
        
        repr.push_back(repr_nei);
        
        if(repr_set.contains(repr_nei)) {
            return {0, true};
        }
        
        repr_set.insert(repr_nei);
    }
    
    if(repr.size() == 3) {
        repr.emplace_back(0); // implicit H, RDKit adds H to the second place, but results do not change if we add it to the last
    }
    
    const auto p = parity4(repr.data());
    
    if(p == 1) tag *= 1;
    if(p == 0) tag *= -1;
    
    return {tag, false};
}

// return chiral_tag, has_same_neighbor
// if(has_same_neighbor) chiral_tag = 0;
std::pair<int, bool> compute_chiral_flag(
    const RDKit::Bond* const bond,
    const std::unordered_map<int, std::string>& atom_repr
) {
    if(bond->getBondType() != RDKit::Bond::DOUBLE) return {0, false};
    
    if(atom_repr.empty()) return {0, true};
    
    int tag{1};
    
    auto double_bond_atoms = std::array{bond->getBeginAtom(), bond->getEndAtom()};
    std::vector<std::string> sub_atoms_repr;
    sub_atoms_repr.reserve(2);
    for(const auto double_bond_atom: double_bond_atoms) {
        const auto double_bond_atom_idx = double_bond_atom->getIdx();
        
        int sub_tag{0};
        sub_atoms_repr.clear();
        for(const auto sub_bond: AtomGetBonds(double_bond_atom)) {
            if(sub_bond->getBondType() != RDKit::Bond::DOUBLE) {
                const auto sub_atom_idx = sub_bond->getOtherAtomIdx(double_bond_atom_idx);
                const auto bond_dir = sub_bond->getBondDir();
                sub_atoms_repr.push_back(atom_repr.at(sub_atom_idx));
                
                // assuming no conflicting directions
                // after push_back
                const auto n = sub_atoms_repr.size();
                
                if(bond_dir == RDKit::Bond::ENDUPRIGHT || bond_dir == RDKit::Bond::ENDDOWNRIGHT) {
                    if(n == 1 && bond_dir == RDKit::Bond::ENDUPRIGHT)   sub_tag = 1;
                    else if(n == 1 && bond_dir == RDKit::Bond::ENDDOWNRIGHT) sub_tag = -1;
                    else if(n == 2 && bond_dir == RDKit::Bond::ENDDOWNRIGHT) sub_tag = 1;
                    else if(n == 2 && bond_dir == RDKit::Bond::ENDUPRIGHT)   sub_tag = -1;
                    
                    if(sub_bond->getBeginAtomIdx() != double_bond_atom_idx) sub_tag *= -1;
                }
                
#ifndef NDEBUG
                std::cout << "compute_chiral_flag(): sub_atom_idx=" << sub_atom_idx << ", mapno: " << sub_bond->getOtherAtom(double_bond_atom)->getAtomMapNum() << ", bond_dir=" << bond_dir << ", n=" << n << ", sub_tag=" << sub_tag << ", bond_repr: " << atom_repr.at(sub_atom_idx) << std::endl;
#endif // NDEBUG
            }
        }
        
        if(sub_tag == 0) return {0, false}; // no bond dir
        
        if(sub_atoms_repr.size() == 0) return {0, false}; // two H
        if(sub_atoms_repr.size() == 1) sub_atoms_repr.emplace_back("H"); // one H, may not work if the other H is in graph
        
        if(sub_atoms_repr[0] == sub_atoms_repr[1]) return {0, true};  // has bond dir, but need to re-label
        
        sub_tag *= (sub_atoms_repr[0] > sub_atoms_repr[1] ? 1 : -1);
        
        tag *= sub_tag;
    }
    
    return {tag, false};
}

// label, chiral_atom_has_same_neighbor
// NOTE: if a chiral atom has same neighbors, it is marked as achiral and chiral_atom_has_same_neighbor=true
std::string get_bond_atom_label(
    const RDKit::RWMOL_SPTR& pattern,
    RDKit::Atom* const atom,
    const std::unordered_map<int, int>& mapno_to_tag, // tie breaking tag
    const std::unordered_map<int, std::string>& atoms_str = {}
) {
    std::hash<std::string> string_hash;
    
    const auto atom_idx = atom->getIdx();
    
    const auto degree = atom->getDegree(); // include in final representation
    const int mapno = atom->getAtomMapNum();
    
    const int atom_tag = mapno_to_tag.contains(mapno) ? mapno_to_tag.at(mapno) : 0;
    
    atom->setAtomMapNum(0); // clear mapno
    const std::string atom_str = AtomGetSmarts(atom); // include in final representation
    atom->setAtomMapNum(mapno); // restore mapno
    
#ifndef NDEBUG
    std::cout << "mapno=" << mapno << ", atom_str=" << atom_str << std::endl;
#endif // NDEBUG
    
    // hash all neighbor bond+atom
    std::size_t bond_atom_hash{0}; // include in final representation
    std::unordered_map<int, std::size_t> bond_atom_repr; // neighbor atom id -> str
    for(const auto& bond_iter_desc: boost::make_iterator_range(pattern->getAtomBonds(atom))) {
        const auto bond = (*pattern)[bond_iter_desc];
        const std::string bond_smarts = BondGetSmarts(bond);
        const auto [bond_stereo, double_bond_has_same_neighbors] = compute_chiral_flag(bond, atoms_str);
        std::size_t bond_hash = string_hash(bond_smarts + std::to_string(bond_stereo));
        
        const auto other_atom_idx = bond->getOtherAtomIdx(atom_idx);
        const auto other_atom = pattern->getAtomWithIdx(other_atom_idx);
        
        const auto other_atom_mapno = other_atom->getAtomMapNum();
        other_atom->setAtomMapNum(0); // clear mapno
        const std::string other_atom_str = AtomGetSmarts(other_atom);
        other_atom->setAtomMapNum(other_atom_mapno); // restore mapno
        
        const int other_atom_tag = mapno_to_tag.contains(other_atom_mapno) ? mapno_to_tag.at(other_atom_mapno) : 0;
        
        std::size_t other_atom_hash = string_hash(other_atom_str + std::to_string(other_atom_tag));
        
        std::size_t this_bond_atom_hash{0};
        boost::hash_combine(this_bond_atom_hash, other_atom_hash);
        boost::hash_combine(this_bond_atom_hash, bond_hash);
    
        if(atoms_str.contains(other_atom_idx)) {
            boost::hash_combine(this_bond_atom_hash, string_hash(atoms_str.at(other_atom_idx)));
        }
        
        bond_atom_hash += this_bond_atom_hash; // must be commutative and associative binary op
        bond_atom_repr[other_atom_idx] = this_bond_atom_hash;
        
#ifndef NDEBUG
        std::cout << "other_atom_mapno=" << other_atom_mapno << ", other_atom_str: " << other_atom_str << ", other_atom_tag: " << other_atom_tag << ", bond_smarts: " << bond_smarts << ", bond_dir=" << bond->getBondDir() << ", bond_stereo=" << bond->getStereo() << ", bond_stereo_computed=" << bond_stereo << ", bond_hash=" << bond_hash << ", this_bond_atom_hash=" << this_bond_atom_hash << ", atoms_str.at(other_atom_idx): " << (atoms_str.contains(other_atom_idx)?atoms_str.at(other_atom_idx):std::string("0")) <<  std::endl;
#endif // NDEBUG
    }
    
    const auto [chiral_flag, chiral_atom_has_same_neighbor] = compute_chiral_flag(atom, bond_atom_repr); // a custom local flag
    
#ifndef NDEBUG
    std::cout << "chiral_flag=" << chiral_flag << ", chiral_atom_has_same_neighbor=" << chiral_atom_has_same_neighbor << std::endl;
#endif // NDEBUG
    
    std::string label;
    label += std::to_string(degree);
    label += atom_str + std::to_string(chiral_flag);
    label += std::to_string(bond_atom_hash);
    
    if(atoms_str.contains(atom_idx)) {
        label += std::to_string(string_hash(atoms_str.at(atom_idx)));
    }
    
    label += std::to_string(atom_tag);
    
    return label;
}

std::unordered_map<int, std::pair<int, int>> build_mapno_to_atomidx(
    const RDKit::RWMOL_SPTR& reactants_pattern,
    const RDKit::RWMOL_SPTR& products_pattern
) {
    std::unordered_map<int, std::pair<int, int>> mapno_to_atomidx;
    mapno_to_atomidx.reserve(reactants_pattern->getNumAtoms());
    for(const auto atom: reactants_pattern->atoms()) {
        const int atom_idx = atom->getIdx();
        const int mapno = atom->getAtomMapNum();
        if(mapno > 0) {
            if(mapno_to_atomidx.contains(mapno)) throw std::runtime_error{"canonicalize_transform(): duplicate mapno"};
            mapno_to_atomidx.emplace(mapno, std::pair<int, int>{atom_idx, -1});
        }
    }
    for(const auto atom: products_pattern->atoms()) {
        const int atom_idx = atom->getIdx();
        const int mapno = atom->getAtomMapNum();
        if(mapno > 0) {
            if(mapno_to_atomidx.contains(mapno)) {
                mapno_to_atomidx.at(mapno).second = atom_idx;
            } else {
                mapno_to_atomidx.emplace(mapno, std::pair<int, int>{-1, atom_idx});
            }
        }
    }
    return mapno_to_atomidx;
}

std::vector<std::vector<int>>
build_condensed_graph_adjacency_matrix(
     const std::unordered_map<int, std::pair<int, int>>& mapno_to_atomidx,
     const RDKit::RWMOL_SPTR& reactants_mol,
     const RDKit::RWMOL_SPTR& products_mol
) {
    // find max mapno, mapno may not be contiguous
    int _max_mapno{0};
    for(const auto [mapno, atomidx]: mapno_to_atomidx) {
        _max_mapno = std::max(_max_mapno, mapno);
    }
    
    auto adj_mat = std::vector(_max_mapno+1, std::vector<int>{}); // 0 is not a valid mapno
    
    for(const auto [mapno, atomidx]: mapno_to_atomidx) {
        const auto [reactant_atomidx, product_atomidx] = atomidx;
        
        auto& adj_list = adj_mat.at(mapno);
        
        if(reactant_atomidx >= 0) {
            for(const auto adj_atom: AtomGetNeighbors(reactants_mol->getAtomWithIdx(reactant_atomidx))) {
                adj_list.push_back(adj_atom->getAtomMapNum());
            }
        }
        
        if(product_atomidx >= 0) {
            for(const auto adj_atom: AtomGetNeighbors(products_mol->getAtomWithIdx(product_atomidx))) {
                adj_list.push_back(adj_atom->getAtomMapNum());
            }
        }
    }
    
    return adj_mat;
}

/* Remove the bond directions for the following case
   A     C
    \   / 
     C=C
    /   \ 
   A     D
 */
int clear_invalid_double_bond_stereo(
    const RDKit::RWMOL_SPTR& mol,
    const std::unordered_map<int, std::string>& atom_str // atomIdx -> repr
) {
    int cnt{0};
    
    for(auto bond: mol->bonds()) {
        if(bond->getBondType() != RDKit::Bond::DOUBLE) {
            continue;
        }
        
        if(bond->getStereo() == RDKit::Bond::STEREONONE || bond->getStereo() == RDKit::Bond::STEREOANY) {
            continue;
        }
        
        auto ba = bond->getBeginAtom();
        auto bb = bond->getEndAtom();
        
        bool need_remove_stereo{false};
        std::vector<RDKit::Bond*> bonds;
        bonds.reserve(4);
        
        if(ba->getDegree() > 1) {
            auto idx = ba->getIdx();
            
            std::vector<std::string> repr;
            repr.reserve(2);
            for(const auto i: AtomGetBonds(ba)) {
                if(i->getBondType() != RDKit::Bond::DOUBLE) {
                    auto idx1 = i->getBeginAtom()->getIdx();
                    if(idx1 != idx) {
                        repr.push_back(atom_str.at(idx1));
                        bonds.push_back(i);
                    }
                    auto idx2 = i->getEndAtom()->getIdx();
                    if(idx2 != idx) {
                        repr.push_back(atom_str.at(idx2));
                        bonds.push_back(i);
                    }
                }
            }
            
            if(repr.size() == 2 && repr[0] == repr[1]) {
                need_remove_stereo = true;
            }
        }
        
        if(bb->getDegree() > 1 && !need_remove_stereo) {
            auto idx = bb->getIdx();
            
            std::vector<std::string> repr;
            repr.reserve(2);
            for(const auto i: AtomGetBonds(bb)) {
                if(i->getBondType() != RDKit::Bond::DOUBLE) {
                    auto idx1 = i->getBeginAtom()->getIdx();
                    if(idx1 != idx) {
                        repr.push_back(atom_str.at(idx1));
                        bonds.push_back(i);
                    }
                    auto idx2 = i->getEndAtom()->getIdx();
                    if(idx2 != idx) {
                        repr.push_back(atom_str.at(idx2));
                        bonds.push_back(i);
                    }
                }
            }
            
            if(repr.size() == 2 && repr[0] == repr[1]) {
                need_remove_stereo = true;
            }
        }
        
        if(need_remove_stereo) {
            cnt++;
            bond->setStereo(RDKit::Bond::STEREONONE);
            for(auto b: bonds) {
                b->setBondDir(RDKit::Bond::NONE);
            }
        }
    }
    
    return cnt;
}

/* Remove the chiral tag for the following case
   A   C
    \ / 
     C
    / \ 
   A   D
   
   Limitation: May not deal with chirality dependency correctly.
   
 */
int clear_invalid_tetra_stereo(
    const RDKit::RWMOL_SPTR& mol,
    const std::unordered_map<int, std::string>& atom_str // atomIdx -> repr
) {
    int cnt{0};
    std::set<std::string> atom_id;
    
    for(const auto atom: mol->atoms()) {
        if(atom->getChiralTag() == RDKit::Atom::CHI_UNSPECIFIED) {
            continue;
        }
        
        bool need_remove{false};
        atom_id.clear();
    
        for(const auto atom_nei: AtomGetNeighbors(atom)) {
            auto id = atom_str.at(atom_nei->getIdx());
            atom_id.insert(id);
        }
        
        // has H?
        const auto atom_smarts = AtomGetSmarts(atom);
        bool has_H{false};
        const auto H_iter = atom_smarts.find("H");
        if(H_iter == std::string::npos) {
            // no H
            has_H = false;
        } else {
            // has H
            // check if H0
            if(H_iter + 1 < atom_smarts.size() && atom_smarts[H_iter+1] != '0') {
                has_H = true;
            } else {
                has_H = false;
            }
        }
        
        if(has_H && atom_id.size() < 3) need_remove = true;
        if(!has_H && atom_id.size() < 4) need_remove = true;
        
        if(need_remove) {
            cnt++;
            atom->setChiralTag(RDKit::Atom::CHI_UNSPECIFIED);
        }
    }
    
    return cnt;
}

void reassign_atom_mapping_mol(
    const RDKit::RWMOL_SPTR& reactants_pattern,
    const RDKit::RWMOL_SPTR& products_pattern
) {
    std::unordered_map<int, std::pair<int, int>> mapno_to_atomidx = build_mapno_to_atomidx(reactants_pattern, products_pattern);
    
    for(int i = 0; const auto [mapno, atom_idx_pair]: mapno_to_atomidx) {
        i++; // new mapno
        
        const auto [reactant_atom_idx, product_atom_idx] = atom_idx_pair;
        
        if(reactant_atom_idx >= 0) {
            reactants_pattern->getAtomWithIdx(reactant_atom_idx)->setAtomMapNum(i);
        }
        
        if(product_atom_idx >= 0) {
            products_pattern->getAtomWithIdx(product_atom_idx)->setAtomMapNum(i);
        }
    }
}

void reassign_atom_mapping_mol(
    const RDKit::ROMOL_SPTR& reactants_pattern,
    const RDKit::ROMOL_SPTR& products_pattern
) {
    reassign_atom_mapping_mol(
        boost::dynamic_pointer_cast<RDKit::RWMol>(reactants_pattern),
        boost::dynamic_pointer_cast<RDKit::RWMol>(products_pattern)
    );
}

std::vector<std::vector<int>>
compute_vertex_partition(const std::unordered_map<int, std::string>& atoms_str_by_mapno) {
    std::unordered_map<std::string, std::vector<int>> mapno2atom_str;
    mapno2atom_str.reserve(atoms_str_by_mapno.size());
    
    for(const auto& [mapno, str]: atoms_str_by_mapno) {
        mapno2atom_str[str].push_back(mapno);
    }
    
    std::vector<std::vector<int>> res;
    res.reserve(mapno2atom_str.size());
    for(const auto& [str, mapno_list]: mapno2atom_str) {
        res.emplace_back(mapno_list);
    }
    
    // sort partition
    for(auto& i: res) {
        std::sort(i.begin(), i.end());
    }
    // std::sort(res.begin(), res.end()); // requires C++20, gcc10
    std::sort(
        res.begin(),
        res.end(),
        // gcc 9
        [](const std::vector<int>& a, const std::vector<int>& b) {
            for(std::size_t i{0}; i < a.size() && i < b.size(); i++) {
                if(a[i] != b[i]) return a[i] < b[i];
            }
            return a.size() < b.size();
        }
    );
    
    return res;
}

// canonicalize a single smarts, keep mapno but don't use them for ranking
std::string canonicalize_smarts(const std::string& smarts) {
    auto smarts_canonicalized_atoms = canonicalize_smarts_atom(smarts);
    RDKit::RWMOL_SPTR pattern(RDKit::SmartsToMol(smarts_canonicalized_atoms));
    
    // compute canonical rank
    typedef std::tuple<int, std::string> tuple_int_str_t;
    std::vector<tuple_int_str_t> atoms_str;
    atoms_str.reserve(pattern->getNumAtoms());
    
    // perform Weisfeiler-Lehman isomorphism test
    std::unordered_map<int, std::string> map_atoms_str; // atomIdx -> repr
    map_atoms_str.reserve(pattern->getNumAtoms());
    
    std::vector<std::vector<int>> atoms_str_partition_by_mapno_prev;
    
    for(size_t i = 0; i < pattern->getNumAtoms(); i++) {
        // compute atoms_str
        atoms_str.clear();
        for(const auto atom: pattern->atoms()) {
            auto label = get_bond_atom_label(pattern, atom, {}, map_atoms_str);
            
            atoms_str.emplace_back(atom->getIdx(), std::move(label));
        }
        
        // must be after the fisrt round of labeling
        for(const auto& [atomidx, s]: atoms_str) {
            map_atoms_str[atomidx] = s;
        }
        
        auto current_partition = compute_vertex_partition(map_atoms_str);
        if(atoms_str_partition_by_mapno_prev == current_partition) {
            break;
        }
        atoms_str_partition_by_mapno_prev = std::move(current_partition);
    }
    
    std::sort(
        atoms_str.begin(),
        atoms_str.end(),
        [](const tuple_int_str_t& a, const tuple_int_str_t& b) -> bool {
            return std::get<1>(a) < std::get<1>(b);
        }
    );
    
    std::vector<unsigned int> order;
    order.reserve(atoms_str.size());
    for(const auto& [idx, s]: atoms_str) {
        order.push_back(idx);
    }
    
    RDKit::ROMOL_SPTR pattern_reorder(RDKit::MolOps::renumberAtoms(*pattern, order));
    
    return RDKit::MolToSmarts(*pattern_reorder, true);
}

typedef std::tuple<int, int, std::string> tuple_int_int_str_t;

void WL_label(
    const RDKit::RWMOL_SPTR& reactants_pattern,
    const RDKit::RWMOL_SPTR& products_pattern,
    const std::unordered_map<int, std::pair<int, int>>& mapno_to_atomidx,
    const std::unordered_map<int, int>& mapno_to_tag, // tie-breaking tag
// passing by mutable ref
    std::vector<tuple_int_int_str_t>& atoms_str,
    std::unordered_map<int, std::string>& reactant_atoms_str,
    std::unordered_map<int, std::string>& product_atoms_str
) {
#ifndef NDEBUG
    std::cout << std::endl << "WL_label, reaction: " << RDKit::MolToSmarts(*reactants_pattern, true)+">>"+RDKit::MolToSmarts(*products_pattern, true) << std::endl;
#endif // NDEBUG
    atoms_str.clear();
    reactant_atoms_str.clear();
    product_atoms_str.clear();
    
    std::unordered_map<int, std::string> atoms_str_by_mapno;
    std::vector<std::vector<int>> atoms_str_partition_by_mapno_prev;
    
    // size*2: two graphs, for nested topology
    for(size_t i = 0; i < mapno_to_atomidx.size()*2; i++) {
#ifndef NDEBUG
        std::cout << std::endl << "WL_label, i=" << i << std::endl;
#endif // NDEBUG
        
        atoms_str_by_mapno.clear();
        
        // compute atoms_str
        atoms_str.clear();
        for(const auto [mapno, atomidx]: mapno_to_atomidx) {
            const auto [reactant_atomidx, product_atomidx] = atomidx;
            
            std::string reactant_label, product_label;
            
            if(reactant_atomidx >= 0) {
                auto reactant_atom = reactants_pattern->getAtomWithIdx(reactant_atomidx);
                reactant_label = get_bond_atom_label(reactants_pattern, reactant_atom, mapno_to_tag, reactant_atoms_str);
            }
            
            if(product_atomidx >= 0) {
                auto product_atom = products_pattern->getAtomWithIdx(product_atomidx);
                product_label = get_bond_atom_label(products_pattern, product_atom, mapno_to_tag, product_atoms_str);
            }
            
#ifndef NDEBUG
            std::cout << "mapno=" << mapno
            << ", reactant_atomidx: " << reactant_atomidx
            << ", product_atomidx: " << product_atomidx
            << ", reactant_label: " << reactant_label
            << ", product_label: " << product_label << std::endl;
#endif // NDEBUG
            
            auto label = reactant_label + product_label;
            
            atoms_str.emplace_back(reactant_atomidx, product_atomidx, label);
            atoms_str_by_mapno.emplace(mapno, label);
        }
        
        // must be after the fisrt round of labeling
        for(const auto& [reactant_atomidx, product_atomidx, s]: atoms_str) {
            if(reactant_atomidx >= 0) reactant_atoms_str[reactant_atomidx] = s;
            if(product_atomidx >= 0) product_atoms_str[product_atomidx] = s;
        }
        
        auto current_partition = compute_vertex_partition(atoms_str_by_mapno);
        if(atoms_str_partition_by_mapno_prev == current_partition) {
            break;
        }
        
        atoms_str_partition_by_mapno_prev = std::move(current_partition);
    }
}

// canonicalize a pair of smarts (aka template)
std::string canonicalize_transform1(const std::string& transform) {
    auto transform_canonicalized_atoms = canonicalize_smarts_atom(transform);
    
    std::vector<std::string> transform_split;
    transform_split.reserve(4);
    boost::algorithm::split(transform_split, transform_canonicalized_atoms, boost::is_any_of(">"));
    
    auto& reactants = transform_split[0];
    auto& products  = transform_split[2];
    
    RDKit::RWMOL_SPTR reactants_pattern(RDKit::SmartsToMol(reactants));
    RDKit::RWMOL_SPTR products_pattern(RDKit::SmartsToMol(products));
    
    auto original_max_mapno_reactant = max_mapno(reactants_pattern);
    auto original_max_mapno_product = max_mapno(products_pattern);
    int original_max_mapno = std::max(original_max_mapno_reactant, original_max_mapno_product);
    int start_mapno2 = assign_mapno(reactants_pattern, original_max_mapno+1);
    assign_mapno(products_pattern, start_mapno2);
    
    // mapno to atom_idx
    std::unordered_map<int, std::pair<int, int>> mapno_to_atomidx = build_mapno_to_atomidx(reactants_pattern, products_pattern);
    
    // compute canonical rank
    std::vector<tuple_int_int_str_t> atoms_str;
    atoms_str.reserve(mapno_to_atomidx.size());
    
    // perform Weisfeiler-Lehman isomorphism test
    std::unordered_map<int, std::string> reactant_atoms_str, product_atoms_str; // atomIdx -> repr
    reactant_atoms_str.reserve(mapno_to_atomidx.size());
    product_atoms_str.reserve(mapno_to_atomidx.size());
    
    std::unordered_map<int, int> mapno_to_tag;
    
    // DFS to determine sub-graph size: resolve complex nested topology
    auto adj_mat = build_condensed_graph_adjacency_matrix(mapno_to_atomidx, reactants_pattern, products_pattern);
    auto connected_mapno = dfs(adj_mat);
    for(const auto& one_graph: connected_mapno) {
        const auto graph_size = one_graph.size();
        for(const auto _mapno: one_graph) {
            mapno_to_tag[_mapno] = graph_size;
        }
    }
    
    // similar to findPotentialStereo() algorithm in RDKit
    // https://www.rdkit.org/docs/RDKit_Book.html#brief-description-of-the-findpotentialstereo-algorithm
    while(true) {
        WL_label(
            reactants_pattern,
            products_pattern,
            mapno_to_atomidx,
            mapno_to_tag, // tie-breaking tag
            atoms_str,
            reactant_atoms_str,
            product_atoms_str
        );
        
        int cnt{0};
        
        cnt += clear_invalid_double_bond_stereo(reactants_pattern, reactant_atoms_str);
        cnt += clear_invalid_double_bond_stereo(products_pattern, product_atoms_str);
        
        cnt += clear_invalid_tetra_stereo(reactants_pattern, reactant_atoms_str);
        cnt += clear_invalid_tetra_stereo(products_pattern, product_atoms_str);
        
#ifndef NDEBUG
        std::cout << "clear invalid cnt=" << cnt << std::endl;
#endif // NDEBUG
        
        if(cnt == 0) {
            break;
        }
    }
    
    for(auto& [reactant_atomidx, product_atomidx, s]: atoms_str) {
        std::string prefix;
        
        if(reactant_atomidx >= 0) {
            prefix += "!"; // '!' is small
        } else {
            prefix += "~"; // '~' is large
        }
        
        if(product_atomidx >= 0) {
            prefix += "!"; // '!' is small
        } else {
            prefix += "~"; // '~' is large
        }
        
        s = prefix + s; // sort non-mapped to the last, 
    }
    
    auto atoms_str_less = [](const tuple_int_int_str_t& a, const tuple_int_int_str_t& b) -> bool {
        return std::get<2>(a) < std::get<2>(b);
    };
    
    [[maybe_unused]]
    auto atoms_str_equal = [](const tuple_int_int_str_t& a, const tuple_int_int_str_t& b) -> bool {
        return std::get<2>(a) == std::get<2>(b);
    };
    
    const auto atoms_str_no_tie_break = atoms_str;
        
    std::vector<tuple_int_int_str_t> atoms_str_tie_break;
    atoms_str_tie_break.reserve(mapno_to_atomidx.size());
    std::unordered_map<int, std::string> reactant_atoms_str_tie_break, product_atoms_str_tie_break; // atomIdx -> repr
    reactant_atoms_str_tie_break.reserve(mapno_to_atomidx.size());
    product_atoms_str_tie_break.reserve(mapno_to_atomidx.size());
    
    int tie_breaking_tag{1000};
    assert(tie_breaking_tag > mapno_to_tag.size());
    
    // atoms_str = atoms_str_no_tie_break + atoms_str_tie_break
    while(true) {
        std::sort(
            atoms_str.begin(),
            atoms_str.end(),
            atoms_str_less
        );
    
#ifndef NDEBUG
        std::cout << "reactants smarts: " << RDKit::MolToSmarts(*reactants_pattern, true) << std::endl;
        std::cout << "products smarts: " << RDKit::MolToSmarts(*products_pattern, true) << std::endl;
        std::cout << "mapno_to_tag: " << std::endl;
        for(auto [_mapno, _tag]: mapno_to_tag) {
            std::cout << "_mapno=" << _mapno << ", _tag=" << _tag << std::endl;
        }
        std::cout << std::endl;
        std::cout << "atoms_str sorted:" << std::endl;
        for(auto& [ridx, pidx, s]: atoms_str) {
            std::cout << "ridx: " << ridx << ", pidx: " << pidx;
            if(ridx >= 0) {
                const auto atom = reactants_pattern->getAtomWithIdx(ridx);
                std::cout << ", r_mapno: " << atom->getAtomMapNum();
                std::cout << ", atom_smarts: " << AtomGetSmarts(atom);
            }
            if(pidx >= 0) {
                const auto atom = products_pattern->getAtomWithIdx(pidx);
                std::cout << ", p_mapno: " << atom->getAtomMapNum();
                std::cout << ", atom_smarts: " << AtomGetSmarts(atom);
            }
            
            std::cout << ", atoms_str: " << s;
            std::cout << std::endl;
        }
#endif // NDEBUG
        
        // must be sorted
        auto iter = std::adjacent_find(atoms_str.begin(),
            atoms_str.end(),
            atoms_str_equal
        );
        bool is_duplicate = iter != atoms_str.end();
        
        if(!is_duplicate) {
            break;
        }
        
        // tie breaking for mapno
        
        const int reactant_idx = std::get<0>(*iter);
        const int product_idx = std::get<1>(*iter);
        int mapno{0};
        if(reactant_idx >= 0) {
            mapno = reactants_pattern->getAtomWithIdx(reactant_idx)->getAtomMapNum();
        } else {
            assert(product_idx >= 0);
            mapno = products_pattern->getAtomWithIdx(product_idx)->getAtomMapNum();
        }
        assert(mapno > 0);
    
#ifndef NDEBUG
        std::cout << "is_duplicate: " << is_duplicate << std::endl;
        std::cout << "reactant_idx: " << reactant_idx << ", product_idx: " << product_idx << ", mapno: " << mapno << std::endl;
#endif // NDEBUG
        
        mapno_to_tag[mapno] += ++tie_breaking_tag;
        
        WL_label(
            reactants_pattern,
            products_pattern,
            mapno_to_atomidx,
            mapno_to_tag, // tie-breaking tag
            atoms_str_tie_break,
            reactant_atoms_str_tie_break,
            product_atoms_str_tie_break
        );
        
        // rebuild atoms_str by merge atoms_str_no_tie_break + atoms_str_tie_break
        atoms_str.clear();
        for(const auto& [reactant_atomidx, product_atomidx, s]: atoms_str_no_tie_break) {
            std::string str_tie_break;
            if(reactant_atomidx >= 0) {
                str_tie_break = reactant_atoms_str_tie_break.at(reactant_atomidx);
            } else {
                str_tie_break = product_atoms_str_tie_break.at(product_atomidx);
            }
            atoms_str.emplace_back(reactant_atomidx, product_atomidx, s + "!" + str_tie_break);
        }
    }
    
    // re-assign mapno: removed mapno for originally unmapped atoms
    for(size_t i = 0; i < atoms_str.size(); i++) {
        const int reactant_idx = std::get<0>(atoms_str[i]);
        const int product_idx = std::get<1>(atoms_str[i]);
        
        int mapno = i + 1;
        if(reactant_idx >= 0 && product_idx >= 0) {
            reactants_pattern->getAtomWithIdx(reactant_idx)->setAtomMapNum(mapno);
            products_pattern->getAtomWithIdx(product_idx)->setAtomMapNum(mapno);
        } else {
            if(reactant_idx >= 0) reactants_pattern->getAtomWithIdx(reactant_idx)->setAtomMapNum(0);
            if(product_idx >= 0) products_pattern->getAtomWithIdx(product_idx)->setAtomMapNum(0);
        }
    }
    
    std::vector<unsigned int> reactants_order, products_order;
    reactants_order.reserve(atoms_str.size());
    products_order.reserve(atoms_str.size());
    for(const auto& [reactant_idx, product_idx, s]: atoms_str) {
        if(reactant_idx >= 0) reactants_order.push_back(reactant_idx);
        if(product_idx >= 0) products_order.push_back(product_idx);
    }
    
#ifndef NDEBUG
    std::cout << "reactants smarts: " << RDKit::MolToSmarts(*reactants_pattern, true) << std::endl;
    std::cout << "reactants_pattern order:" << std::endl;
    for(const auto atom: reactants_pattern->atoms()) {
        std::cout << AtomGetSmarts(atom) << "  " << reactants_order[atom->getIdx()] << std::endl;
    }
    std::cout << "reactants_pattern order end" << std::endl;
    std::cout << "products smarts: " << RDKit::MolToSmarts(*products_pattern, true) << std::endl;
    std::cout << "products_pattern order:" << std::endl;
    for(const auto atom: products_pattern->atoms()) {
        std::cout << AtomGetSmarts(atom) << "  " << products_order[atom->getIdx()] << std::endl;
    }
    std::cout << "products_pattern order end" << std::endl;
#endif // NDEBUG
    
    // canonicalize atom order
    RDKit::ROMOL_SPTR reactants_pattern_reorder(RDKit::MolOps::renumberAtoms(*reactants_pattern, reactants_order));
    RDKit::ROMOL_SPTR products_pattern_reorder(RDKit::MolOps::renumberAtoms(*products_pattern, products_order));
    
    return RDKit::MolToSmarts(*reactants_pattern_reorder, true) + ">>" + RDKit::MolToSmarts(*products_pattern_reorder, true);
}

#ifdef NDEBUG
#define canonicalize_transform2 canonicalize_transform
#else
std::string canonicalize_transform(const std::string& transform, const bool chirality_inversion_equivalent) {
    return canonicalize_transform_consistency_check(transform, chirality_inversion_equivalent);
}
#endif // NDEBUG

std::string canonicalize_transform2(const std::string& transform, const bool chirality_inversion_equivalent) {
    auto res = canonicalize_transform1(transform);
    
    // compare the mirror
    // if(res.contains('@')) { // C++23
    if(chirality_inversion_equivalent && res.find("@") != std::string::npos) {
#ifndef NDEBUG
        std::cout << "chirality_inversion_equivalent" << std::endl;
#endif // NDEBUG
        
        auto transform_invert = canonicalize_smarts_atom(transform, true, true);
        auto res_invert_can = canonicalize_transform1(transform_invert);
            
#ifndef NDEBUG
        std::cout << "res_invert_can: " << res_invert_can << std::endl;
        std::cout << "res_no_invert_can: " << res << std::endl;
#endif // NDEBUG
        
        if(res < res_invert_can) {
            res = res_invert_can;
        }
    }
    
    return res;
}

// test consistency
std::string canonicalize_transform_consistency_check(const std::string& transform, const bool chirality_inversion_equivalent) {
#ifndef NDEBUG
    std::cout << std::endl << "canonicalize_transform starts: transform: " << transform << std::endl;
    std::cout << "canonicalize_transform starts: chirality_inversion_equivalent: " << chirality_inversion_equivalent << std::endl;
#endif // NDEBUG
    
    std::string res{transform};
    
    int i{0};
    constexpr int MAX_NUM_ITER{2};
    std::string res_prev{res};
    for(i = 0; i < MAX_NUM_ITER; i++) {
        res = canonicalize_transform2(res, chirality_inversion_equivalent);
        
#ifndef NDEBUG
        std::cout << "i=" << i << ", res: " << res << std::endl;
#endif // NDEBUG
        
        if(res_prev == res) {
            break;
        }
        
        res_prev = res;
    }
    if(i == MAX_NUM_ITER) {
        throw std::runtime_error{"canonicalize_transform(): ERROR: Cannot reach consistent transform after cycle="+std::to_string(i)+", transform: "+transform};
    }
    
    return res;
}

}
