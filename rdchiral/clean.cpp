#include <string>
#include <vector>
#include <exception>
#include <regex>

#include <boost/algorithm/string.hpp>

#include <GraphMol/GraphMol.h>
#include <GraphMol/SmilesParse/SmilesWrite.h>
#include <GraphMol/SmilesParse/SmilesParse.h>


namespace rdchiral {
std::string canonicalize_outcome_smiles(const std::string& smiles, const bool ensure) {
    std::string smiles_can;
    if(ensure) {
        const auto outcome = RDKit::RWMOL_SPTR{RDKit::SmilesToMol(smiles)};
        if(!outcome) {
            //if PLEVEL >= 1: print('~~ could not parse self?')
            //if PLEVEL >= 1: print('Attempted SMILES: {}', smiles)
            throw std::runtime_error{"canonicalize_outcome_smiles(): Cannot parse SMILES"};
        }
        smiles_can = RDKit::MolToSmiles(*outcome, true);
    }
    std::vector<std::string> smiles_can_split;
    boost::algorithm::split(smiles_can_split, smiles_can, boost::is_any_of("."));
    std::sort(smiles_can_split.begin(), smiles_can_split.end());
    std::string joined = boost::algorithm::join(smiles_can_split, ".");
    return joined;
}

std::unordered_set<std::string> combine_enantiomers_into_racemic(const std::unordered_set<std::string>& final_outcomes) {
    std::unordered_set<std::string> tmp{final_outcomes};
    
    static std::regex reg_expr{"@@"};
    static std::regex reg_expr_cis_trans1{R"((\/)([^=\.\\\/]+=[^=\.\\\/]+)(\/))"};
    static std::regex reg_expr_cis_trans2{R"((\\)([^=\.\\\/]+=[^=\.\\\/]+)(\\))"};
    const std::sregex_iterator regex_iter_end;
    
    const std::unordered_map<std::string, std::string> cis_trains_opposite{{"\\", "/"}, {"/", "\\"}};
    
    for(const auto& smiles: final_outcomes) {
        // Look for @@ tetrahedral center
        std::sregex_iterator regex_iter(smiles.begin(), smiles.end(), reg_expr);
        while(regex_iter != regex_iter_end) {
            const auto pos = regex_iter->position();
            const auto len = regex_iter->length();
            const auto str_prefix = smiles.substr(0, pos);
            const auto str_suffix = smiles.substr(pos+len);
            const auto smiles_inv = str_prefix + "@" + str_suffix;
            const auto iter = tmp.find(smiles_inv);
            if(iter != tmp.end()) {
                tmp.erase(iter);
                const auto iter2 = tmp.find(smiles);
                if(iter2 != tmp.end())
                    tmp.erase(iter2);
                // Re-parse smiles so that hydrogens can become implicit
                const auto smiles_new = str_prefix + str_suffix;
                const std::string smiles_new_can = canonicalize_outcome_smiles(smiles_new, true);
                tmp.insert(smiles_new_can);
            }
            regex_iter++;
        }

        // Look for // or \\ trans bond
        // where [^=\.] is any non-double bond or period or slash
        std::sregex_iterator regex_iter1(smiles.begin(), smiles.end(), reg_expr_cis_trans1);
        std::sregex_iterator regex_iter2(smiles.begin(), smiles.end(), reg_expr_cis_trans2);
        regex_iter = regex_iter1;
        regex_iter1 = regex_iter_end;
        if(regex_iter == regex_iter_end) {
            regex_iter = regex_iter2;
            regex_iter2 = regex_iter_end;
        }
        while(regex_iter != regex_iter_end) {
            const auto pos = regex_iter->position();
            const auto len = regex_iter->length();
            const auto str_prefix = smiles.substr(0, pos);
            const auto str_suffix = smiles.substr(pos+len);
            // See if cis version is present in list of outcomes
            const auto smiles_cis1 = str_prefix + \
                (*regex_iter)[1].str() + (*regex_iter)[2].str() + \
                cis_trains_opposite.at((*regex_iter)[3].str()) + str_suffix;
            const auto smiles_cis2 = str_prefix + 
                cis_trains_opposite.at((*regex_iter)[1].str()) + (*regex_iter)[2].str() + \
                (*regex_iter)[3].str() + str_suffix;
            // Also look for equivalent trans
            const auto smiles_trans2 = str_prefix + \
                cis_trains_opposite.at((*regex_iter)[1].str()) + (*regex_iter)[2].str() + \
                cis_trains_opposite.at((*regex_iter)[3].str()) + str_suffix;
            // Kind of weird remove conditionals...
            bool remove{false};
            auto iter = tmp.find(smiles_cis1);
            if(iter != tmp.end()) {
                tmp.erase(iter);
                remove = true;
            }
            iter = tmp.find(smiles_cis2);
            if(iter != tmp.end()) {
                tmp.erase(iter);
                remove = true;
            }
            iter = tmp.find(smiles_trans2);
            const auto iter_smiles = tmp.find(smiles);
            if(iter != tmp.end() && iter_smiles != tmp.end()) {
                tmp.erase(iter);
            }
            if(remove) {
                if(iter_smiles != tmp.end()) tmp.erase(iter_smiles);
                const auto smiles_new = str_prefix + (*regex_iter)[2].str() + str_suffix;
                const std::string smiles_new_can = canonicalize_outcome_smiles(smiles_new, true);
                tmp.insert(smiles_new_can);
            }
            
            regex_iter++;
            if(regex_iter == regex_iter_end) {
                regex_iter = regex_iter2;
                regex_iter2 = regex_iter_end;
            }
        }
    }
    return tmp;
}

}
