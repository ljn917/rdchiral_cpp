#include <GraphMol/SmilesParse/SmilesWrite.h>
#include <GraphMol/SmilesParse/SmilesParse.h>
#include <GraphMol/MolOps.h>
#include <GraphMol/ChemReactions/ReactionParser.h>
#include <GraphMol/ChemTransforms/ChemTransforms.h>

#include "rdchiral/rdchiral.hpp"
#include "rdchiral/chiral.hpp"
#include "rdchiral/clean.hpp"
#include "rdchiral/rdkit_helper.hpp"


namespace rdchiral {

std::pair<RDKit::ROMOL_SPTR, RDKit::ROMOL_SPTR>
get_template_frags_from_rxn(std::shared_ptr<RDKit::ChemicalReaction> rxn) {
    RDKit::ROMOL_SPTR template_r{nullptr}, template_p{nullptr};
    for(auto i: rxn->getReactants()) {
        auto rct = RDKit::ROMOL_SPTR{i};
        if(template_r)
            template_r = RDKit::ROMOL_SPTR{RDKit::combineMols(*template_r, *rct)};
        else
            template_r = rct;
    }
    for(auto i: rxn->getProducts()) {
        auto prd = RDKit::ROMOL_SPTR{i};
        if(template_p)
            template_p = RDKit::ROMOL_SPTR{RDKit::combineMols(*template_p, *prd)};
        else
            template_p = prd;
    }
    return std::pair(template_r, template_p);
}

std::shared_ptr<RDKit::ChemicalReaction> initialize_rxn_from_smarts(const std::string& reaction_smarts) {
    std::shared_ptr<RDKit::ChemicalReaction> rxn{RDKit::RxnSmartsToChemicalReaction(reaction_smarts)};
    if (!rxn->isInitialized()) {
        rxn->initReactantMatchers();
    }
    
    unsigned int numWarnings, numErrors;
    rxn->validate(numWarnings, numErrors);
    if(numErrors != 0) {
        throw std::runtime_error{
            std::string{"initialize_rxn_from_smarts(): reaction validation failed, numWarnings="}
            + std::to_string(numWarnings) + ", numErrors=" + std::to_string(numErrors)
        };
    }
    
    std::unordered_set<int> prd_maps;
    prd_maps.reserve(128);
    for(const auto prd: rxn->getProducts()) {
        for(auto a: prd->atoms()) {
            const int mapno = a->getAtomMapNum();
            if(mapno != 0) {
                prd_maps.insert(mapno);
            }
        }
    }

    int unmapped = 700;
    for(auto rct: rxn->getReactants()) {
        rct->updatePropertyCache(false);
        RDKit::MolOps::assignStereochemistry(*rct, false, true, true);
        // Fill in atom map numbers
        for(auto a: rct->atoms()) {
            if(a->getAtomMapNum() == 0 || !prd_maps.contains(a->getAtomMapNum())) {
                a->setAtomMapNum(unmapped);
                unmapped++;
            }
        }
    }
    //if PLEVEL >= 2: print('Added {} map nums to unmapped reactants'.format(unmapped-700))
    if(unmapped > 800)
        throw std::runtime_error{"initialize_rxn_from_smarts(): Too many unmapped atoms in the template reactants."};
    
    return rxn;
}

Reaction::Reaction(const std::string& reaction_smarts) :
    m_reaction_smarts{reaction_smarts},
    m_rxn{initialize_rxn_from_smarts(reaction_smarts)}
{
    std::tie(m_template_r, m_template_p) = get_template_frags_from_rxn(m_rxn);
    
    m_atoms_rt_map.reserve(m_template_r->getNumAtoms());
    m_atoms_rt_idx_to_map.reserve(m_template_r->getNumAtoms());
    for(const auto atom: m_template_r->atoms()) {
        const int mapno = atom->getAtomMapNum();
        m_atoms_rt_idx_to_map[atom->getIdx()] = mapno;
        if(mapno != 0) {
            m_atoms_rt_map[mapno] = atom;
        }
    }
    m_atoms_pt_map.reserve(m_template_p->getNumAtoms());
    m_atoms_pt_idx_to_map.reserve(m_template_p->getNumAtoms());
    for(const auto atom: m_template_p->atoms()) {
        const int mapno = atom->getAtomMapNum();
        m_atoms_pt_idx_to_map[atom->getIdx()] = mapno;
        if(mapno != 0) {
            m_atoms_pt_map[mapno] = atom;
        }
    }
    
/*#ifndef NDEBUG
    for(auto [k, v]: m_atoms_rt_map) {
        auto k2 = m_atoms_pt_map.find(k);
        if(k2 != m_atoms_pt_map.end()) {
            if(v->getAtomicNum() != k2->second->getAtomicNum()) {
                throw std::logic_error{"Reaction::Reaction(): Atomic identity should not change in a reaction!"};
            }
        }
    }
#endif // NDEBUG*/
    
    for(auto atom: m_template_r->atoms()) {
        template_atom_could_have_been_tetra(atom);
    }
    for(auto atom: m_template_p->atoms()) {
        template_atom_could_have_been_tetra(atom);
    }
    
    //m_rt_bond_dirs_by_mapnum = bond_dirs_by_mapnum(*m_template_r);
    //m_pt_bond_dirs_by_mapnum = bond_dirs_by_mapnum(*m_template_p);
    
    std::tie(m_required_rt_bond_defs, m_required_bond_defs_coreatoms) = \
            enumerate_possible_cistrans_defs(*m_template_r);
}

std::pair<
    std::vector<std::string>,
    std::unordered_map<std::string, std::pair<std::string, std::vector<int>>>
>
Reaction::run(Reactants& rct, const bool keep_mapnums, const bool combine_enantiomers, const bool relax_chiral_match, const bool verbose) {
    std::unordered_set<std::string> final_outcomes;
    std::unordered_map<std::string, std::pair<std::string, std::vector<int>>> mapped_outcomes;
    
    reset();
    
    std::vector<RDKit::MOL_SPTR_VECT> outcomes = m_rxn->runReactants(RDKit::MOL_SPTR_VECT{rct.m_reactants_achiral});
    
    if(verbose) {
        std::cout << "===================START========================" << std::endl;
        std::cout << "reactants: " << rct.smiles() << std::endl;
        std::cout << "templates: " << this->smarts() << std::endl;
        std::cout << "outcomes length: " << outcomes.size() << std::endl;
    }
    
    if(outcomes.empty()) {
        return std::pair(std::vector<std::string>(final_outcomes.begin(), final_outcomes.end()), std::move(mapped_outcomes));
    }
    
    for(auto& outcome: outcomes) {
        // if PLEVEL >= (2): print('Processing {}'.format(str([Chem.MolToSmiles(x, True) for x in outcome])))
        if(verbose) {
            std::cout << "------------------------------------------------" << std::endl;
            std::cout << "Processing outcome: " << std::endl;
            for(const auto m: outcome) {
                std::cout << RDKit::MolToSmiles(*m) << std::endl;
            }
            std::cout << std::endl;
        }
        constexpr int unmapped_start{900};
        int unmapped = unmapped_start;
        for(const auto m: outcome) {
            for(auto a: m->atoms()) {
                // Assign map number to outcome based on react_atom_idx
                if(a->hasProp("react_atom_idx")) {
                    a->setAtomMapNum(rct.idx_to_mapnum(std::stoi(a->getProp<std::string>("react_atom_idx"))));
                }
                if(a->getAtomMapNum() == 0) {
                    a->setAtomMapNum(unmapped, false);
                    unmapped++;
                }
            }
        }
        // if PLEVEL >= 2: print('Added {} map numbers to product'.format(unmapped-900))
        if(verbose) {
            std::cout << "Added " << (unmapped - unmapped_start) << " map numbers to products" << std::endl;
            std::cout << "Now the products are" << std::endl;
            for(const auto m: outcome) {
                std::cout << RDKit::MolToSmiles(*m) << std::endl;
            }
            std::cout << std::endl;
        }
        
        // mapno -> Atom*
        std::unordered_map<int, RDKit::Atom*> atoms_rt;
        atoms_rt.reserve(100);
        for(const auto m: outcome) {
            for(const auto a: m->atoms()) {
                if(a->hasProp("old_mapno")) {
                    int old_mapno = a->getProp<int>("old_mapno");
                    if(m_atoms_rt_map.contains(old_mapno)) {
                        atoms_rt[a->getAtomMapNum()] = m_atoms_rt_map.at(old_mapno);
                    }
                }
            }
        }
        
        for(const auto [i, a]: atoms_rt) {
            a->setAtomMapNum(i);
        }
        
        if(verbose) {
            std::cout << "Start to check tetra chirality" << std::endl;
        }
        int prev;
        bool prev_is_set{false}, skip_outcome{false};
        for(const auto [i, a]: atoms_rt) {
            const int match = atom_chirality_matches(a, rct.m_atoms_r.at(i), verbose);
            if(match == 0) {
                //if PLEVEL >= 2: print('Chirality violated! Should not have gotten this match')
                if(verbose) {
                    std::cout << "Chirality violated! Should not have gotten this match, mapno=" << i << std::endl;
                }
                
                if(relax_chiral_match) {
                    skip_outcome = false;
                    if(verbose) {
                        std::cout << "...but allowing it due to relax_chiral_match=" << relax_chiral_match << std::endl;
                    }
                } else {
                    // default, skip
                    skip_outcome = true;
                }
                
                break;
            } else if(match == 2) { // ambiguous case
                if(verbose) {
                    std::cout << "ambiguous case, mapno=" << i << std::endl;
                }
                continue;
            } else if(prev_is_set == false) {
                if(verbose) {
                    std::cout << "set prev_is_set=true, mapno=" << i << std::endl;
                }
                prev = match;
                prev_is_set = true;
            } else if(match != prev) {
                //if PLEVEL >= 2: print('Part of the template matched reactant chirality, part is inverted! Should not match')
                if(verbose) {
                    std::cout << "Part of the template matched reactant chirality, part is inverted! Should not match, mapno=" << i << std::endl;
                }
                skip_outcome = true;
                break;
            }
        }
        if(skip_outcome) {
            if(verbose) {
                std::cout << "skip_outcome=true, skip to next. Chirality mismatches" << std::endl;
            }
            continue;
        }
        // if PLEVEL >= 2: print('Chirality matches! Just checked with atom_chirality_matches')
        if(verbose) {
            std::cout << "Chirality matches! Just checked with atom_chirality_matches" << std::endl;
        }
        
        // Check bond chirality - iterate through reactant double bonds where
        // chirality is specified (or not). atoms defined by map number
        if(verbose) {
            std::cout << "Start to check double bond chirality" << std::endl;
        }
        skip_outcome = false;
        //for atoms, dirs, is_implicit in rct.m_atoms_across_double_bonds:
        for(const auto [atoms, dirs, is_implicit]: rct.m_atoms_across_double_bonds) {
            bool all_i_in_atoms_rt{true};
            for(const auto i: atoms) {
                if(!atoms_rt.contains(i)) {
                    all_i_in_atoms_rt = false;
                    break;
                }
            }
            //if all(i in atoms_rt for i in atoms):
            if(all_i_in_atoms_rt) {
                // All atoms definining chirality were matched to the reactant template
                // So, check if it is consistent with how the template is defined
                //...but /=/ should match \=\ since they are both trans...
                //auto matched_atom_map_nums = std::tuple(
                //    atoms_rt[atoms[0]]->getAtomMapNum(),
                //    atoms_rt[atoms[1]]->getAtomMapNum(),
                //    atoms_rt[atoms[2]]->getAtomMapNum(),
                //    atoms_rt[atoms[3]]->getAtomMapNum()
                //);

                // Convert atoms_rt to original template's atom map numbers:
                const auto matched_atom_map_nums = std::tuple(
                    m_atoms_rt_idx_to_map.at(atoms_rt.at(atoms[0])->getIdx()),
                    m_atoms_rt_idx_to_map.at(atoms_rt.at(atoms[1])->getIdx()),
                    m_atoms_rt_idx_to_map.at(atoms_rt.at(atoms[2])->getIdx()),
                    m_atoms_rt_idx_to_map.at(atoms_rt.at(atoms[3])->getIdx())
                );
                
                const auto iter = m_required_rt_bond_defs.find(matched_atom_map_nums);
                if(iter == m_required_rt_bond_defs.end())
                    continue; // this can happen in ring openings, for example
                const auto dirs_template = iter->second;
                if(dirs != dirs_template && \
                        std::pair(BondDirOpposite(dirs.first), BondDirOpposite(dirs.second)) != dirs_template && \
                        ! (dirs_template == std::pair(RDKit::Bond::NONE, RDKit::Bond::NONE) && is_implicit)) {
                    //if PLEVEL >= 5: print('Reactant bond chirality does not match template!')
                    //if PLEVEL >= 5: print('Based on map numbers...')
                    //if PLEVEL >= 5: print('  rct: {} -> {}'.format(matched_atom_map_nums, dirs))
                    //if PLEVEL >= 5: print('  tmp: {} -> {}'.format(matched_atom_map_nums, dirs_template))
                    //if PLEVEL >= 5: print('skipping this outcome, should not have matched...')
                    if(verbose) {
                        std::cout << "Reactant bond chirality does not match template!" << std::endl;
                        std::cout << "mapno -> rct, tmp: "
                                  << "("
                                  << std::get<0>(matched_atom_map_nums) << ","
                                  << std::get<1>(matched_atom_map_nums) << ","
                                  << std::get<2>(matched_atom_map_nums) << ","
                                  << std::get<3>(matched_atom_map_nums)
                                  << ")"
                                  << "->"
                                  << " reactants: (" << dirs.first << ", " << dirs.second << "), "
                                  << " templates: (" << dirs_template.first << ", " << dirs_template.second << "), "
                                  << std::endl;
                        std::cout << "skipping this outcome, should not have matched..." << std::endl;
                    }
                    skip_outcome = true;
                    break;
                }
            }
        }
        if(verbose) {
            std::cout << "Finished checking double bond chirality" << std::endl;
        }
        if(skip_outcome) {
            if(verbose) {
                std::cout << "skip_outcome=true, skip to next" << std::endl;
            }
            continue;
        }
        
        // Convert product(s) to single product so that all 
        // reactions can be treated as pseudo-intramolecular
        // But! check for ring openings mistakenly split into multiple
        // This can be diagnosed by duplicate map numbers (i.e., SMILES)
        if(verbose) {
            std::cout << "Convert to single product" << std::endl;
        }
        RDKit::ROMOL_SPTR single_outcome;
        std::vector<int> mapnums;
        mapnums.reserve(32);
        for(const auto m: outcome) {
            for(const auto a: m->atoms()) {
                if(a->getAtomMapNum() != 0)
                    mapnums.push_back(a->getAtomMapNum());
            }
        }
        std::unordered_set<int> mapnums_set;
        mapnums_set.reserve(mapnums.size());
        mapnums_set.insert(mapnums.begin(), mapnums.end());
        if(mapnums.size() != mapnums_set.size()) { // duplicate?
            //if PLEVEL >= 1: print('Found duplicate mapnums in product - need to stitch')
            // need to do a fancy merge
            if(verbose) {
                std::cout << "Found duplicate mapnums in product - need to stitch" << std::endl;
            }
            RDKit::RWMOL_SPTR merged_mol{boost::dynamic_pointer_cast<RDKit::RWMol>(outcome[0])};
            std::unordered_map<int, int> merged_map_to_id;
            merged_map_to_id.reserve(64);
            for(const auto a: outcome[0]->atoms()) {
                const int mapno = a->getAtomMapNum();
                if(mapno != 0)
                    merged_map_to_id[mapno] = a->getIdx();
            }
            for(size_t j = 1; j < outcome.size(); j++) {
                const auto new_mol = outcome[j];
                for(const auto a: new_mol->atoms()) {
                    const int mapno = a->getAtomMapNum();
                    if(!merged_map_to_id.contains(mapno))
                        merged_map_to_id[mapno] = merged_mol->addAtom(a);
                }
                for(const auto b: new_mol->bonds()) {
                    const int bi = b->getBeginAtom()->getAtomMapNum();
                    const int bj = b->getEndAtom()->getAtomMapNum();
                    //if PLEVEL >= 10: print('stitching bond between {} and {} in stich has chirality {}, {}'.format(
                    //    bi, bj, b.GetStereo(), b.GetBondDir()
                    //))
                    if(verbose) {
                        std::cout << "stitching bond between " << bi << " and " << bj << " in stich has chirality, stereo=" << b->getStereo() << ", dir=" << b->getBondDir() << std::endl;
                    }
                    if(!merged_mol->getBondBetweenAtoms(
                            merged_map_to_id[bi], merged_map_to_id[bj])
                    ){
                        merged_mol->addBond(merged_map_to_id[bi],
                            merged_map_to_id[bj], RDKit::Bond::UNSPECIFIED);
                        const int idx = merged_mol->getBondBetweenAtoms(
                            merged_map_to_id[bi], merged_map_to_id[bj]
                        )->getIdx();
                        merged_mol->replaceBond(idx, b);
                        //merged_mol->addBond(merged_map_to_id[bi],
                        //    merged_map_to_id[bj], b->getBondType());
                        //merged_mol->getBondBetweenAtoms(
                        //    merged_map_to_id[bi], merged_map_to_id[bj]
                        //)->setStereo(b->getStereo());
                        //merged_mol->getBondBetweenAtoms(
                        //    merged_map_to_id[bi], merged_map_to_id[bj]
                        //)->setBondDir(b->getBondDir());
                    }
                }
            }
            single_outcome = boost::static_pointer_cast<RDKit::ROMol>(merged_mol);
            //if PLEVEL >= 1: print('Merged editable mol, converted back to real mol, {}'.format(Chem.MolToSmiles(outcome, True)))
        } else {
            single_outcome = outcome[0];
            for(size_t j = 1; j < outcome.size(); j++)
                single_outcome = RDKit::ROMOL_SPTR{RDKit::combineMols(*single_outcome, *outcome[j])};
        }
        //if PLEVEL >= 2: print('Converted all outcomes to single molecules')
        if(verbose) {
            std::cout << "Converted all outcomes to single molecules" << std::endl;
        }
        outcome.clear();
        // use single_outcome instead of outcome after this point
        
        // Figure out which atoms were matched in the templates
        // atoms_rt and atoms_p will be outcome-specific.
        std::unordered_map<int, RDKit::Atom*> atoms_pt;
        atoms_pt.reserve(single_outcome->getNumAtoms());
        for(const auto a: single_outcome->atoms()) {
            if(a->hasProp("old_mapno")) {
                const int old_mapno = a->getProp<int>("old_mapno");
                if(m_atoms_pt_map.contains(old_mapno)) {
                    atoms_pt[a->getAtomMapNum()] = m_atoms_pt_map.at(old_mapno);
                }
            }
        }
        std::unordered_map<int, RDKit::Atom*> atoms_p;
        atoms_p.reserve(single_outcome->getNumAtoms());
        for(const auto a: single_outcome->atoms()) {
            const int mapno = a->getAtomMapNum();
            if(mapno != 0) {
                atoms_p[mapno] = a;
            }
        }
        
        // Set map numbers of product template
        // note: this is okay to do within the loop, because ALL atoms must be matched
        // in the templates, so the map numbers will get overwritten every time
        // This makes it easier to check parity changes
        for(auto [i, a]: atoms_pt) {
            a->setAtomMapNum(i);
        }
        
        // Check for missing bonds. These are bonds that are present in the reactants,
        // not specified in the reactant template, and not in the product. Accidental
        // fragmentation can occur for intramolecular ring openings
        if(verbose) {
            std::cout << "Check for missing bonds" << std::endl;
        }
        std::vector<std::tuple<int, int, RDKit::Bond*>> missing_bonds;
        missing_bonds.reserve(16);
        for(const auto [i, j, b]: rct.m_bonds_by_mapnum) {
            // if i in atoms_p and j in atoms_p:
            if(atoms_p.contains(i) && atoms_p.contains(j)) {
                // atoms from reactant bond show up in product
                if(!single_outcome->getBondBetweenAtoms(atoms_p.at(i)->getIdx(), atoms_p.at(j)->getIdx())) {
                    //...but there is not a bond in the product between those atoms
                    // if i not in atoms_rt or j not in atoms_rt or not template_r.GetBondBetweenAtoms(atoms_rt[i].GetIdx(), atoms_rt[j]->getIdx()):
                    if(!atoms_rt.contains(i) || !atoms_rt.contains(j) || !m_template_r->getBondBetweenAtoms(atoms_rt.at(i)->getIdx(), atoms_rt.at(j)->getIdx())) {
                        // the reactant template did not specify a bond between those atoms (e.g., intentionally destroy)
                        missing_bonds.push_back(std::tuple(i, j, b));
                    }
                }
            }
        }
        if(!missing_bonds.empty()) {
            //if PLEVEL >= 1: print('Product is missing non-reacted bonds that were present in reactants!')
            if(verbose) {
                std::cout << "Product is missing non-reacted bonds that were present in reactants!" << std::endl;
            }
            auto outcome_rw = boost::dynamic_pointer_cast<RDKit::RWMol>(single_outcome);
            std::unordered_map<int, int> rwmol_map_to_id;
            rwmol_map_to_id.reserve(outcome_rw->getNumAtoms());
            for(const auto a: outcome_rw->atoms()) {
                const int mapno = a->getAtomMapNum();
                if(mapno != 0)
                    rwmol_map_to_id[mapno] = a->getIdx();
            }
            for(const auto [i, j, b]: missing_bonds) {
                outcome_rw->addBond(rwmol_map_to_id[i], rwmol_map_to_id[j]);
                auto new_b = outcome_rw->getBondBetweenAtoms(rwmol_map_to_id[i], rwmol_map_to_id[j]);
                new_b->setBondType(b->getBondType());
                new_b->setBondDir(b->getBondDir());
                new_b->setIsAromatic(b->getIsAromatic());
            }
            single_outcome = boost::static_pointer_cast<RDKit::ROMol>(outcome_rw);
            for(const auto a: single_outcome->atoms()) {
                const int mapno = a->getAtomMapNum();
                if(mapno != 0)
                    atoms_p[mapno] = a;
            }
        } else {
            //if PLEVEL >= 3: print('No missing bonds')
            if(verbose) {
                std::cout << "No missing bonds" << std::endl;
            }
        }
        
        // Now that we've fixed any bonds, connectivity is set. This is a good time
        // to udpate the property cache, since all that is left is fixing atom/bond
        // stereochemistry.
        if(verbose) {
            std::cout << "RDKit::MolOps::sanitizeMol and updatePropertyCache" << std::endl;
        }
        try {
            RDKit::MolOps::sanitizeMol(*boost::dynamic_pointer_cast<RDKit::RWMol>(single_outcome));
            single_outcome->updatePropertyCache();
        } catch([[maybe_unused]] std::exception& e) {
            //if PLEVEL >= 1: print('{}, {}'.format(Chem.MolToSmiles(outcome, True), e))
            if(verbose) {
                std::cout << "RDKit::MolOps::sanitizeMol failed" << std::endl;
            }
            continue;
        }
        
        // Correct tetra chirality in the outcome
        if(verbose) {
            std::cout << "Correct tetra chirality in the outcome" << std::endl;
        }
        std::vector<RDKit::Atom*> tetra_copied_from_reactants;
        tetra_copied_from_reactants.reserve(single_outcome->getNumAtoms());
        for(auto a: single_outcome->atoms()) {
            // Participants in reaction core (from reactants) will have old_mapno
            // Spectators present in reactants will have react_atom_idx
            // ...so new atoms will have neither!
            if(!a->hasProp("old_mapno")) {
                // Not part of the reactants template
                
                if(!a->hasProp("react_atom_idx")) {
                    // Atoms only appear in product template - their chirality
                    // should be properly instantiated by RDKit...hopefully...
                    //if PLEVEL >= 4: print('Atom {} created by product template, should have right chirality'.format(a.GetAtomMapNum()))
                    if(verbose) {
                        std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") created by product template, should have right chirality" << std::endl;
                    }
                } else {
                    //if PLEVEL >= 4: print('Atom {} outside of template, copy chirality from reactants'.format(a.GetAtomMapNum()))
                    if(verbose) {
                        std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") outside of template, copy chirality from reactants" << std::endl;
                    }
                    copy_chirality(rct.m_atoms_r.at(a->getAtomMapNum()), a, verbose);
                    if(a->getChiralTag() != RDKit::Atom::CHI_UNSPECIFIED)
                        tetra_copied_from_reactants.push_back(a);
                }
            } else {
                // Part of reactants and reaction core
                
                if(template_atom_could_have_been_tetra(atoms_rt.at(a->getAtomMapNum()))) {
                    //if PLEVEL >= 3: print('Atom {} was in rct template (could have been tetra)'.format(a.GetAtomMapNum()))
                    if(verbose) {
                        std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") was in rct template (could have been tetra)" << std::endl;
                    }
                    
                    if(template_atom_could_have_been_tetra(atoms_pt.at(a->getAtomMapNum()))) {
                        //if PLEVEL >= 3: print('Atom {} in product template could have been tetra, too'.format(a.GetAtomMapNum()))
                        if(verbose) {
                            std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") in product template could have been tetra, too" << std::endl;
                        }
                        
                        // Was the product template specified?
                        
                        if(atoms_pt.at(a->getAtomMapNum())->getChiralTag() == RDKit::Atom::CHI_UNSPECIFIED) {
                            // No, leave unspecified in product
                            //if PLEVEL >= 3: print('...but it is not specified in product, so destroy chirality')
                            if(verbose) {
                                std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") ...but it is not specified in product, so destroy chirality" << std::endl;
                            }
                            a->setChiralTag(RDKit::Atom::CHI_UNSPECIFIED);
                        } else {
                            // Yes
                            //if PLEVEL >= 3: print('...and product is specified')
                            if(verbose) {
                                std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") ...and product is specified" << std::endl;
                            }
                            
                            // Was the reactant template specified?
                            
                            if(atoms_rt.at(a->getAtomMapNum())->getChiralTag() == RDKit::Atom::CHI_UNSPECIFIED) {
                                // No, so the reaction introduced chirality
                                //if PLEVEL >= 3: print('...but reactant template was not, so copy from product template')
                                if(verbose) {
                                    std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") ...but reactant template was not, so copy from product template" << std::endl;
                                }
                                copy_chirality(atoms_pt.at(a->getAtomMapNum()), a, verbose);
                            } else {
                                // Yes, so we need to check if chirality should be preserved or inverted
                                //if PLEVEL >= 3: print('...and reactant template was, too! copy from reactants')
                                if(verbose) {
                                    std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") ...and reactant template was, too! copy from reactants" << std::endl;
                                }
                                copy_chirality(rct.m_atoms_r.at(a->getAtomMapNum()), a, verbose);
                                if(atom_chirality_matches(atoms_pt.at(a->getAtomMapNum()), atoms_rt.at(a->getAtomMapNum()), verbose) == -1) {
                                    //if PLEVEL >= 3: print('but! reactant template and product template have opposite stereochem, so invert')
                                    if(verbose) {
                                        std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") ...but! reactant template and product template have opposite stereochem, so invert" << std::endl;
                                    }
                                    a->invertChirality();
                                }
                            }
                        }
                    }else {
                        // Reactant template chiral, product template not - the
                        // reaction is supposed to destroy chirality, so leave
                        // unspecified
                        //if PLEVEL >= 3: print('If reactant template could have been ' +
                        //    'chiral, but the product template could not, then we dont need ' +
                        //    'to worry about specifying product atom chirality')
                        if(verbose) {
                            std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") If reactant template could have been chiral, but the product template could not, then we don't need to worry about specifying product atom chirality" << std::endl;
                        }
                    }
                } else {
                    //if PLEVEL >= 3: print('Atom {} could not have been chiral in reactant template'.format(a.GetAtomMapNum()))
                    if(verbose) {
                        std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") could not have been chiral in reactant template" << std::endl;
                    }
                    
                    if(!template_atom_could_have_been_tetra(atoms_pt.at(a->getAtomMapNum()))) {
                        //if PLEVEL >= 3: print('Atom {} also could not have been chiral in product template', a.GetAtomMapNum())
                        //if PLEVEL >= 3: print('...so, copy chirality from reactant instead')
                        if(verbose) {
                            std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") also could not have been chiral in product template  ...so, copy chirality from reactant instead" << std::endl;
                        }
                        copy_chirality(rct.m_atoms_r.at(a->getAtomMapNum()), a, verbose);
                        if(a->getChiralTag() != RDKit::Atom::CHI_UNSPECIFIED)
                            tetra_copied_from_reactants.push_back(a);
                    } else {
                        //if PLEVEL >= 3: print('Atom could/does have product template chirality!'.format(a.GetAtomMapNum()))
                        //if PLEVEL >= 3: print('...so, copy chirality from product template')
                        if(verbose) {
                            std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") could/does have product template chirality! ...so, copy chirality from product template" << std::endl;
                        }
                        copy_chirality(atoms_pt.at(a->getAtomMapNum()), a, verbose);
                    }
                }
            }
            //if PLEVEL >= 3: print('New chiral tag {}'.format(a.GetChiralTag()))
            if(verbose) {
                std::cout << "Atom (mapno=" << a->getAtomMapNum() << ") New chiral tag " << a->getChiralTag() << std::endl;
            }
        }
        if(skip_outcome) {
            //if PLEVEL >= 2: print('Skipping this outcome - chirality broken?')
            if(verbose) {
                std::cout << "Skipping this outcome - chirality broken?" << std::endl;
            }
            continue;
        }
        //if PLEVEL >= 2: print('After attempting to re-introduce chirality, outcome = {}'.format(Chem.MolToSmiles(outcome, True)))
        if(verbose) {
            std::cout << "After attempting to re-introduce chirality, outcome = " << RDKit::MolToSmiles(*single_outcome) << std::endl;
        }
        
        // Correct bond directionality in the outcome
        if(verbose) {
            std::cout << "Correct bond directionality in the outcome" << std::endl;
        }
        for(const auto b: single_outcome->bonds()) {
            if(b->getBondType() != RDKit::Bond::DOUBLE)
                continue;

            // Ring double bonds do not need to be touched(?)
            if(BondIsInRing(b))
                continue;
            
            const auto ba = b->getBeginAtom();
            const auto bb = b->getEndAtom();

            // Is it possible at all to specify this bond?
            if(ba->getDegree() == 1 || bb->getDegree() == 1)
                continue;

            //if PLEVEL >= 5: print('Looking at outcome bond {}={}'.format(ba.GetAtomMapNum(), bb.GetAtomMapNum()))
            if(verbose) {
                std::cout << "Looking at outcome bond " << ba->getAtomMapNum() << "=" << bb->getAtomMapNum() << std::endl;
            }

            if(ba->hasProp("old_mapno") && bb->hasProp("old_mapno")) {
                // Need to rely on templates for bond chirality, both atoms were
                // in the reactant template 
                //if PLEVEL >= 5: print('Both atoms in this double bond were in the reactant template')
                if(verbose) {
                    std::cout << "Both atoms in this double bond were in the reactant template" << std::endl;
                }
                if( m_required_bond_defs_coreatoms.contains(
                        std::pair(ba->getProp<int>("old_mapno"), bb->getProp<int>("old_mapno"))
                    )
                ) {
                    //if PLEVEL >= 5: print('and reactant template *could* have specified the chirality!')
                    //if PLEVEL >= 5: print('..product should be property instantiated')
                    if(verbose) {
                        std::cout << "and reactant template *could* have specified the chirality!" << std::endl;
                        std::cout << "...product should be property instantiated" << std::endl;
                    }
                    continue;
                }
                //if PLEVEL >= 5: print('But it was impossible to have specified chirality (e.g., aux C=C for context)')
                if(verbose) {
                    std::cout << "But it was impossible to have specified chirality (e.g., aux C=C for context)" << std::endl;
                }
            } else if(! ba->hasProp("react_atom_idx") && ! bb->hasProp("react_atom_idx")) {
                // The atoms were both created by the product template, so any bond
                // stereochemistry should have been instantiated by the product template
                // already...hopefully...otherwise it isn't specific enough?
                if(verbose) {
                    std::cout << "The atoms were both created by the product template" << std::endl;
                }
                continue;
            }
            // Need to copy from reactants, this double bond was simply carried over,
            // *although* one of the atoms could have reacted and been an auxilliary
            // atom in the reaction, e.g., C/C=C(/CO)>>C/C=C(/C[Br])
            //if PLEVEL >= 5: print('Restoring cis/trans character of bond {}={} from reactants'.format(
            //    ba.GetAtomMapNum(), bb.GetAtomMapNum()))
            if(verbose) {
                std::cout << "Restoring cis/trans character of bond " << ba->getAtomMapNum() << "=" << bb->getAtomMapNum() << " from reactants" << std::endl;
            }
            
            // Start with setting the BeginAtom
            const auto begin_atom_specified = restore_bond_stereo_to_sp2_atom(ba, rct.m_bond_dirs_by_mapnum, verbose);
            
            if(!begin_atom_specified) {
                // don't bother setting other side of bond, since we won't be able to
                // fully specify this bond as cis/trans
                continue;
            }
            // Look at other side of the bond now, the EndAtom
            const auto end_atom_specified = restore_bond_stereo_to_sp2_atom(bb, rct.m_bond_dirs_by_mapnum, verbose);
            /*
            if not end_atom_specified:
                // note: this can happen if C=C/C-N turns into C=C/C=N 
                if PLEVEL >= 1:
                    print(reactants.bond_dirs_by_mapnum)
                    print(ba.GetAtomMapNum())
                    print(bb.GetAtomMapNum())
                    print(Chem.MolToSmiles(reactants.reactants, True))
                    print(Chem.MolToSmiles(outcome, True))
                    print('Uh oh, looks like bond direction is only specified for half of this bond?')
            */
            if(verbose) {
                if(!end_atom_specified) {
                    std::cout << ba->getAtomMapNum() << "=" << bb->getAtomMapNum() << std::endl;
                    std::cout << "Uh oh, looks like bond direction is only specified for half of this bond?" << std::endl;
                }
            }
        }
        
        //Keep track of the reacting atoms for later use in grouping
        std::unordered_map<int, bool> atoms_diff;
        atoms_diff.reserve(atoms_rt.size());
        for(const auto [k, v]: atoms_rt) {
            atoms_diff[k] = atoms_are_different(rct.m_atoms_r.at(k), atoms_p.at(k));
        }
        //make tuple of changed atoms
        std::vector<int> atoms_changed;
        atoms_changed.reserve(16);
        for(const auto [k, v]: atoms_diff) {
            if(v) atoms_changed.push_back(k);
        }
        auto mapped_outcome = RDKit::MolToSmiles(*single_outcome, true); // non-const for move

        if(!keep_mapnums) {
            for(auto a: single_outcome->atoms())
                a->setAtomMapNum(0);
        }

        // Now, check to see if we have destroyed chirality 
        // this occurs when chirality was not actually possible (e.g., due to
        // symmetry) but we had assigned a tetrahedral center originating
        // from the reactants.
        //    ex: SMILES C(=O)1C[C@H](Cl)CCC1
        //        SMARTS [C:1]-[C;H0;D3;+0:2](-[C:3])=[O;H0;D1;+0]>>[C:1]-[CH2;D2;+0:2]-[C:3]
        skip_outcome = false;
        if(tetra_copied_from_reactants.size() > 0) {
            RDKit::MolOps::assignStereochemistry(*single_outcome, true, true, false);
            for(const auto a: tetra_copied_from_reactants) {
                if(a->getChiralTag() == RDKit::Atom::CHI_UNSPECIFIED) {
                    //if PLEVEL >= 2: print('Auxiliary reactant atom was chiral, now is broken -> skip outcome')
                    if(verbose) {
                        std::cout << "Auxiliary reactant atom was chiral, now is broken -> skip outcome" << std::endl;
                    }
                    skip_outcome = true;
                    break;
                }
            }
        }
        if(skip_outcome) {
            continue;
        }
        if(verbose) {
            std::cout << "After check to see if we have destroyed chirality " << std::endl;
        }

        const auto smiles = RDKit::MolToSmiles(*single_outcome, true);
        auto smiles_new = canonicalize_outcome_smiles(smiles); // non-const for move
        if(smiles_new.empty()) continue;
        
        if(verbose) {
            std::cout << "SMILES: " << smiles_new << std::endl;
            std::cout << "SMILES (mapped): " << mapped_outcome << std::endl;
        }

        final_outcomes.insert(smiles_new);
        mapped_outcomes.emplace(std::move(smiles_new), std::pair(std::move(mapped_outcome), std::move(atoms_changed))); // must be the last line due to std::move
    }
    
    if(combine_enantiomers) {
        final_outcomes = combine_enantiomers_into_racemic(final_outcomes);
    }
    
    if(verbose) {
        std::cout << "===================END==========================" << std::endl;
    }
    
    return std::pair(std::vector<std::string>(final_outcomes.begin(), final_outcomes.end()), std::move(mapped_outcomes));
}


std::pair<
    std::vector<std::string>,
    std::unordered_map<std::string, std::pair<std::string, std::vector<int>>>
>
Reaction::run(const std::string& reaction_smarts, const std::string& reactants_smiles, const bool keep_mapnums, const bool combine_enantiomers, const bool relax_chiral_match, const bool verbose) {
    Reactants _reactants{reactants_smiles};
    Reaction _reaction{reaction_smarts};
    return _reaction.run(_reactants, keep_mapnums, combine_enantiomers, relax_chiral_match, verbose);
}

void Reaction::reset(void) {
    for(auto [idx, mapnum]: m_atoms_rt_idx_to_map) {
        m_template_r->getAtomWithIdx(idx)->setAtomMapNum(mapnum);
    }
    for(auto [idx, mapnum]: m_atoms_pt_idx_to_map) {
        m_template_p->getAtomWithIdx(idx)->setAtomMapNum(mapnum);
    }
}

std::string Reaction::smarts(void) const {
    return m_reaction_smarts;
}

}
