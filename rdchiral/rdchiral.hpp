#ifndef RDCHIRAL_RDCHIRAL_HEADER
#define RDCHIRAL_RDCHIRAL_HEADER

#include <string>
#include <utility>
#include <tuple>
#include <vector>
#include <unordered_map>
#include <memory>

#include <GraphMol/GraphMol.h>
#include <GraphMol/ChemReactions/Reaction.h>

#include "rdchiral/bonds.hpp"
#include "rdchiral/util.hpp"
#include "rdchiral/template_extractor.hpp"

namespace rdchiral {

class Reactants;

// rdchiralReaction
class Reaction {
protected:
    std::string m_reaction_smarts;
    std::shared_ptr<RDKit::ChemicalReaction> m_rxn;
    RDKit::ROMOL_SPTR m_template_r, m_template_p;
    std::unordered_map<int, RDKit::Atom*> m_atoms_rt_map, m_atoms_pt_map;
    std::unordered_map<int, int> m_atoms_rt_idx_to_map, m_atoms_pt_idx_to_map;
    //std::unordered_map<
    //    std::pair<int, int>,
    //    RDKit::Bond::BondDir,
    //    _hash_pair
    //> m_rt_bond_dirs_by_mapnum, m_pt_bond_dirs_by_mapnum;
    std::unordered_map<
        std::tuple<int, int, int, int>,
        std::pair<RDKit::Bond::BondDir, RDKit::Bond::BondDir>,
        _hash_tuple<int, int, int, int>
    > m_required_rt_bond_defs;
    std::unordered_set<std::pair<int, int>, _hash_pair> m_required_bond_defs_coreatoms;

public:
    explicit Reaction(const std::string& reaction_smarts);
    Reaction()=delete;
    
    ~Reaction()=default;
    
    // relax_chiral_match: allow chiral-achiral matches
    std::pair<
        std::vector<std::string>,
        std::unordered_map<std::string, std::pair<std::string, std::vector<int>>>
    > run(Reactants& rct, const bool keep_mapnums=false, const bool combine_enantiomers=true, const bool relax_chiral_match=false, const bool verbose=false);
    
    static
    std::pair<
        std::vector<std::string>,
        std::unordered_map<std::string, std::pair<std::string, std::vector<int>>>
    > run(const std::string& reaction_smarts, const std::string& reactants_smiles, const bool keep_mapnums=false, const bool combine_enantiomers=true, const bool relax_chiral_match=false, const bool verbose=false);
    
    void reset(void);
    
    std::string smarts(void) const;
};

// rdchiralReactants
class Reactants {
protected:
    std::string m_reactant_smiles; // input SMILES
    RDKit::RWMOL_SPTR m_reactants;
    RDKit::RWMOL_SPTR m_reactants_achiral; // must be declared after m_reactants
    std::unordered_map<int, RDKit::Atom*> m_atoms_r;
    std::vector<std::tuple<int, int, RDKit::Bond*>> m_bonds_by_mapnum;
    std::unordered_map<std::pair<int, int>, RDKit::Bond::BondDir, _hash_pair> m_bond_dirs_by_mapnum;
    std::vector<DoubleBond> m_atoms_across_double_bonds;
    
public:
    explicit Reactants(const std::string& reactant_smiles);
    Reactants()=delete;
    
    ~Reactants()=default;
    
    int idx_to_mapnum(const int idx) const { return m_reactants->getAtomWithIdx(idx)->getAtomMapNum(); }
    
    std::string smiles(void) const;
    
    friend class Reaction;
};

}

#endif // RDCHIRAL_RDCHIRAL_HEADER
