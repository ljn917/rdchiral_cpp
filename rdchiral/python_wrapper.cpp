#include <boost/python.hpp>
#include <rdchiral/rdchiral.hpp>
#include <rdchiral/smarts_util.hpp>

namespace rdchiral {

template<typename T>
boost::python::list convert_to_python_list(const std::vector<T>& cpp_vec_T) {
    boost::python::list py_list;
    for(auto i: cpp_vec_T) {
        py_list.append(i);
    }
    return py_list;
}

boost::python::object convert_rxn_return_type(
    const std::pair<
        std::vector<std::string>,
        std::unordered_map<std::string, std::pair<std::string, std::vector<int>>>
    >& cpp_var,
    const bool return_mapped
) {
    auto py_tuple_1 = convert_to_python_list(cpp_var.first);
    
    if(!return_mapped) {
        return py_tuple_1;
    }
    
    auto py_tuple_2 = boost::python::dict();
    for(auto [k, v]: cpp_var.second) {
        auto py_v = boost::python::make_tuple(v.first, convert_to_python_list(v.second));
        py_tuple_2[k] = py_v;
    }
    return boost::python::make_tuple(py_tuple_1, py_tuple_2);
}

boost::python::object
rdchiralRunText(
    const std::string reaction_smarts,
    const std::string reactant_smiles,
    const bool keep_mapnums=false,
    const bool combine_enantiomers=true,
    const bool return_mapped=false,
    const bool relax_chiral_match=false,
    const bool verbose=false
) {
    return convert_rxn_return_type(Reaction::run(reaction_smarts, reactant_smiles, keep_mapnums, combine_enantiomers, relax_chiral_match, verbose), return_mapped);
}


boost::python::object
rdchiralRun(
    Reaction& rxn,
    Reactants& reactants,
    const bool keep_mapnums=false,
    const bool combine_enantiomers=true,
    const bool return_mapped=false,
    const bool relax_chiral_match=false,
    const bool verbose=false
) {
    return convert_rxn_return_type(rxn.run(reactants, keep_mapnums, combine_enantiomers, relax_chiral_match, verbose), return_mapped);
}

boost::python::dict
extract_from_reaction_python(
    boost::python::dict reaction,
    std::string reactants,
    std::string products,
    bool verbose = false,
    bool use_stereochemistry = true,
    unsigned maximum_number_unmapped_product_atoms = 5,
    bool include_all_unmapped_reactant_atoms = true,
    int radius = 1,
    bool no_special_groups = false,
    bool do_canonicalize = true
) {
    if(reaction.has_key("reactants")) {
        if(!reactants.empty()) throw std::runtime_error{"extract_from_reaction: only reaction['reactants'] or reactants can be set"};
        reactants = boost::python::extract<std::string>(reaction["reactants"]);
    }
    if(reaction.has_key("products")) {
        if(!products.empty()) throw std::runtime_error{"extract_from_reaction: only reaction['products'] or products can be set"};
        products  = boost::python::extract<std::string>(reaction["products"]);
    }
    
    if(reactants.empty() || products.empty()) throw std::runtime_error{"extract_from_reaction: both reactants and products must be set"};
    
    if(radius < 0 || radius > 10) throw std::runtime_error{"extract_from_reaction: radius="+std::to_string(radius)+" is out of normal range 0-10."};
    
    bool use_custom_special_groups{false};
    if(no_special_groups) {
        use_custom_special_groups = true; // special_groups = {}
    }
    
    auto results = extract_from_reaction(
        reactants,
        products,
        verbose,
        use_stereochemistry,
        maximum_number_unmapped_product_atoms,
        include_all_unmapped_reactant_atoms,
        radius,
        use_custom_special_groups,
        {},
        do_canonicalize
    );
    
    boost::python::dict results_python;
    results_python["reactants_smarts"]          = results.reactants_smarts;
    results_python["products_smarts"]           = results.products_smarts;
    results_python["reaction_smarts_forward"]   = results.reaction_smarts_forward;
    results_python["reaction_smarts_retro"]     = results.reaction_smarts_retro;
    results_python["intra_only"]                = results.intra_only;
    results_python["dimer_only"]                = results.dimer_only;
    results_python["necessary_reagent"]         = results.necessary_reagent;
    // backward compat with python
    results_python["reaction_smarts"]           = results.reaction_smarts_retro;
    results_python["reactants"]                 = results.reactants_smarts;
    results_python["products"]                  = results.products_smarts;
    
    if(reaction.has_key("_id")) {
        results_python["reaction_id"] = reaction["_id"];
        results_python["_id"]         = reaction["_id"];
    }
    
    return results_python;
}

const auto reactants_class_docstr =
    "Class to store everything that should be pre-computed "
    "for a reactant mol so that library application is faster\n"
    "Inputs: reactants_smiles\n";

const auto reaction_class_docstr =
    "Class to store everything that should be pre-computed "
    "for a reaction. This makes library application much faster, since we can pre-do a lot of work "
    "instead of doing it for every mol-template pair.\n"
    "Inputs: reaction_smarts\n";

const auto rdchiralRunText_function_docstr =
    "Run from SMARTS string and SMILES string. This is NOT recommended"
    "for library application, since initialization is pretty slow. You should"
    "separately initialize the template and molecules and call rdchiralRun()\n"
    "\n"
    "Args:\n"
    "    reaction_smarts (str): Reaction SMARTS string\n"
    "    reactant_smiles (str): Reactant SMILES string\n"
    "    **kwargs: passed through to rdchiralRun\n"
    "\n"
    "Returns:\n"
    "    (list, str (optional)): Returns list of outcomes. If `return_mapped` is True,"
    "additionally return atom mapped SMILES strings\n";

const auto rdchiralRun_function_docstr =
    "Run rdchiral reaction\n"
    "NOTE: there is a fair amount of initialization (assigning stereochem), most"
    "importantly assigning atom map numbers to the reactant atoms. It is "
    "HIGHLY recommended to use the custom classes for initialization.\n"
    "\n"
    "Args:\n"
    "    rxn (rdchiralReaction): (rdkit reaction + auxilliary information)\n"
    "    reactants (rdchiralReactants): (rdkit mol + auxilliary information)\n"
    "    keep_mapnums (bool): Whether to keep map numbers or not\n"
    "    combine_enantiomers (bool): Whether to combine enantiomers\n"
    "    return_mapped (bool): Whether to additionally return atom mapped SMILES strings\n"
    "    relax_chiral_match (bool): Allow chiral-achiral match\n"
    "\n"
    "Returns:\n"
    "    (list, str (optional)): Returns list of outcomes. If `return_mapped` is True,"
    "additionally return atom mapped SMILES strings\n";

const auto extract_from_reaction_function_docstr =
    "Extract template SMARTS from a reaction\n"
    "\n"
    "Args:\n"
    "    reaction (dict): keys: reactants and products\n"
    "    reactants_smiles (str): reactants SMILES\n"
    "    products_smiles (str): products SMILES\n"
    "    verbose (bool): Show debug information\n"
    "    use_stereochemistry (bool): Whether to include chirality in the template\n"
    "    maximum_number_unmapped_product_atoms: Allow only maximum number of unmapped atoms in products\n"
    "    include_all_unmapped_reactant_atoms (bool): Include all unmapped reactant atoms\n"
    "    radius (int): Include atoms around reaction center in a radius\n"
    "    no_special_groups (bool): Do not use special groups if true\n"
    "    do_canonicalize (bool): Whether to canonicalize the template\n"
    "\n"
    "Returns:\n"
    "    (dict): Returns dict with keys: reactants_smarts, products_smarts,\n"
    "            reaction_smarts_forward, reaction_smarts_retro,\n"
    "            intra_only, dimer_only, necessary_reagent\n";

const auto canonicalize_transform_function_docstr =
    "Canonicalize a template\n"
    "\n"
    "Args:\n"
    "    transform (str): The template string\n"
    "    chirality_inversion_equivalent (bool): Treat R->S and S->R as the same template\n"
    "\n"
    "Returns:\n"
    "    (str): Return the canonical template string\n";

boost::python::object create_submodule(
    std::string submodule_name
) {
    boost::python::scope current_scope;
    
    // get the name of current module
    std::string current_module_name{
        boost::python::extract<const char*>(current_scope.attr("__name__"))
    };
    // from current_module_name import current_module_name.submodule_name
    std::string new_module_fullname = current_module_name+"."+submodule_name;
    boost::python::object new_module(boost::python::handle<>(boost::python::borrowed(
        PyImport_AddModule(new_module_fullname.c_str())
    )));
    // from import submodule_name
    current_scope.attr(submodule_name.c_str()) = new_module;
    
    return new_module;
}

boost::python::object reactants_class(void) {
    return boost::python::class_<Reactants>(
        "rdchiralReactants",
        reactants_class_docstr,
        boost::python::init<std::string>("Input argument (str): Reactants SMILES")
    ).def(
        "smiles",
        &Reactants::smiles,
        (
            boost::python::arg("self")
        ),
        "SMILES with atom mapping"
    );
}

boost::python::object reaction_class(void) {
    return boost::python::class_<Reaction>(
        "rdchiralReaction",
        reaction_class_docstr,
        boost::python::init<std::string>("Input argument (str): Reaction SMARTS")
    ).def(
        "run",
        rdchiralRun,
        (
            boost::python::arg("self"),
            boost::python::arg("reactants"),
            boost::python::arg("keep_mapnums") = false,
            boost::python::arg("combine_enantiomers") = true,
            boost::python::arg("return_mapped") = false,
            boost::python::arg("relax_chiral_match") = false,
            boost::python::arg("verbose") = false
        ),
        rdchiralRun_function_docstr
    ).def(
        "smarts",
        &Reaction::smarts,
        (
            boost::python::arg("self")
        ),
        "SMARTS string"
    );
}

void rdchiralRunText_function(void) {
    boost::python::def(
        "rdchiralRunText",
        rdchiralRunText,
        (
            boost::python::arg("reaction_smarts"),
            boost::python::arg("reactant_smiles"),
            boost::python::arg("keep_mapnums") = false,
            boost::python::arg("combine_enantiomers") = true,
            boost::python::arg("return_mapped") = false,
            boost::python::arg("relax_chiral_match") = false,
            boost::python::arg("verbose") = false
        )
    );
}

void rdchiralRun_function(void) {
    boost::python::def(
        "rdchiralRun",
        rdchiralRun,
        (
            boost::python::arg("rxn"),
            boost::python::arg("reactants"),
            boost::python::arg("keep_mapnums") = false,
            boost::python::arg("combine_enantiomers") = true,
            boost::python::arg("return_mapped") = false,
            boost::python::arg("relax_chiral_match") = false,
            boost::python::arg("verbose") = false
        ),
        rdchiralRun_function_docstr
    );
}

void template_extractor_functions(void) {
    boost::python::def(
        "extract_from_reaction",
        extract_from_reaction_python,
        (
            boost::python::arg("reaction") = boost::python::dict(),
            boost::python::arg("reactants") = "",
            boost::python::arg("products") = "",
            boost::python::arg("verbose") = false,
            boost::python::arg("use_stereochemistry") = true,
            boost::python::arg("maximum_number_unmapped_product_atoms") = 5,
            boost::python::arg("include_all_unmapped_reactant_atoms") = true,
            boost::python::arg("radius") = 1,
            boost::python::arg("no_special_groups") = false,
            boost::python::arg("do_canonicalize") = true
        ),
        extract_from_reaction_function_docstr
    );
    
    boost::python::def(
        "reassign_atom_mapping",
        reassign_atom_mapping,
        (
            boost::python::arg("transform")
        )
    );
    
    boost::python::def(
        "canonicalize_transform",
        canonicalize_transform,
        (
            boost::python::arg("transform"),
            boost::python::arg("chirality_inversion_equivalent") = true
        ),
        canonicalize_transform_function_docstr
    );
    
    boost::python::def(
        "canonicalize_smarts",
        canonicalize_smarts,
        (
            boost::python::arg("smarts")
        )
    );
    
    boost::python::def(
        "canonicalize_smarts_atom",
        canonicalize_smarts_atom,
        (
            boost::python::arg("transform"),
            boost::python::arg("output_degree") = true,
            boost::python::arg("invert_all_chiral_centers") = false,
            boost::python::arg("output_hydrogen") = true
        )
    );
}

BOOST_PYTHON_MODULE(rdchiral) {
    auto reactants_class_obj = reactants_class();
    auto reaction_class_obj = reaction_class();
    rdchiralRunText_function();
    rdchiralRun_function();
    
    template_extractor_functions();
    
    {
        boost::python::scope new_module{create_submodule("main")};
        
        rdchiralRunText_function();
        rdchiralRun_function();
        
        new_module.attr("rdchiralReactants") = reactants_class_obj;
        new_module.attr("rdchiralReaction") = reaction_class_obj;
    }
    
    {
        boost::python::scope new_module{create_submodule("initialization")};
        
        new_module.attr("rdchiralReactants") = reactants_class_obj;
        new_module.attr("rdchiralReaction") = reaction_class_obj;
    }
    
    {
        boost::python::scope new_module{create_submodule("template_extractor")};
        
        template_extractor_functions();
    }
}

}
