#ifndef RDCHIRAL_CLEAN_HEADER
#define RDCHIRAL_CLEAN_HEADER

#include <string>
#include <vector>

#include <GraphMol/GraphMol.h>

#include "rdchiral/util.hpp"

namespace rdchiral {

std::string canonicalize_outcome_smiles(const std::string& smiles, const bool ensure=true);

std::unordered_set<std::string> combine_enantiomers_into_racemic(const std::unordered_set<std::string>& final_outcomes);

}

#endif // RDCHIRAL_CLEAN_HEADER
