#include "rdchiral/bonds.hpp"
#include "rdchiral/rdkit_helper.hpp"
#include <GraphMol/MolOps.h>
#include <GraphMol/SmilesParse/SmilesWrite.h>

namespace rdchiral {

RDKit::Bond::BondDir BondDirOpposite(const RDKit::Bond::BondDir dir) {
    if(dir == RDKit::Bond::ENDUPRIGHT) {
        return RDKit::Bond::ENDDOWNRIGHT;
    } else if(dir == RDKit::Bond::ENDDOWNRIGHT) {
        return RDKit::Bond::ENDUPRIGHT;
    } else if(dir == RDKit::Bond::NONE) {
        return RDKit::Bond::NONE;
    } else {
        throw std::logic_error{"BondDirOpposite: BondDir not allowed."};
    }
}

std::string BondDirLabel(const RDKit::Bond::BondDir dir) {
    if(dir == RDKit::Bond::ENDUPRIGHT) {
        return "\\";
    } else if(dir == RDKit::Bond::ENDDOWNRIGHT) {
        return "/";
    } else {
        throw std::logic_error{"BondDirLabel: BondDir not allowed."};
    }
}

std::vector<DoubleBond> get_atoms_across_double_bonds(const RDKit::RWMol& mol) {
    std::vector<DoubleBond> res;
    res.reserve(32);
    
    if(!mol.getRingInfo()->isInitialized()) {
        RDKit::MolOps::findSSSR(mol);
    }
    const auto atomrings = mol.getRingInfo()->atomRings(); // vector<vector<int>>
    
    for(const auto b: mol.bonds()) {
        if(b->getBondType() != RDKit::Bond::DOUBLE)
            continue;
        
        const auto ba = b->getBeginAtom();
        const auto bb = b->getEndAtom();
        
        if(ba->getDegree() == 1 || bb->getDegree() == 1)
            continue;
        
        const int ba_label = ba->getAtomMapNum();
        const int bb_label = bb->getAtomMapNum();
        
        DoubleBond double_bond;
        const RDKit::Bond *bab{nullptr}, *bbb{nullptr};
        bool is_front_set{false}, is_back_set{false};
        for(const auto i: AtomGetBonds(ba)) {
            if(i->getBondType() != RDKit::Bond::DOUBLE) {
                bab = i;
                if(bab->getBondDir() != RDKit::Bond::NONE) {
                    is_front_set = true;
                    double_bond.mapnums[0] = bab->getBeginAtom()->getAtomMapNum();
                    double_bond.mapnums[1] = bab->getEndAtom()->getAtomMapNum();
                    double_bond.dirs.first = bab->getBondDir();
                    break;
                }
            }
        }
        for(const auto i: AtomGetBonds(bb)) {
            if(i->getBondType() != RDKit::Bond::DOUBLE) {
                bbb = i;
                if(bbb->getBondDir() != RDKit::Bond::NONE) {
                    is_back_set = true;
                    double_bond.mapnums[2] = bbb->getBeginAtom()->getAtomMapNum();
                    double_bond.mapnums[3] = bbb->getEndAtom()->getAtomMapNum();
                    double_bond.dirs.second = bbb->getBondDir();
                    break;
                }
            }
        }
        if(bab == nullptr || bbb == nullptr)
            continue;
        
        if(is_front_set == false || is_back_set == false) {
            if(BondIsInRing(b)) {
                for(const auto& atomring: atomrings) {
                    bool inring_ba = std::find(atomring.begin(), atomring.end(), ba->getIdx()) != atomring.end();
                    bool inring_bb = std::find(atomring.begin(), atomring.end(), bb->getIdx()) != atomring.end();
                    if(inring_ba && inring_bb) {
                        double_bond.mapnums[0] = bab->getOtherAtom(ba)->getAtomMapNum();
                        double_bond.mapnums[1] = ba_label;
                        double_bond.mapnums[2] = bb_label;
                        double_bond.mapnums[3] = bbb->getOtherAtom(bb)->getAtomMapNum();
                        const bool inring_a = std::find(atomring.begin(), atomring.end(), bab->getOtherAtomIdx(ba->getIdx())) != atomring.end();
                        const bool inring_b = std::find(atomring.begin(), atomring.end(), bbb->getOtherAtomIdx(bb->getIdx())) != atomring.end();
                        if(inring_a != inring_b) {
                            // one of these atoms are in the ring, one is outside -> trans
                            // if PLEVEL >= 10: print('Implicit trans found')
                            double_bond.dirs.first = RDKit::Bond::ENDUPRIGHT;
                            double_bond.dirs.second = RDKit::Bond::ENDUPRIGHT;
                        } else {
                            // if PLEVEL >= 10: print('Implicit cis found')
                            double_bond.dirs.first = RDKit::Bond::ENDUPRIGHT;
                            double_bond.dirs.second = RDKit::Bond::ENDDOWNRIGHT;
                        }
                        double_bond.is_implicit = true;
                        break;
                    }
                }
            } else {
                double_bond.mapnums[0] = bab->getBeginAtom()->getAtomMapNum();
                double_bond.mapnums[1] = bab->getEndAtom()->getAtomMapNum();
                double_bond.dirs.first = RDKit::Bond::NONE;
                double_bond.mapnums[2] = bbb->getBeginAtom()->getAtomMapNum();
                double_bond.mapnums[3] = bbb->getEndAtom()->getAtomMapNum();
                double_bond.dirs.second = RDKit::Bond::NONE;
            }
        }
        
        res.emplace_back(std::move(double_bond));
    }
    
    return res;
}

std::unordered_map<std::pair<int, int>, RDKit::Bond::BondDir, _hash_pair>
bond_dirs_by_mapnum(const RDKit::ROMol& mol) {
    std::unordered_map<std::pair<int, int>, RDKit::Bond::BondDir, _hash_pair> res;
    res.reserve(128);
    
    for(const auto b: mol.bonds()) {
        const int i = b->getBeginAtom()->getAtomMapNum();
        const int j = b->getEndAtom()->getAtomMapNum();
        if(i == 0 || j == 0 || b->getBondDir() == RDKit::Bond::NONE)
            continue;
        res.emplace(std::pair{i, j}, b->getBondDir());
        res.emplace(std::pair{j, i}, BondDirOpposite(b->getBondDir()));
    }
    return res;
}

std::pair<
    std::unordered_map<
        std::tuple<int, int, int, int>,
        std::pair<RDKit::Bond::BondDir, RDKit::Bond::BondDir>,
        _hash_tuple<int, int, int, int>
    >,
    std::unordered_set<std::pair<int, int>, _hash_pair>
> enumerate_possible_cistrans_defs(const RDKit::ROMol& mol) {
    std::unordered_map<
        std::tuple<int, int, int, int>,
        std::pair<RDKit::Bond::BondDir, RDKit::Bond::BondDir>,
        _hash_tuple<int, int, int, int>
    > required_bond_defs;
    std::unordered_set<std::pair<int, int>, _hash_pair> required_bond_defs_coreatoms;
    
    required_bond_defs.reserve(32);
    required_bond_defs_coreatoms.reserve(32);
    
    //if PLEVEL >= 10: print('Looking at initializing template frag')
    for(auto b: mol.bonds()) {
        if(b->getBondType() != RDKit::Bond::DOUBLE)
            continue;

        // Define begin and end atoms of the double bond
        const auto ba = b->getBeginAtom();
        const auto bb = b->getEndAtom();

        // Now check if it is even possible to specify
        if(ba->getDegree() == 1 || bb->getDegree() == 1)
            continue;

        const int ba_label = ba->getAtomMapNum();
        const int bb_label = bb->getAtomMapNum();
            
        //if PLEVEL >= 10: print('Found a double bond with potential cis/trans (based on degree)')
        //if PLEVEL >= 10: print('{} {} {}'.format(ba_label,
        //                       b.GetSmarts(),
        //                       bb_label))

        // Save core atoms so we know that cis/trans was POSSIBLE to specify
        required_bond_defs_coreatoms.emplace(ba_label, bb_label);
        required_bond_defs_coreatoms.emplace(bb_label, ba_label);
            
        // Define heaviest mapnum neighbor for each atom, excluding the other side of the double bond
        std::vector<int> ba_neighbor_labels, bb_neighbor_labels;
        ba_neighbor_labels.reserve(8);
        bb_neighbor_labels.reserve(8);
        for(const auto a: AtomGetNeighbors(ba)) {
            const int mapno = a->getAtomMapNum();
            if(bb_label != mapno) {
                ba_neighbor_labels.push_back(mapno);
            }
        }
        const int ba_neighbor_labels_max = *std::max_element(ba_neighbor_labels.begin(), ba_neighbor_labels.end());
        for(const auto a: AtomGetNeighbors(bb)) {
            const int mapno = a->getAtomMapNum();
            if(ba_label != mapno) {
                bb_neighbor_labels.push_back(mapno);
            }
        }
        const int bb_neighbor_labels_max = *std::max_element(bb_neighbor_labels.begin(), bb_neighbor_labels.end());

        // The direction of the bond being observed might need to be flipped,
        // based on
        //     (a) if it is the heaviest atom on this side, and 
        //     (b) if the begin/end atoms for the directional bond are 
        //         in the wrong order (i.e., if the double-bonded atom 
        //         is the begin atom)
        RDKit::Bond::BondDir front_spec, back_spec;
        bool front_spec_set{false}, back_spec_set{false};
        for(const auto bab: AtomGetBonds(ba)) {
            if(bab->getBondDir() != RDKit::Bond::NONE) {
                if(bab->getBeginAtom()->getAtomMapNum() == ba_label) {
                    // Bond is in wrong order - flip
                    if(bab->getEndAtom()->getAtomMapNum() != ba_neighbor_labels_max) {
                        // Defined atom is not the heaviest - flip
                        front_spec = bab->getBondDir();
                        front_spec_set = true;
                        break;
                    }
                    front_spec = BondDirOpposite(bab->getBondDir());
                    front_spec_set = true;
                    break;
                }
                if(bab->getBeginAtom()->getAtomMapNum() != ba_neighbor_labels_max) {
                    // Defined atom is not heaviest
                    front_spec = BondDirOpposite(bab->getBondDir());
                    front_spec_set = true;
                    break;
                }
                front_spec = bab->getBondDir();
                front_spec_set = true;
                break;
            }
        }
        if(!front_spec_set) {
            //if PLEVEL >= 10: print('Chirality not specified at front end of the bond!')
        } else {
            //if PLEVEL >= 10: print('Front specification: {}'.format(front_spec))
            
            for(const auto bbb: AtomGetBonds(bb)) {
                if(bbb->getBondDir() != RDKit::Bond::NONE) {
                    // For the "back" specification, the double-bonded atom *should* be the BeginAtom
                    if(bbb->getEndAtom()->getAtomMapNum() == bb_label) {
                        // Bond is in wrong order - flip
                        if(bbb->getBeginAtom()->getAtomMapNum() != bb_neighbor_labels_max) {
                            // Defined atom is not the heaviest - flip
                            back_spec = bbb->getBondDir();
                            back_spec_set = true;
                            break;
                        }
                        back_spec = BondDirOpposite(bbb->getBondDir());
                        back_spec_set = true;
                        break;
                    }
                    if(bbb->getEndAtom()->getAtomMapNum() != bb_neighbor_labels_max) {
                        // Defined atom is not heaviest - flip
                        back_spec = BondDirOpposite(bbb->getBondDir());
                        back_spec_set = true;
                        break;
                    }
                    back_spec = bbb->getBondDir();
                    back_spec_set = true;
                    break;
                }
            }
        }
        if(!back_spec_set) {
            //if PLEVEL >= 10: print('Chirality not specified at back end of the bond!')
        } else {
            //if PLEVEL >= 10: print('Back specification: {}'.format(back_spec))
        }

        // Is this an overall unspecified bond? Put it in the dictionary anyway, 
        // so there is something to match
        if(!front_spec_set || !back_spec_set) {
            // Create a definition over this bond so that reactant MUST be unspecified, too
            for(const auto start_atom: ba_neighbor_labels) {
                for(const auto end_atom: bb_neighbor_labels) {
                    required_bond_defs.emplace(std::tuple{start_atom, ba_label, bb_label, end_atom}, std::pair{RDKit::Bond::NONE, RDKit::Bond::NONE});
                    required_bond_defs.emplace(std::tuple{ba_label, start_atom, bb_label, end_atom}, std::pair{RDKit::Bond::NONE, RDKit::Bond::NONE});
                    required_bond_defs.emplace(std::tuple{start_atom, ba_label, end_atom, bb_label}, std::pair{RDKit::Bond::NONE, RDKit::Bond::NONE});
                    required_bond_defs.emplace(std::tuple{ba_label, start_atom, end_atom, bb_label}, std::pair{RDKit::Bond::NONE, RDKit::Bond::NONE});
                    required_bond_defs.emplace(std::tuple{bb_label, end_atom, start_atom, ba_label}, std::pair{RDKit::Bond::NONE, RDKit::Bond::NONE});
                    required_bond_defs.emplace(std::tuple{end_atom, bb_label, start_atom, ba_label}, std::pair{RDKit::Bond::NONE, RDKit::Bond::NONE});
                    required_bond_defs.emplace(std::tuple{bb_label, end_atom, ba_label, start_atom}, std::pair{RDKit::Bond::NONE, RDKit::Bond::NONE});
                    required_bond_defs.emplace(std::tuple{end_atom, bb_label, ba_label, start_atom}, std::pair{RDKit::Bond::NONE, RDKit::Bond::NONE});
                }
            }
            continue;
        }
        
        if(front_spec == back_spec) {
            //if PLEVEL >= 10: print('-> locally TRANS')
            b->setProp("localChirality", "trans");
        } else {
            //if PLEVEL >= 10: print('--> locally CIS')
            b->setProp("localChirality", "cis");
        }

        std::unordered_map<
            std::tuple<int, int, int, int>,
            std::pair<RDKit::Bond::BondDir, RDKit::Bond::BondDir>,
            _hash_tuple<int, int, int, int>
        > possible_defs;
        possible_defs.reserve(32);
        for(const auto start_atom: ba_neighbor_labels) {
            for(const auto end_atom: bb_neighbor_labels) {
                bool needs_inversion = (start_atom != ba_neighbor_labels_max) != \
                    (end_atom != bb_neighbor_labels_max);
                for(const auto start_atom_dir: std::vector<RDKit::Bond::BondDir>{RDKit::Bond::ENDUPRIGHT, RDKit::Bond::ENDDOWNRIGHT}) {
                    // When locally trans, BondDir of start shold be same as end, 
                    // unless we need inversion
                    RDKit::Bond::BondDir end_atom_dir;
                    if((front_spec != back_spec) != needs_inversion) {
                        // locally cis and does not need inversion (True, False)
                        // or locally trans and does need inversion (False, True)
                        end_atom_dir = BondDirOpposite(start_atom_dir);
                    } else {
                        // locally cis and does need inversion (True, True)
                        // or locally trans and does not need inversion (False, False)
                        end_atom_dir = start_atom_dir;
                    }

                    // Enumerate all combinations of atom orders...
                    possible_defs.emplace(std::tuple{start_atom, ba_label, bb_label, end_atom}, std::pair{start_atom_dir, end_atom_dir});
                    possible_defs.emplace(std::tuple{ba_label, start_atom, bb_label, end_atom}, std::pair{BondDirOpposite(start_atom_dir), end_atom_dir});
                    possible_defs.emplace(std::tuple{start_atom, ba_label, end_atom, bb_label}, std::pair{start_atom_dir, BondDirOpposite(end_atom_dir)});
                    possible_defs.emplace(std::tuple{ba_label, start_atom, end_atom, bb_label}, std::pair{BondDirOpposite(start_atom_dir), BondDirOpposite(end_atom_dir)});

                    possible_defs.emplace(std::tuple{bb_label, end_atom, start_atom, ba_label}, std::pair{end_atom_dir, start_atom_dir});
                    possible_defs.emplace(std::tuple{bb_label, end_atom, ba_label, start_atom}, std::pair{end_atom_dir, BondDirOpposite(start_atom_dir)});
                    possible_defs.emplace(std::tuple{end_atom, bb_label, start_atom, ba_label}, std::pair{BondDirOpposite(end_atom_dir), start_atom_dir});
                    possible_defs.emplace(std::tuple{end_atom, bb_label, ba_label, start_atom}, std::pair{BondDirOpposite(end_atom_dir), BondDirOpposite(start_atom_dir)});
                }
            }
        }
        
        // Save to the definition of this bond (in either direction)
        required_bond_defs.merge(std::move(possible_defs));
    }
    
    //if PLEVEL >= 10: print('All bond specs for this template:' )
    //if PLEVEL >= 10: print(str([(k, v) for (k, v) in required_bond_defs.items()]))

    return std::pair(required_bond_defs, required_bond_defs_coreatoms);
}

bool
restore_bond_stereo_to_sp2_atom(
    const RDKit::Atom* const a,
    const std::unordered_map<std::pair<int, int>, RDKit::Bond::BondDir, _hash_pair>& bond_dirs_by_mapnum,
    const bool verbose
) {
    bool needs_inversion;
    for(auto bond_to_spec: AtomGetBonds(a)) {
        if( bond_dirs_by_mapnum.contains(
                std::pair(bond_to_spec->getOtherAtom(a)->getAtomMapNum(), a->getAtomMapNum())
            )
        ) {
            bond_to_spec->setBondDir(
                bond_dirs_by_mapnum.at(
                    std::pair(bond_to_spec->getBeginAtom()->getAtomMapNum(),
                    bond_to_spec->getEndAtom()->getAtomMapNum())
                )
            );
            /*if PLEVEL >= 2: print('Tried to copy bond direction b/w {} and {}'.format(
                bond_to_spec.GetBeginAtom().GetAtomMapNum(),
                bond_to_spec.GetEndAtom().GetAtomMapNum()
            ))*/
            if(verbose) {
                std::cout << __func__ << ": Tried to copy bond direction between " << bond_to_spec->getBeginAtom()->getAtomMapNum() << " and " << bond_to_spec->getEndAtom()->getAtomMapNum() << std::endl;
            }
            return true;
        }
    }
    
    // Weird case, like C=C/O >> C=C/Br
    //if PLEVEL >= 2: print('Bond stereo could not be restored to sp2 atom, missing the branch that was used to define before...')
    if(verbose) {
        std::cout << __func__ << ": Bond stereo could not be restored to sp2 atom, missing the branch that was used to define before..." << std::endl;
    }
    
    if(a->getDegree() == 2) {
        // Either the branch used to define was replaced with H (deg 3 -> deg 2)
        // or the branch used to define was reacted (deg 2 -> deg 2)
        for(auto bond_to_spec: AtomGetBonds(a)) {
            if(bond_to_spec->getBondType() == RDKit::Bond::DOUBLE)
                continue;
            if(!bond_to_spec->getOtherAtom(a)->hasProp("old_mapno")) {
                // new atom, deg2->deg2, assume direction preserved
                //if PLEVEL >= 5: print('Only single-bond attachment to atom {} is new, try to reproduce chirality'.format(a.GetAtomMapNum()))
                if(verbose) {
                    std::cout << __func__ << ": Only single-bond attachment to atom " << a->getAtomMapNum() << " is new, try to reproduce chirality" << std::endl;
                }
                needs_inversion = false;
            } else {
                // old atom, just was not used in chirality definition - set opposite
                //if PLEVEL >= 5: print('Only single-bond attachment to atom {} is old, try to reproduce chirality'.format(a.GetAtomMapNum()))
                if(verbose) {
                    std::cout << __func__ << ": Only single-bond attachment to atom " << a->getAtomMapNum() << " is old, try to reproduce chirality" << std::endl;
                }
                needs_inversion = true;
            }

            for(const auto [key, bond_dir]: bond_dirs_by_mapnum) {
                if(bond_dir != RDKit::Bond::NONE) {
                    if(key.first == bond_to_spec->getBeginAtom()->getAtomMapNum()) {
                        if(needs_inversion)
                            bond_to_spec->setBondDir(BondDirOpposite(bond_dir));
                        else
                            bond_to_spec->setBondDir(bond_dir);
                        return true;
                    }
                }
            }
        }
    }

    if(a->getDegree() == 3) {
        // If we lost the branch defining stereochem, it must have been replaced
        for(auto bond_to_spec: AtomGetBonds(a)) {
            if(bond_to_spec->getBondType() == RDKit::Bond::DOUBLE)
                continue;
            const auto oa = bond_to_spec->getOtherAtom(a);
            if(oa->hasProp("old_mapno") || oa->hasProp("react_atom_idx")) {
                // looking at an old atom, which should have opposite direction as removed atom
                if(verbose) {
                    std::cout << __func__ << ": looking at an old atom, which should have opposite direction as removed atom, mapno=" << a->getAtomMapNum() << std::endl;
                }
                needs_inversion = true;
            } else {
                // looking at a new atom, assume same as removed atom
                if(verbose) {
                    std::cout << __func__ << ": looking at a new atom, assume same as removed atom, mapno=" << a->getAtomMapNum() << std::endl;
                }
                needs_inversion = false;
            }

            for(const auto [key, bond_dir]: bond_dirs_by_mapnum) {
                if(bond_dir != RDKit::Bond::NONE) {
                    if(key.first == bond_to_spec->getBeginAtom()->getAtomMapNum()) {
                        if(needs_inversion)
                            bond_to_spec->setBondDir(BondDirOpposite(bond_dir));
                        else
                            bond_to_spec->setBondDir(bond_dir);
                        return true;
                    }
                }
            }
        }
    }

    return false;
}

// util.py
std::string bond_to_label(const RDKit::Bond *bond) {
    std::string a1_label = std::to_string(bond->getBeginAtom()->getAtomicNum());
    std::string a2_label = std::to_string(bond->getEndAtom()->getAtomicNum());
    if(bond->getBeginAtom()->getAtomMapNum())
        a1_label += std::to_string(bond->getBeginAtom()->getAtomMapNum());
    if(bond->getEndAtom()->getAtomMapNum())
        a2_label += std::to_string(bond->getEndAtom()->getAtomMapNum());
    if(a1_label > a2_label) std::swap(a1_label, a2_label);

    return a1_label + RDKit::SmilesWrite::GetBondSmiles(bond, -1, false, false) + a2_label; // cannot use BondGetSmarts(bond)
}

bool atoms_are_different(const RDKit::Atom * const atom1, const RDKit::Atom * const atom2, const bool use_smarts) {
    if(use_smarts && AtomGetSmarts(atom1) != AtomGetSmarts(atom2)) return true; // should be very general
    if(atom1->getAtomicNum() != atom2->getAtomicNum()) return true; // must be true for atom mapping
    if(atom1->getTotalNumHs() != atom2->getTotalNumHs()) return true;
    if(atom1->getFormalCharge() != atom2->getFormalCharge()) return true;
    if(atom1->getDegree() != atom2->getDegree()) return true;
    if(atom1->getNumRadicalElectrons() != atom2->getNumRadicalElectrons()) return true;
    if(atom1->getIsAromatic() != atom2->getIsAromatic()) return true;

    // Check bonds and nearest neighbor identity
    std::vector<std::string> bonds1, bonds2;
    bonds1.reserve(4);
    bonds2.reserve(4);
    for(const auto bond: AtomGetBonds(atom1))
        bonds1.emplace_back(bond_to_label(bond));
    for(const auto bond: AtomGetBonds(atom2))
        bonds2.emplace_back(bond_to_label(bond));
    std::sort(bonds1.begin(), bonds1.end());
    std::sort(bonds2.begin(), bonds2.end());
    if(bonds1 != bonds2) return true;

    return false;
}

}
