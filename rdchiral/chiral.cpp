#include <vector>
#include <algorithm>
#include <iostream>

#include "rdchiral/chiral.hpp"
#include "rdchiral/rdkit_helper.hpp"
#include "rdchiral/util.hpp"

namespace rdchiral {

bool template_atom_could_have_been_tetra(RDKit::Atom *atom, const bool strip_if_spec, const bool cache) {
    if(atom->hasProp("tetra_possible")) {
        bool res;
        atom->getProp("tetra_possible", res);
        return res;
    }
    bool is_set = false;
    if(atom->getDegree() == 3) {
        auto s = AtomGetSmarts(atom);
        if(s.find("H") == std::string::npos) {
            is_set = true;
        }
    }
    if(atom->getDegree() < 3 || is_set) {
        if(cache)
            atom->setProp("tetra_possible", false);
        if(strip_if_spec) // Clear chiral tag in case improperly set
            atom->setChiralTag(RDKit::Atom::CHI_UNSPECIFIED);
        return false;
    }
    if(cache)
        atom->setProp("tetra_possible", true);
    return true;
}

void copy_chirality(RDKit::Atom *a_src, RDKit::Atom *a_new, const bool verbose) {
    if(a_new->getDegree() < 3)
        return;
    if(a_new->getDegree() == 3) {
        for(auto b: AtomGetBonds(a_new)) {
            if(b->getBondType() != RDKit::Bond::SINGLE) return;
        }
    }

    // if PLEVEL >= 3: print('For mapnum {}, copying src {} chirality tag to new'.format(
    //     a_src.GetAtomMapNum(), a_src.GetChiralTag()))
    if(verbose) {
        std::cout << __func__ << ": For mapnum " << a_src->getAtomMapNum() << ", copying src " << a_src->getChiralTag() << " chirality tag to new" << std::endl;
    }
    a_new->setChiralTag(a_src->getChiralTag());
    
    if(atom_chirality_matches(a_src, a_new, verbose) == -1) {
        //if PLEVEL >= 3: print('For mapnum {}, inverting chirality'.format(a_new.GetAtomMapNum()))
        if(verbose) {
            std::cout << __func__ << ": For mapnum " << a_new->getAtomMapNum() << ", inverting chirality" << std::endl;
        }
        a_new->invertChirality();
    }
}

// Return values:
//      chiral:     match = 1, mismatch = -1
//      achiral:    match or ambiguous = 2, mismatch or ambiguous = 0
int atom_chirality_matches(RDKit::Atom *a_tmp, RDKit::Atom* a_mol, const bool verbose) {
    if(a_mol->getChiralTag() == RDKit::Atom::CHI_UNSPECIFIED) {
        if(a_tmp->getChiralTag() == RDKit::Atom::CHI_UNSPECIFIED) {
            //if PLEVEL >= 3: print('atom {} is achiral & achiral -> match'.format(a_mol.GetAtomMapNum()))
            if(verbose) {
                std::cout << __func__ << ": atom " << a_mol->getAtomMapNum() << " is achiral & achiral -> match" << std::endl;
            }
            return 2; // # achiral template, achiral molecule -> match
        }
        // What if the template was chiral, but the reactant isn't just due to symmetry?
        if(!a_mol->hasProp("_ChiralityPossible")) {
            // It's okay to make a match, as long as the product is achiral (even
            // though the product template will try to impose chirality)
            //if PLEVEL >= 3: print('atom {} is specified in template, but cant possibly be chiral in mol'.format(a_mol.GetAtomMapNum()))
            if(verbose) {
                std::cout << __func__ << ": atom " << a_mol->getAtomMapNum() << " is specified in template, but cant possibly be chiral in mol" << std::endl;
            }
            return 2;
        }

        // Discussion: figure out if we want this behavior - should a chiral template
        // be applied to an achiral molecule? For the retro case, if we have
        // a retro reaction that requires a specific stereochem, return False;
        // however, there will be many cases where the reaction would probably work
        //if PLEVEL >= 3: print('atom {} is achiral in mol, but specified in template'.format(a_mol.GetAtomMapNum()))
        if(verbose) {
            std::cout << __func__ << ": atom " << a_mol->getAtomMapNum() << " is achiral in mol, but specified in template" << std::endl;
        }
        return 0;
    }
    if(a_tmp->getChiralTag() == RDKit::Atom::CHI_UNSPECIFIED) {
        //if PLEVEL >= 3: print('Reactant {} atom chiral, rtemplate achiral...'.format(a_tmp.GetAtomMapNum()))
        if(verbose) {
            std::cout << __func__ << ": Reactant " << a_tmp->getAtomMapNum() << " atom chiral, rtemplate achiral..." << std::endl;
        }
        if(template_atom_could_have_been_tetra(a_tmp)) {
            //if PLEVEL >= 3: print('...and that atom could have had its chirality specified! no_match')
            if(verbose) {
                std::cout << __func__ << ": ...and that atom could have had its chirality specified! no_match" << std::endl;
            }
            return 0;
        }
        //if PLEVEL >= 3: print('...but the rtemplate atom could not have had chirality specified, match anyway')
        if(verbose) {
            std::cout << __func__ << ": ...but the rtemplate atom could not have had chirality specified, match anyway" << std::endl;
        }
        return 2;
    }

    std::vector<int> mapnums_tmp, mapnums_mol;
    mapnums_tmp.reserve(8);
    mapnums_mol.reserve(8);
    for(auto a: AtomGetNeighbors(a_tmp))
        mapnums_tmp.push_back(a->getAtomMapNum());
    for(auto a: AtomGetNeighbors(a_mol))
        mapnums_mol.push_back(a->getAtomMapNum());

    // When there are fewer than 3 heavy neighbors, chirality is ambiguous...
    if(mapnums_tmp.size() < 3 || mapnums_mol.size() < 3)
        return 2;

    // Degree of 3 -> remaining atom is a hydrogen, add to list
    if(mapnums_tmp.size() < 4)
        mapnums_tmp.push_back(-1); // H
    if(mapnums_mol.size() < 4)
        mapnums_mol.push_back(-1); // H

    //try:
        //if PLEVEL >= 10: print(str(mapnums_tmp))
        //if PLEVEL >= 10: print(str(mapnums_mol))
        //if PLEVEL >= 10: print(str(a_tmp.GetChiralTag()))
        //if PLEVEL >= 10: print(str(a_mol.GetChiralTag()))
        // only_in_src = [i for i in mapnums_tmp if i not in mapnums_mol][::-1] # reverse for popping
        std::vector<int> only_in_src, only_in_mol;
        only_in_src.reserve(mapnums_tmp.size());
        only_in_mol.reserve(mapnums_mol.size());
        for(auto i: mapnums_tmp) {
            auto iter = std::find(mapnums_mol.begin(), mapnums_mol.end(), i);
            if(iter == mapnums_mol.end()) {
                only_in_src.push_back(i);
            }
        }
        std::reverse(only_in_src.begin(), only_in_src.end());
        // only_in_mol = [i for i in mapnums_mol if i not in mapnums_tmp]
        for(auto i: mapnums_mol) {
            auto iter = std::find(mapnums_tmp.begin(), mapnums_tmp.end(), i);
            if(iter == mapnums_tmp.end()) {
                only_in_mol.push_back(i);
            }
        }
        if(only_in_src.size() <= 1 && only_in_mol.size() <= 1) {
            int tmp_parity = parity4(mapnums_tmp.data());
            std::vector<int> _tmp;
            _tmp.reserve(mapnums_mol.size());
            // [i if i in mapnums_tmp else only_in_src.pop() for i in mapnums_mol]
            for(auto i: mapnums_mol) {
                auto iter = std::find(mapnums_tmp.begin(), mapnums_tmp.end(), i);
                if(iter != mapnums_tmp.end()) {
                    _tmp.push_back(i);
                } else {
                    _tmp.push_back(only_in_src.back());
                    only_in_src.pop_back();
                }
            }
            int mol_parity = parity4(_tmp.data());
            //if PLEVEL >= 10: print(str(tmp_parity))
            //if PLEVEL >= 10: print(str(mol_parity))
            bool parity_matches = tmp_parity == mol_parity;
            bool tag_matches = a_tmp->getChiralTag() == a_mol->getChiralTag();
            bool chirality_matches = parity_matches == tag_matches;
            //if PLEVEL >= 2: print('mapnum {} chiral match? {}'.format(a_tmp.GetAtomMapNum(), chirality_matches))
            if(verbose) {
                std::cout << __func__ << ": mapnum " << a_tmp->getAtomMapNum() << " chiral match? chirality_matches=" << chirality_matches << std::endl;
            }
            if(chirality_matches)
                return 1;
            else
                return -1;
        } else {
            //if PLEVEL >= 2: print('mapnum {} chiral match? Based on mapnum lists, ambiguous -> True'.format(a_tmp.GetAtomMapNum()))
            if(verbose) {
                std::cout << __func__ << ": mapnum " << a_tmp->getAtomMapNum() << " chiral match? Based on mapnum lists, ambiguous -> True" << std::endl;
            }
            return 2; // ambiguous case, just return for now
        }
    /*
    except IndexError as e:
        print(a_tmp.GetPropsAsDict())
        print(a_mol.GetPropsAsDict())
        print(a_tmp.GetChiralTag())
        print(a_mol.GetChiralTag())
        print(str(e))
        print(str(mapnums_tmp))
        print(str(mapnums_mol))
        raise KeyError('Pop from empty set - this should not happen!')
    */
}

}
