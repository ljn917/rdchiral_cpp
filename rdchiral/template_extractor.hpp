#ifndef RDCHIRAL_TEMPLATE_EXTRACTOR_HEADER
#define RDCHIRAL_TEMPLATE_EXTRACTOR_HEADER

#include <string>
#include <vector>
#include <utility>

#include <GraphMol/RWMol.h>

namespace rdchiral {

struct template_result {
    std::string reactants_smarts;
    std::string products_smarts;
    std::string reaction_smarts_forward;
    std::string reaction_smarts_retro;
    bool intra_only{false}, dimer_only{false};
    std::string necessary_reagent;
};

// this function may throw
struct template_result
extract_from_reaction(
    const std::string& reactants_smiles,
    const std::string& products_smiles,
    const bool verbose = false,
    const bool use_stereochemistry = true,
    const unsigned maximum_number_unmapped_product_atoms = 5,
    const bool include_all_unmapped_reactant_atoms = true,
    const int radius = 1,
    const bool use_custom_special_groups = false,
    const std::vector<std::pair<std::vector<int>,RDKit::RWMOL_SPTR>> special_groups = {},
    const bool do_canonicalize = true
);

}

#endif // RDCHIRAL_TEMPLATE_EXTRACTOR_HEADER
